//
//  GetHelpTableViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 12/31/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetHelpTableViewController : UITableViewController

@end
