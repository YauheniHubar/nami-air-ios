//
//  FAQTableViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 12/27/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "FAQTableViewController.h"

@interface FAQTableViewController ()
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSLayoutConstraint *qConstraint;
@property (strong, nonatomic) NSLayoutConstraint *aConstraint;
@property (nonatomic, strong) UITableViewCell *prototypeCell;
@end

@implementation FAQTableViewController

@synthesize selectedIndexPath, data, qConstraint, aConstraint, prototypeCell;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 44;
    // Set negative index path so that all rows start collapsed
    selectedIndexPath = [NSIndexPath indexPathForRow:-1 inSection:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return data.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (selectedIndexPath.row != indexPath.row) {
        [tableView beginUpdates];
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        selectedIndexPath = indexPath;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView endUpdates];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    } else {
        selectedIndexPath = [NSIndexPath indexPathForRow:-1 inSection:0];
        [tableView beginUpdates];
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView endUpdates];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] != NSOrderedAscending) {
        return UITableViewAutomaticDimension;
    }
    
    UILabel *qLabel = (UILabel*)[self.prototypeCell.contentView viewWithTag:1];
    UILabel *aLabel = (UILabel*)[self.prototypeCell.contentView viewWithTag:2];
    
    NSDictionary *dict = data[indexPath.row];
    
    qLabel.text = dict[@"q"];
    aLabel.text = (indexPath.row == selectedIndexPath.row) ? dict[@"a"] : @"";
    
    CGRect qlabelRect = [qLabel.text boundingRectWithSize:CGSizeMake(qLabel.frame.size.width, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:qLabel.font} context:nil];
    CGRect aLabelRect = [aLabel.text boundingRectWithSize:CGSizeMake(aLabel.frame.size.width, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:aLabel.font} context:nil];
    
    CGFloat padding = (indexPath.row == selectedIndexPath.row) ? 53 + aLabelRect.size.height : 23;
    
    return qlabelRect.size.height + padding;
}

- (UITableViewCell *)prototypeCell {
    if (!prototypeCell) {
        prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:@"PrototypeCell"];
    }
    
    return prototypeCell;
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    UILabel *qLabel = (UILabel*)[cell.contentView viewWithTag:1];
    UILabel *aLabel = (UILabel*)[cell.contentView viewWithTag:2];
    qLabel.translatesAutoresizingMaskIntoConstraints = NO;
    aLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIImageView *showHideImage = (UIImageView*)[cell.contentView viewWithTag:3];
    showHideImage.image = (indexPath.row == selectedIndexPath.row) ? [UIImage imageNamed:@"faq_hide"] : [UIImage imageNamed:@"faq_show"];
    
    NSDictionary *dict = data[indexPath.row];
    
    qLabel.text = dict[@"q"];
    aLabel.text = (indexPath.row == selectedIndexPath.row) ? dict[@"a"] : @"";
    [qLabel sizeToFit];
    [aLabel sizeToFit];
    
    NSLayoutConstraint *constrain = [NSLayoutConstraint constraintWithItem:((indexPath.row == selectedIndexPath.row) ? aLabel : qLabel)
                                                                 attribute:NSLayoutAttributeBottomMargin
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:cell.contentView
                                                                 attribute:NSLayoutAttributeBottomMargin
                                                                multiplier:1.0
                                                                  constant:-10];
    
    for (NSLayoutConstraint *currentConstraint in cell.contentView.constraints) {
        if (currentConstraint.firstItem != ((indexPath.row == selectedIndexPath.row) ? aLabel : qLabel) && currentConstraint.firstAttribute == NSLayoutAttributeBottomMargin) {
            [cell.contentView removeConstraint:currentConstraint];
        }
    }
    
    [cell.contentView addConstraint:constrain];
}

@end
