//
//  EditAccountViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/19/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "EditAccountViewController.h"
#import "Models.h"
#import <QuartzCore/QuartzCore.h>
#import "DLRadioButton.h"
#import "konstant.h"
#import "AppDelegate.h"


@interface EditAccountViewController ()
{
    NSInteger newUserType;
    NSInteger currentUserType;
}

@property (strong, nonatomic) IBOutletCollection(DLRadioButton) NSArray *topRadioButtons;
@property (nonatomic) NSArray *bottomRadioButtons;

@end

@implementation EditAccountViewController

@synthesize btnSave,txtConfirmPassword,txtEmailAddress,txtPassword;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    btnSave.layer.cornerRadius = 5;
    currentUserType = [[[PFUser currentUser] objectForKey:@"userType"] integerValue];
    txtEmailAddress.text = [[PFUser currentUser] objectForKey:@"username"];
    for (DLRadioButton *radioButton in self.topRadioButtons) {
        radioButton.ButtonIcon = [UIImage imageNamed:@"check-green-off.png"];
        radioButton.ButtonIconSelected = [UIImage imageNamed:@"check-green-on.png"];
        
        if (currentUserType == USER_TYPE_USER && [radioButton.titleLabel.text isEqualToString:FINDING]) {
            [radioButton setSelected:YES];
        } else if (currentUserType == USER_TYPE_CAREGIVER && [radioButton.titleLabel.text isEqualToString:SUPPORTING]) {
            [radioButton setSelected:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveClicked:(id)sender {
    NSString *username = txtEmailAddress.text;
    NSString *password = txtPassword.text;
    NSString *confirm = txtConfirmPassword.text;
    
    
    if (!username || username.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Error"
                                                        message:@"User Name Required"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (!password || password.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Error"
                                                        message:@"Password Required"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (!confirm || confirm.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Error"
                                                        message:@"Confirm Password Required"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (![confirm isEqualToString:password]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Error"
                                                        message:@"Passwords do not match."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSString *buttonName = [(DLRadioButton *)self.topRadioButtons[0] selectedButton].titleLabel.text;
    if ([buttonName isEqual:FINDING]) {
        newUserType = 1;
    } else {
        newUserType = 2;
    }
    
    if (newUserType != currentUserType) {
        
        [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser *user, NSError *error) {
            if (!error) {
                [user setObject:@(newUserType) forKey:@"userType"];
                [user saveInBackgroundWithBlock:^(BOOL success, NSError *error) {
                    if (success) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"You have successfully switched groups!  Your feed will now display posts associated with this group and any new posts you compose will appear within this group." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                        [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser *user, NSError *error) {
                            NSDictionary *params = @{@"notificationType":@(NOTIFICATION_TYPE_CHANGE_GROUP)};
                            [PFCloud callFunctionInBackground:@"notificationCreateChangeGroup" withParameters:params block:^(id object, NSError *error) {
                                if (error) {
                                    NSLog(@"Could not send group change notification %@",error.description);
                                }
                                [self.navigationController popViewControllerAnimated:YES];
                            }];
                            
                        }];
                    }
                    else {
                        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                        
                        /*
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Error"
                                                                        message:@"Could not save new group."
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                        */
                    }
                }];
            }
            else {
                
                
                NSString *errorMessage = [[error userInfo] objectForKey:@"error"];
                if ([errorMessage rangeOfString:@"invalid login"].length > 0) {
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Error"
                                                                    message:@"Could not validate user."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                } else {
                    
                    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                }
                
            }
        }];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
