//
//  AppVersionViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/12/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppVersionViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *version;

@end
