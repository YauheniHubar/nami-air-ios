//
//  LegalViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 1/7/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LegalViewController : UIViewController<UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UITextView *textView;

@end
