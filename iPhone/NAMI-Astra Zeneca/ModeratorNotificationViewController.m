//
//  ModeratorNotificationViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/12/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import "ModeratorNotificationViewController.h"

@interface ModeratorNotificationViewController ()

@end

@implementation ModeratorNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.outerView.layer.cornerRadius = 3.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction) ignoreButtonClicked:(id)sender {
    
    if (self.delegate) {
        [self.delegate moderatorMessageIgnoreButtonClicked:self];
    }
}
-(IBAction) openButtonClicked:(id)sender {
    
    if (self.delegate) {
        [self.delegate moderatorMessageOpenButtonClicked:self];
    }
}

@end
