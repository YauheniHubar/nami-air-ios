//
//  DeleteAccountViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/19/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "DeleteAccountViewController.h"
#import "Models.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface DeleteAccountViewController ()

@end

@implementation DeleteAccountViewController

@synthesize txtPassword,btnDelete;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    btnDelete.layer.cornerRadius = 5;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)deleteClicked:(id)sender {
    NSString *password = txtPassword.text;
    if (password && password.length > 0) {
        PFUser *user = [PFUser currentUser];
        NSString *username = user.username;
        [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser *loggedUser, NSError *error) {
            if (error) {
                
                //[((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Error" message:@"Your password is incorrect" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            } else {
                NSDictionary *params = @{};
                [PFCloud callFunctionInBackground:@"deleteAccount" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    } else {
                        [PFUser logOut];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"NAMIUserLoggedOut" object:nil];
                        [self.navigationController popToRootViewControllerAnimated:NO];
                    }
                }];
            }
        }];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Error"
                                                        message:@"Password Required"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}
@end
