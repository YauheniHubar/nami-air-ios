//
//  PostDetailViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/3/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostPopupDelegate.h"
#import "NoteSelectionViewController.h"
@class PFObject;

@protocol PostDetailDelegate <NSObject>

-(void)userHidPost:(PFObject *)post;
-(void)userBlockedAuthorOfPost:(PFObject *)post;

@end

@protocol PostDetailUpdateDelegate <NSObject>


-(void)updatePostDetailView:(PFObject *)post;

@end

@interface PostDetailsViewController : UIViewController <PostPopupDelegate,NoteSelectionDelegate,UIScrollViewDelegate, PostDetailUpdateDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *btnLeaveNote;
@property (strong, nonatomic) NSArray *postArray;
@property (assign) NSUInteger chosenPostIndex;
@property (strong,nonatomic) id<PostDetailDelegate> delegate;


- (IBAction)leaveNote:(id)sender;


-(void)updatePostDetailView:(PFObject *)post;
-(void)showMeTooPopup;
@end
