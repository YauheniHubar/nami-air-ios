//
//  PostHug.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostHug.h"
#import <Parse/PFObject+Subclass.h>

@implementation PostHug

@dynamic displayName,post,user;

+(NSString *)parseClassName {
    return @"PostHug";
}


@end
