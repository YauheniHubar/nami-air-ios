//
//  PostPopupDelegate.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/11/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#ifndef NAMI_Astra_Zeneca_PostPopupDelegate_h
#define NAMI_Astra_Zeneca_PostPopupDelegate_h

#define POST_POPUP_FLAGGED 9000
#define POST_POPUP_METOO   9001

@protocol PostPopupDelegate <NSObject>

@optional
-(void)okButtonClicked:(id)sender;
-(void)cancelButtonClicked:(id)sender;

@end


#endif
