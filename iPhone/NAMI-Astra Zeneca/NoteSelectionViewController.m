//
//  NoteSelectionViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/11/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "NoteSelectionViewController.h"
#import "AppDelegate.h"

@interface NoteSelectionViewController ()
{
    NSArray *hashNoteModels;
    NSMutableArray *selectedNotes;
}

@property (strong,nonatomic) UIBarButtonItem *doneButton;
@property (assign) int selectedCount;

@end

@implementation NoteSelectionViewController

@synthesize delegate,listMode,doneButton,selectedCount;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonClicked:)];
    doneButton.enabled = NO;
    self.navigationItem.rightBarButtonItem = doneButton;
    if (listMode == LIST_MODE_NOTES) {
        hashNoteModels = [AppDelegate getHashNoteModels];
    }
    else {
        self.title = @"I'm Feeling...";
        hashNoteModels = [AppDelegate getFeelingModels];
    }
    selectedNotes = [[NSMutableArray alloc] initWithCapacity:hashNoteModels.count];
    for (int i=0; i<hashNoteModels.count; i++) {
        [selectedNotes setObject:@(NO) atIndexedSubscript:i];
    }
    selectedCount = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)doneButtonClicked:(id)sender
{
    if (delegate) {
        [delegate userSelectedNotes:selectedNotes];
    }
    else [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return hashNoteModels.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoteSelectionCell" forIndexPath:indexPath];
    UIColor *postGreen = [UIColor colorWithRed:103.0f/255.0f green:138.0f/255.0f blue:2.0f/255.0f alpha:1.0];

    UIButton *btnCheck = (UIButton *)[cell viewWithTag:1];
    UILabel *textLabel = (UILabel *)[cell viewWithTag:2];
    textLabel.text = [[hashNoteModels objectAtIndex:indexPath.row] objectForKey:@"name"];
    BOOL selected = [[selectedNotes objectAtIndex:indexPath.row] boolValue];
    if (selected) {
        btnCheck.selected = YES;
        textLabel.textColor = postGreen;
    }
    else {
        btnCheck.selected = NO;
        textLabel.textColor = [UIColor darkGrayColor];
    }
    
    // Clear the button text.
    [btnCheck setTitle:@"" forState:UIControlStateNormal];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL selected = [[selectedNotes objectAtIndex:indexPath.row] boolValue];
    if (selected) {
        [selectedNotes setObject:@(NO) atIndexedSubscript:indexPath.row];
        selectedCount--;
    }
    else {
        [selectedNotes setObject:@(YES) atIndexedSubscript:indexPath.row];
        selectedCount++;
    }
    doneButton.enabled = (selectedCount > 0);
    
    [tableView reloadData];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
