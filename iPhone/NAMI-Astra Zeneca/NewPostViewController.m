//

#import "konstant.h"
#import "NewPostViewController.h"
#import "AppDelegate.h"
#import "Parse/Parse.h"
#import "Models.h"
#import "PostPendingPopupViewController.h"
#import "PostDetailView.h"

@interface NewPostViewController () <UITextViewDelegate>
{
    NoteSelectionViewController *feelingsController;
    NSInteger oldTabIndex;
    UIView *accView;
    PostPendingPopupViewController *pppvc;
    PFObject *updatedPost;
    BOOL postSubmitted;
}

@end

@implementation NewPostViewController

@synthesize textView,accessoryView,inspiringPost, postButton, fromFeed, postUpdateDelegate;

#pragma mark - View lifecycle

- (void)loadView
{
    [super loadView];

    postSubmitted = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)observeKeyboard {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Begin observing the keyboard notifications when the view is loaded.
    [self observeKeyboard];
    feelingsController = nil;
    textView.text = @"";
    accView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,44)];
    accView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    accView.alpha = .5;
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 260, 19)];
    iv.image = [UIImage imageNamed:@"feelings-indicator"];
    [accView addSubview:iv];
    iv.center = accView.center;
    iv.userInteractionEnabled = YES;
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectFeelings:)];
    [iv addGestureRecognizer:gr];
    
    
    textView.inputAccessoryView = accView;

}

-(void)viewWillAppear:(BOOL)animated
{
    [textView becomeFirstResponder];
}

#pragma mark - Button handlers

- (IBAction)addButton:(id)sender {
    [textView resignFirstResponder];
    [self addButtonTouchHandler:sender];
}

- (IBAction)deleteButton:(id)sender {
    [textView resignFirstResponder];
    [self cancelButtonTouchHandler:sender];
}

- (void)addButtonTouchHandler:(id)sender 
{

}

- (void)cancelButtonTouchHandler:(id)sender 
{
    [self dismissViewControllerAnimated:YES completion:nil];
    // Dismiss the viewController upon cancel
}

- (IBAction)cancelButtonHandler:(id)sender {
    if (textView.text.length > 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cancel Post" message:@"Are you sure you want to cancel this post?" delegate:self cancelButtonTitle:@"No, Don't Cancel" otherButtonTitles:@"Yes, Cancel Post", nil];
        [alert show];
    } else {
        [self closePostScreen];
    }
}

- (IBAction)addButtonHandler:(id)sender {
    BOOL approved = YES;
    NSArray *redFlags = [AppDelegate getRedFlagWordModels];
    NSMutableArray *redFlagsFound = [[NSMutableArray alloc] init];
    
    NSMutableCharacterSet *seperatorSet = [NSMutableCharacterSet whitespaceAndNewlineCharacterSet];
    [seperatorSet formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
    
    NSArray *words = [textView.text componentsSeparatedByCharactersInSet:seperatorSet];
    
    for (NSString *word in words) {
        for (RedFlagKeyword *rfk in redFlags) {
            NSString *fword = rfk.name;
            if ([[word lowercaseString] isEqualToString:[fword lowercaseString]]) {
                approved = NO;
                [redFlagsFound addObject:fword];
            }
           
        }
    }
    if (approved) {
        [self submitPost:YES];
    }
    else {
        [textView resignFirstResponder];
        CGPoint centerPoint = self.view.center;
        UIView *clickEater = [[UIView alloc] initWithFrame:CGRectMake(0,0,1000,1000)];
        clickEater.backgroundColor = [UIColor clearColor];
        clickEater.userInteractionEnabled = YES;
        clickEater.tag = 555;
        [self.view addSubview:clickEater];
        pppvc = [[PostPendingPopupViewController alloc] initWithNibName:@"PostPendingPopupViewController" bundle:nil];
        pppvc.keywords = redFlagsFound;
        
        pppvc.delegate = self;
        centerPoint.y = centerPoint.y - self.view.frame.origin.y;
        pppvc.view.center = centerPoint;
        [self.view addSubview:pppvc.view];
    }
}

-(void)okButtonClicked:(id)sender {
    [[self.view viewWithTag:555] removeFromSuperview];
    [pppvc.view removeFromSuperview];
    pppvc = nil;
    [self submitPost:NO];
}

-(void)cancelButtonClicked:(id)sender {
    [[self.view viewWithTag:555] removeFromSuperview];
    [pppvc.view removeFromSuperview];
    pppvc = nil;
    [textView becomeFirstResponder];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.cancelButtonIndex != buttonIndex) {
        [self closePostScreen];
    }
}

- (void)closePostScreen {
    textView.text = @"";
    if (postUpdateDelegate == nil) {
        [self.navigationController popViewControllerAnimated:YES];
        if (postSubmitted == YES && self.inspiringPost != nil) {
            ((AppDelegate *)[[UIApplication sharedApplication] delegate]).inspiredPost = self.inspiringPost;
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:self.inspiringPost forKey:@"inspiredPost"];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"NAMIShowInspiredPost" object:nil userInfo:userInfo];
            return;
        } else {
            ((AppDelegate *)[[UIApplication sharedApplication] delegate]).scrollToTopAllPosts = YES;
        }
        
        
        
        self.tabBarController.selectedIndex = 0;
       
        
    } else {
        [self.navigationController popViewControllerAnimated:YES];
        
        if (postSubmitted == YES && self.inspiringPost != nil) {
            ((AppDelegate *)[[UIApplication sharedApplication] delegate]).inspiredPost = self.inspiringPost;
            
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:self.inspiringPost forKey:@"inspiredPost"];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"NAMIShowInspiredPost" object:nil userInfo:userInfo];
            
            return;
            
        } else if (postSubmitted == YES) {
            
            if (postUpdateDelegate != nil) [postUpdateDelegate updatePostDetailView:self.inspiringPost];
        }
        
        self.tabBarController.selectedIndex = 0;
    }
    
    postUpdateDelegate = nil;
    postSubmitted = NO;
}


-(void)submitPost:(BOOL)approved
{
    if (textView.text.length == 0) {
        return;
    }
    // Create a new Post object and create relationship with PFUser
    PFObject *newPost = [PFObject objectWithClassName:@"Post"];
    [newPost setObject:[textView text] forKey:@"text"];
    [newPost setObject:[PFUser currentUser] forKey:@"user"];
    [newPost setObject:[[PFUser currentUser] objectForKey:@"userType"] forKey:@"userType"];
    if (inspiringPost)
        [newPost setObject:inspiringPost forKey:@"inspiredByPost"];
    [newPost setObject:[NSNumber numberWithInteger:(approved)?1:0] forKey:@"approvedStatus"];
    [newPost setObject:[NSNumber numberWithInteger:0] forKey:@"flaggedStatus"];
    [newPost setObject:[NSNumber numberWithBool:FALSE] forKey:@"isPrivate"];
    
    NSLog(@"newPost: %@", newPost);
    
    // Set ACL permissions for added security
    PFACL *postACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [postACL setPublicReadAccess:YES];
    [postACL setPublicWriteAccess:YES];
    [newPost setACL:postACL];
    
    __block UIActivityIndicatorView *progress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    progress.color = [UIColor darkGrayColor];
    [self.view addSubview:progress];
    progress.center = self.view.center;
    [progress startAnimating];
    [postButton setEnabled:NO];
    
    // Save new Post object in Parse
    [newPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            //[self dismissViewControllerAnimated:YES completion:nil];
            // Dismiss the viewController upon success
            
            updatedPost = newPost;
            postSubmitted = YES;
            
            // Determine when to close screen.
            __block NSInteger shouldClose = 0;
            if (inspiringPost) shouldClose+=2;
            if (!approved) shouldClose++;
            
            if (inspiringPost) {
                
                // Create the inspired notification
                NSMutableDictionary *params = (NSMutableDictionary *)@{@"postId":inspiringPost.objectId,@"notificationType":@(NOTIFICATION_TYPE_INSPIRED)};
                [PFCloud callFunctionInBackground:@"notificationCreate" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    }
                    
                    shouldClose--;
                    if (shouldClose == 0) {
                        [postButton setEnabled:YES];
                        [progress stopAnimating];
                        [progress removeFromSuperview];
                        [self closePostScreen];
                    }
           
                }];
                
                // Update the inspired npost count.
                params = (NSMutableDictionary *)@{@"postId":inspiringPost.objectId};
                [PFCloud callFunctionInBackground:@"updateInspiredPostCount" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@", error);
                        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    } else {
                        
                        int storyCnt = [[inspiringPost objectForKey:@"meTooStoryCount"] intValue];
                        storyCnt++;
                        [inspiringPost setObject:@(storyCnt) forKey:@"meTooStoryCount"];
                    }
                    
                    shouldClose--;
                    if (shouldClose == 0) {
                        [postButton setEnabled:YES];
                        [progress stopAnimating];
                        [progress removeFromSuperview];
                        [self closePostScreen];
                    }
               
                }];

            }
            
            // If not approved create a pending notification
            if (!approved) {
                // Create the inspired notification
                NSDictionary *params = @{@"postId":newPost.objectId,@"notificationType":@(NOTIFICATION_TYPE_PENDING)};
                [PFCloud callFunctionInBackground:@"notificationCreate" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    }
                    
                    NSDictionary *redFlagParams = @{@"postId":newPost.objectId};
                    [PFCloud callFunctionInBackground:@"redFlagKeyword" withParameters:redFlagParams block:^(id object, NSError *error) {
                        
                        if (error) {
                            NSLog(@"Error:%@",error);
                            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                        }
                        
                        shouldClose--;
                        if (shouldClose == 0) {
                            [postButton setEnabled:YES];
                            [progress stopAnimating];
                            [progress removeFromSuperview];
                            [self closePostScreen];
                        }
                        
                    }];
                }];
            }
            
            
            if (shouldClose == 0) {
                [postButton setEnabled:YES];
                [progress stopAnimating];
                [progress removeFromSuperview];
                [self closePostScreen];
            }
        } else {
            NSLog(@"Error:%@",error);
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
        }
    }];
}

- (IBAction)selectFeelings:(id)sender {
    if (textView.text.length == 0) {
        return;
    }
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    feelingsController = [sb instantiateViewControllerWithIdentifier:@"NoteSelectionViewController"];
    feelingsController.listMode = LIST_MODE_FEELINGS;
    feelingsController.delegate = self;
    [self.navigationController pushViewController:feelingsController animated:YES];
}

-(void)userSelectedNotes:(NSArray *)selectionStates
{
    [self.navigationController popViewControllerAnimated:YES];
    NSMutableArray *selectedNotes = [[NSMutableArray alloc] init];
    NSMutableArray *selectedNames = [[NSMutableArray alloc] init];
    NSArray *feelingModels = [AppDelegate getFeelingModels];
    for (int i=0; i<feelingModels.count; i++) {
        if ([[selectionStates objectAtIndex:i] boolValue]) {
            Feeling *feeling = [feelingModels objectAtIndex:i];
            [selectedNames addObject:feeling.name];    ///[[feelingModels objectAtIndex:i] objectForKey:@"name"]];
        }
    }
    NSString *currentText = textView.text;
    NSRange currentFeelings = [currentText rangeOfString:@" - Feeling "];
    NSMutableString *newFeelings = [[NSMutableString alloc] init];
    [newFeelings appendString:@" - Feeling "];
    BOOL first = YES;
    for (NSString *name in selectedNames) {
        if (!first)
            [newFeelings appendString:@", "];
        [newFeelings appendString:name];
        first = NO;
    }
    NSString *newText;
    if (currentFeelings.location != NSNotFound) {
        newText = [currentText substringToIndex:currentFeelings.location];
    }
    else {
        newText = [NSString stringWithString:currentText];
    }
    newText = [newText stringByAppendingString:newFeelings];
    textView.text = newText;
}

- (void)textViewDidChange:(UITextView *)_textView {
    postButton.enabled = (_textView.text.length > 0);
    accView.alpha = (_textView.text.length > 0) ? 1 : .5;
}

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    self.textView.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height - 95, 0);
    self.textView.scrollIndicatorInsets = self.textView.contentInset;
}

- (void)keyboardWillHide:(NSNotification*)notification {
    self.textView.contentInset = UIEdgeInsetsZero;
    self.textView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (IBAction)dismissKeyboard:(id)sender {
    [self.textView resignFirstResponder];
}
@end
