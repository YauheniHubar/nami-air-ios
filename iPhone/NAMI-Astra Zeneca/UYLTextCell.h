//
//  UYLTextCell.h
//  
//
//  Created by Adam Behnke on 11/26/14.
//
//

#import <UIKit/UIKit.h>
@interface UYLTextCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *leftBorder;
@property (nonatomic, weak) IBOutlet UILabel *rightBorder;
@property (nonatomic, weak) IBOutlet UILabel *lineLabel;
@property (nonatomic, weak) IBOutlet UIView  *backgroundMask;
@property (nonatomic, weak) IBOutlet UILabel *likeCount;
@property (nonatomic, weak) IBOutlet UILabel *hugCount;
@property (nonatomic, weak) IBOutlet UILabel *meTooCount;
@property (nonatomic, weak) IBOutlet UILabel *noteCount;
@property (nonatomic, weak) IBOutlet UILabel *meTooStoryCount;
@property (nonatomic, weak) IBOutlet UILabel *daysAgo;
@property (nonatomic, weak) IBOutlet UIButton *likeButton;
@property (nonatomic, weak) IBOutlet UIButton *hugButton;
@property (nonatomic, weak) IBOutlet UIButton *meTooButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;
@property (strong, nonatomic) IBOutlet UITextView *txtContent;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *pendingContainerHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *flagContainerHeight;
@property (strong, nonatomic) IBOutlet UIImageView *pendingImage;
@property (strong, nonatomic) IBOutlet UILabel *pendingLabel;
@property (strong, nonatomic) IBOutlet UILabel *lblFlagBanner;
@end