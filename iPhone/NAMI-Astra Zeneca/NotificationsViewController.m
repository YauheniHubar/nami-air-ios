//
//  NotificationsViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/15/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "konstant.h"
#import "NotificationsViewController.h"
#import "NotificationTableViewCell.h"
#import "PostsViewController.h"
#import "ModeratorMessageViewController.h"
#import "Parse/Parse.h"
#import "Models.h"
#import "AppDelegate.h"

@interface NotificationsViewController ()
{
    NSMutableArray *notifications;
    NSInteger unreadCount;
    
    NSDictionary *attrsDictionary;
    NSDictionary *attrsBoldDictionary;
    NSDictionary *attrsItalDictionary;
}

@end


@implementation NotificationsViewController

@synthesize notificationRefreshControl;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    notifications = [[NSMutableArray alloc] init];
    
    UIFont *font = [UIFont fontWithName:@"ProximaNova-Regular" size:16.0];
    UIFont *boldFont = [UIFont fontWithName:@"ProximaNova-Bold" size:16.0];
    UIFont *italicFont = [UIFont italicSystemFontOfSize:12.0f];
    attrsDictionary = [NSDictionary dictionaryWithObject:font
                                                                forKey:NSFontAttributeName];
    
    attrsBoldDictionary = [NSDictionary dictionaryWithObject:boldFont
                                                                    forKey:NSFontAttributeName];
    
    attrsItalDictionary = [NSDictionary dictionaryWithObject:italicFont
                                                                    forKey:NSFontAttributeName];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([PFUser currentUser]) {
        [self refreshButtonHandler:nil];
    }
}



- (void)refreshButtonHandler:(id)sender
{
    /*
    PFUser *user = [PFUser currentUser];
    //Create query for all Post object by the current user
    PFQuery *postQuery = [PFQuery queryWithClassName:@"PostNotification"];
    [postQuery whereKey:@"user" equalTo:user];
    
    [postQuery includeKey:@"post"];
    
    [postQuery orderByDescending:@"modifiedAt"];
    */
    
    __block UIActivityIndicatorView *progress = nil;
    
    if ([self.notificationRefreshControl isRefreshing] == NO) {
        
        progress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        progress.color = [UIColor darkGrayColor];
        [self.view addSubview:progress];
        progress.center = self.view.center;
        [progress startAnimating];
    }

    
    PFQuery *postQuery = [PostNotification queryForUsersNotifications];
    
    // Run the query
    [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //Save results and update the table
            
            NSMutableArray *moderatorNotifications = [[NSMutableArray alloc] init];
            NSMutableArray *otherNotifications = [[NSMutableArray alloc] init];
            
            for (PostNotification *notification in objects) {
                if (notification.notificationType == 7 && !notification.read) {
                    [moderatorNotifications addObject:notification];
                } else {
                    [otherNotifications addObject:notification];
                }
            }
            
            [notifications removeAllObjects];
            [notifications addObjectsFromArray:moderatorNotifications];
            [notifications addObjectsFromArray:otherNotifications];
            
            unreadCount = 0;
            
            // Get the delegate.
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            int moderatorMessage = -1;
            for (int index = 0; index < notifications.count; index++) {
                PostNotification *notification = [notifications objectAtIndex:index];
                if (!notification.read) unreadCount++;
                
                if (appDelegate.unreadNotificationId != nil && moderatorMessage < 0) {
                    if ([notification.objectId isEqualToString:appDelegate.unreadNotificationId]) {
                        moderatorMessage = index;
                        appDelegate.unreadNotificationId = nil;
                    }
                }
            }
            
            if (unreadCount > 0) {
                self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%ld",(long)unreadCount];
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:unreadCount];
            }
            else {
                self.navigationController.tabBarItem.badgeValue = nil;
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            }
            
            
            
            [self.tableView reloadData];
            
            if (self.notificationRefreshControl.isRefreshing)
                [self.notificationRefreshControl endRefreshing];
            
            [appDelegate loadModeratorFlagPosts];
            
            if (progress != nil) {
                [progress stopAnimating];
                [progress removeFromSuperview];
            }
            
            if (moderatorMessage >= 0) {
                [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:moderatorMessage inSection:0]];
            }
        } else {
            NSLog(@"Error : %@", error);
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
        }
    }];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    if (notifications != nil && notifications.count == 0) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 1;
    }
    else {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    
    return notifications.count;
}

/*
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}
*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (notifications.count == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"noresults" forIndexPath:indexPath];
        return cell;
    }
    
    PostNotification *notification = [notifications objectAtIndex:indexPath.row];
    
    NSInteger notificationType = [[notification objectForKey:@"notificationType"] integerValue];
    
    NotificationTableViewCell *cell = nil;
    if (notificationType == NOTIFICATION_TYPE_DIRECT)
        cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationCellModerator" forIndexPath:indexPath];
    else cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationCell" forIndexPath:indexPath];
    

    
    switch (notificationType) {
        case NOTIFICATION_TYPE_SOCIAL:
            [self configureSocialNotificationText:notification forCell:cell];
            break;
        case NOTIFICATION_TYPE_NOTES:
            [self configureNotesNotificationText:notification forCell:cell];
            break;
        case NOTIFICATION_TYPE_INSPIRED:
            [self configureInspiredNotificationText:notification forCell:cell];
            break;
        case NOTIFICATION_TYPE_PENDING:
            [self configureGeneralNotificationText:notification forCell:cell withSuffix:@"is pending"];
            break;
        case NOTIFICATION_TYPE_APPROVED:
            [self configureGeneralNotificationText:notification forCell:cell withSuffix:@"has been approved"];
            break;
        case NOTIFICATION_TYPE_DISAPPROVED:
            [self configureGeneralNotificationText:notification forCell:cell withSuffix:@"has been disapproved"];
            break;
        case NOTIFICATION_TYPE_DIRECT:
            [self configureModeratorNotificationText:notification forCell:cell];
            break;
        case NOTIFICATION_TYPE_CHANGE_GROUP:
            [self configureGroupChangeNotificationText:notification forCell:cell];
            break;
    }
    
    if (notification.read) {
        cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
        if (notificationType == NOTIFICATION_TYPE_DIRECT) {
            cell.lblText.textColor = [UIColor colorWithRed:(85.0f / 255.0f) green:(85.0f / 255.0f) blue:(85.0f / 255.0f) alpha:1.0];
            cell.lblImageView.image = [UIImage imageNamed:@"icon-info-lg-read"];
        }
    }
    else {
        if (notificationType == NOTIFICATION_TYPE_DIRECT) {
            cell.backgroundColor = [UIColor colorWithRed:(255.0 / 255.0) green:(99.0 / 255.0) blue:(48.0/255.0) alpha:1.0];
            cell.lblText.textColor = [UIColor whiteColor];
            cell.lblImageView.image = [UIImage imageNamed:@"icon-info-lg"];
        }
        else cell.backgroundColor = [UIColor colorWithRed:(200.0/255.0) green:(216.0/255.0) blue:(236.0/255.0) alpha:1.0];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (notifications.count == 0) {
        
        CGFloat tableHeight = self.tableView.bounds.size.height;
        return tableHeight - 65.0f;
    }
    
    return 64.0f;
}

#pragma mark - configure attributedMessage

-(CGFloat)sizeCellToLabel:(NotificationTableViewCell *)cell
{
    CGSize maximumLabelSize = CGSizeMake(290,CGFLOAT_MAX);
    CGSize requiredSize = [cell.lblText sizeThatFits:maximumLabelSize];
    
    cell.lblTextHeight.constant = requiredSize.height;
    
    return requiredSize.height;
}

- (CGFloat)configureSocialNotificationText:(PostNotification *)notification forCell:(NotificationTableViewCell *)cell
{
    NSUInteger numLikes;
    NSUInteger numHugs;
    NSUInteger numMeToo;
    
    numLikes = notification.likeCount;  //[[notification objectForKey:@"likeCount"] intValue];
    numHugs =  notification.hugCount;   //[[notification objectForKey:@"hugCount"] intValue];
    numMeToo = notification.meTooCount; //[[notification objectForKey:@"meTooCount"] intValue];

    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@"Your " attributes:attrsDictionary];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"post " attributes:attrsBoldDictionary]];
    
    NSMutableString *socialString;
    if (numHugs || numLikes || numMeToo) {
        socialString = [[NSMutableString alloc] initWithString:@"has "];
        if (numLikes) {
            if (numLikes > 1)
                [socialString appendFormat:@"%ld Likes", numLikes];
            else
                [socialString appendString:@"1 Like"];
            if (numHugs && numMeToo)
                [socialString appendString:@", "];
            else
                [socialString appendString:@" and "];
        }
        if (numHugs) {
            if (numHugs > 1)
                [socialString appendFormat:@"%ld Hugs", numHugs];
            else
                [socialString appendString:@"1 Hug"];
            if (numMeToo)
                [socialString appendString:@", and "];
        }
        if (numMeToo) {
            if (numMeToo > 1)
                [socialString appendFormat:@"%ld Me Toos", numMeToo];
            else
                [socialString appendString:@"1 Me Too"];
        }
        
    }
    else {
        socialString = [[NSMutableString alloc] initWithString:@"has no activity."];
    }
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:socialString attributes:attrsDictionary]];
    NSDate *modDate = [notification objectForKey:@"modifiedAt"];
    NSString *dateString = [NSString stringWithFormat:@". %@",[PostsViewController daysSinceDate:modDate]];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:dateString attributes:attrsItalDictionary]];
    
    cell.lblText.attributedText = attributedMessage;

    return [self sizeCellToLabel:cell];
}

- (CGFloat)configureNotesNotificationText:(PostNotification *)notification forCell:(NotificationTableViewCell *)cell
{
    NSUInteger numNotes = notification.noteCount;
    
    
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@"Your " attributes:attrsDictionary];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"post " attributes:attrsBoldDictionary]];
    
    NSMutableString *socialString;
    socialString = [[NSMutableString alloc] initWithString:@"has "];
    if (numNotes != 1)
        [socialString appendFormat:@"%ld Notes", numNotes];
    else
        [socialString appendString:@"1 Note"];
    
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:socialString attributes:attrsDictionary]];
    NSDate *modDate = [notification objectForKey:@"modifiedAt"];
    NSString *dateString = [NSString stringWithFormat:@". %@",[PostsViewController daysSinceDate:modDate]];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:dateString attributes:attrsItalDictionary]];
    
    
    cell.lblText.attributedText = attributedMessage;
    
    return [self sizeCellToLabel:cell];
}

- (CGFloat)configureInspiredNotificationText:(PostNotification *)notification forCell:(NotificationTableViewCell *)cell
{
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@"Your " attributes:attrsDictionary];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"post " attributes:attrsBoldDictionary]];
    
    NSString *inspiredStringFormat = nil;
    if (notification.inspiredByCount > 1)
        inspiredStringFormat = @"has %ld Inspired Posts";
    else inspiredStringFormat = @"has %ld Inspired Post";
    
    NSString *inspiredString = [NSString stringWithFormat:inspiredStringFormat,notification.inspiredByCount];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:inspiredString attributes:attrsDictionary]];
    NSDate *modDate = [notification objectForKey:@"modifiedAt"];
    NSString *dateString = [NSString stringWithFormat:@". %@",[PostsViewController daysSinceDate:modDate]];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:dateString attributes:attrsItalDictionary]];
    
    
    cell.lblText.attributedText = attributedMessage;
    
    return [self sizeCellToLabel:cell];
}


- (CGFloat)configureGeneralNotificationText:(PostNotification *)notification forCell:(NotificationTableViewCell *)cell withSuffix:(NSString *)suffix
{
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@"Your " attributes:attrsDictionary];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"post " attributes:attrsBoldDictionary]];
    
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:suffix attributes:attrsDictionary]];
    NSDate *modDate = [notification objectForKey:@"modifiedAt"];
    NSString *dateString = [NSString stringWithFormat:@". %@",[PostsViewController daysSinceDate:modDate]];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:dateString attributes:attrsItalDictionary]];
    
    
    cell.lblText.attributedText = attributedMessage;
    
    return [self sizeCellToLabel:cell];
}

- (CGFloat)configureModeratorNotificationText:(PostNotification *)notification forCell:(NotificationTableViewCell *)cell
{
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@"You have a message from the moderator." attributes:attrsDictionary];
    
    NSDate *modDate = [notification objectForKey:@"modifiedAt"];
    NSString *dateString = [NSString stringWithFormat:@". %@",[PostsViewController daysSinceDate:modDate]];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:dateString attributes:attrsItalDictionary]];
    
    
    cell.lblText.attributedText = attributedMessage;
    
    return [self sizeCellToLabel:cell];
}

- (CGFloat)configureGroupChangeNotificationText:(PostNotification *)notification forCell:(NotificationTableViewCell *)cell
{
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@"You have changed groups" attributes:attrsDictionary];
    //[attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"post " attributes:attrsBoldDictionary]];
    
    //[attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:suffix attributes:attrsDictionary]];
    NSDate *modDate = [notification objectForKey:@"modifiedAt"];
    NSString *dateString = [NSString stringWithFormat:@". %@",[PostsViewController daysSinceDate:modDate]];
    [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:dateString attributes:attrsItalDictionary]];
    
    
    cell.lblText.attributedText = attributedMessage;
    
    return [self sizeCellToLabel:cell];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostNotification *notification = [notifications objectAtIndex:indexPath.row];
    notification.read = YES;
    [notification saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [tableView beginUpdates];
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
        }
    }];
    
    NSInteger notificationType = notification.notificationType;
    
    switch (notificationType) {
        case NOTIFICATION_TYPE_SOCIAL:
        case NOTIFICATION_TYPE_NOTES:
        case NOTIFICATION_TYPE_PENDING:
        case NOTIFICATION_TYPE_APPROVED:
        case NOTIFICATION_TYPE_DISAPPROVED:
            [self showPostForNotification:notification];
            break;
        case NOTIFICATION_TYPE_INSPIRED:
            [self showInspiredPostForNotification:notification];
            break;
        case NOTIFICATION_TYPE_DIRECT:
            [self showModeratorNotification:notification];
            break;
        case NOTIFICATION_TYPE_CHANGE_GROUP:
            [self refreshButtonHandler:nil];
            break;
    }
    
}

-(void)showPostForNotification:(PostNotification *)notification
{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
    vc.postArray = @[notification.post];
    vc.chosenPostIndex = 0;
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)showInspiredPostForNotification:(PostNotification *)notification
{
    
    // Check the user type.
    PFObject *post = [notification post];
    NSInteger currentUserType = [[[PFUser currentUser] objectForKey:@"userType"] integerValue];
    NSInteger postUserType = [[post objectForKey:@"userType"] integerValue];
    if (currentUserType != postUserType) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incorrect Group" message:@"You may not view this post because it belongs to a different user group." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
    vc.postArray = @[notification.post];
    vc.chosenPostIndex = 0;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)showModeratorNotification:(PostNotification *)notification
{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ModeratorMessageViewController *vc = [sb instantiateViewControllerWithIdentifier:@"ModeratorMessageViewController"];
    vc.notification = notification;
    self.navigationItem.backBarButtonItem.title = @"Back";
    [self.navigationController pushViewController:vc animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(IBAction)refresh:(id)sender {
    
    [self refreshButtonHandler:nil];
}

@end
