//
//  Note.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "Note.h"
#import <Parse/PFObject+Subclass.h>

@implementation Note

@dynamic displayName,name,nameSearchable;

+(NSString *)parseClassName {
    return @"Note";
}


@end
