//
//  GetHelpNowTableViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 12/31/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "GetHelpNowTableViewController.h"

@interface GetHelpNowTableViewController () <UIAlertViewDelegate>
@property (nonatomic,strong) NSString *callNumber;
@end

@implementation GetHelpNowTableViewController

@synthesize callNumber;

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)makeCall:(UIButton*)sender {
    NSString *callTitle;
    if (sender.tag == 1) {
        callNumber = @"tel://18009506264";
        callTitle = @"Would you like to place a call to the NAMI Helpline now?";
    } else if (sender.tag == 2) {
        callNumber = @"tel://18002738255";
        callTitle = @"Would you like to place a call to the National Suicide Prevention Lifeline now?";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:callTitle delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callNumber]];
    }
}

- (IBAction)emailNAMI:(id)sender {
    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
    mailer.mailComposeDelegate = self;
    [mailer setToRecipients:@[@"info@nami.org"]];
    [self presentViewController:mailer animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
