//
//  ChooseGroupDelegate.h
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/11/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#ifndef NAMI_Astra_Zeneca_ChooseGroupDelegate_h
#define NAMI_Astra_Zeneca_ChooseGroupDelegate_h

@protocol ChooseGroupDelegate <NSObject>

@optional

-(void) chooseGroupCloseButtonClicked:(id)sender;

@end

#endif
