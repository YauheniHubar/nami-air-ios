//
//  NoteSelectionViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/11/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

#define LIST_MODE_NOTES    1
#define LIST_MODE_FEELINGS 2

@protocol NoteSelectionDelegate <NSObject>

-(void)userSelectedNotes:(NSArray *)selectionStates;

@end

@interface NoteSelectionViewController : UITableViewController

@property (strong,nonatomic) id<NoteSelectionDelegate> delegate;
@property (assign) NSUInteger listMode;

@end
