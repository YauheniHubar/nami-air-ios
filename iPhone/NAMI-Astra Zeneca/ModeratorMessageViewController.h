//
//  ModeratorMessageViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/9/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@class PFObject;

@interface ModeratorMessageViewController : UIViewController

-(IBAction)headerClicked:(id)sender;


@property (nonatomic, weak) PFObject *notification;
@property (nonatomic, weak) IBOutlet UIView *header;
@property (nonatomic, weak) IBOutlet UILabel *headerLabel;
@property (nonatomic, weak) IBOutlet UITextView *message;
@end
