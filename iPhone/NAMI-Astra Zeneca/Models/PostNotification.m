//
//  PostNotification.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostNotification.h"
#import <Parse/PFObject+Subclass.h>

@implementation PostNotification

@dynamic displayName,read,likeCount,hugCount,meTooCount,noteCount,inspiredByCount,message,notificationType,modificationDate,post,user;

+(NSString *)parseClassName {
    return @"PostNotification";
}

+(PFQuery *)queryForUsersNotifications {
    PFUser *user = [PFUser currentUser];
    //Create query for all Post object by the current user
    PFQuery *postQuery = [PostNotification query];//[PFQuery queryWithClassName:@"PostNotification"];
    [postQuery whereKey:@"user" equalTo:user];
    
    [postQuery includeKey:@"post"];
    
    [postQuery orderByDescending:@"modifiedAt"];
    return postQuery;
}

+(PFQuery *)queryForUnreadModeratorotifications {
    PFUser *user = [PFUser currentUser];
    //Create query for all Post object by the current user
    PFQuery *postQuery = [PostNotification query];//[PFQuery queryWithClassName:@"PostNotification"];
    [postQuery whereKey:@"user" equalTo:user];
    [postQuery whereKey:@"read" equalTo:[NSNumber numberWithBool:NO]];
    [postQuery whereKey:@"notificationType" equalTo:[NSNumber numberWithInt:7]];
    
    [postQuery includeKey:@"post"];
    
    [postQuery orderByDescending:@"modifiedAt"];
    return postQuery;
}

-(NSDate *)modificationDate {
    return self.updatedAt;
}

@end
