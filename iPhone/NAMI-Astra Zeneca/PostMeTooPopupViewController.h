//
//  PostMeTooPopupViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/18/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostPopupDelegate.h"



@interface PostMeTooPopupViewController : UIViewController

@property (strong,nonatomic) id<PostPopupDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *btnYes;
@property (strong, nonatomic) IBOutlet UIButton *btnNo;
- (IBAction)yesButtonClicked:(id)sender;
- (IBAction)noButtonClicked:(id)sender;

@end
