//
//  NotificationTableViewCell.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/16/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblText;
@property (strong, nonatomic) IBOutlet UIImageView *lblImageView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblTextHeight;

@end
