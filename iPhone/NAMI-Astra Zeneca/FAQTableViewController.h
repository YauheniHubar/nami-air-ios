//
//  FAQTableViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 12/27/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *data;

@end