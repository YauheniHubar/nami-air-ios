//
//  PostDetailView.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/3/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostDetailView.h"
#import "PostsViewController.h"
#import "PostDetailsViewController.h"
#import "PostMeTooPopupViewController.h"
#import "AppDelegate.h"
#import "konstant.h"

NSString * const hashTagKey = @"HashTag";
NSString * const normalKey = @"NormalKey";
NSString * const wordType = @"WordType";

extern NSArray *hashTagNoteNames;

@interface PostDetailView()
{
    
    PostDetailsViewController *parentViewController;
    PostFlaggedPopupViewController *pfvc;
    PostMeTooPopupViewController *pmtvc;
    NSInteger noteCount;
}
@end

@implementation PostDetailView

@synthesize containerView,scrollView,flagBannerHeight,post,txtPostContent,postContentHeight,tagContainerView,tagsContainerHeight,inspiredContainerView, inspiredContainerHeight;
@synthesize btnLike,btnHug,btnMeToo,activityView,lblFlagBanner,notes, skipLoading, moderatorFlagBannerHeight, moderatorLabel;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithCoder:(NSCoder *)aDecoder
{
    return [super initWithCoder:aDecoder];
}

-(instancetype)initWithFrame:(CGRect)frame
{
    return [super initWithFrame:frame];
}

- (void)setupView:(PFObject *)thePost :(PostDetailsViewController *)viewController
{
    parentViewController = viewController;
    
    btnMeToo.layer.cornerRadius = 3.0f;
    btnLike.layer.cornerRadius = 3.0f;
    btnHug.layer.cornerRadius = 3.0f;
    
    /*
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
    */
    //notes = nil;
    NSArray *tagViews = tagContainerView.subviews;
    for (UIView *v in tagViews) {
        [v removeFromSuperview];
    }
    if (thePost) {
        UIColor *postBlue = [UIColor colorWithRed:23.0f/255.0f green:135.0f/255.0f blue:151.0f/255.0f alpha:1.0];
        UIColor *postGray = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0];
        UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTapped:)];
        post = thePost;
        flagBannerHeight.constant = 34;
        NSInteger approval = [[post objectForKey:@"approvedStatus"] integerValue];
        if (approval != POST_APPROVAL_APPROVED) {
            if (approval == 0) {
                lblFlagBanner.text = @"Your post is pending.";
                lblFlagBanner.backgroundColor = [UIColor colorWithRed:(252.0/255.0) green:(78.0/255.0) blue:(23.0/255.0) alpha:1.0];
            } else if (approval == 2) {
                lblFlagBanner.text = @"Your post has been disapproved.";
                lblFlagBanner.backgroundColor = [UIColor redColor];
            }
            else flagBannerHeight.constant = 0;
        } else flagBannerHeight.constant = 0;
        
        NSInteger flagged = [AppDelegate getFlaggedPostStatus:post];
        NSInteger moderatorFlagged = [AppDelegate getModeratorFlaggedPostStats:post];
        if (moderatorFlagged != 0 && ((AppDelegate *)[UIApplication sharedApplication].delegate).selectedFeed == 1) {
            moderatorFlagBannerHeight.constant = 34;
            moderatorLabel.text = [self constructFlaggedText:moderatorFlagged format:@"Your post was flagged as %@."];
        } else if (flagged != 0) {
            moderatorFlagBannerHeight.constant = 34;
            moderatorLabel.text = [self constructFlaggedText:flagged format:@"You flagged this post as %@."];
        } else moderatorFlagBannerHeight.constant = 0;
        
        
        tagsContainerHeight.constant = 0;
        
        // Set up the post date stamp
        UIFont *font = [UIFont fontWithName:@"ProximaNova-SemiboldIt" size:16.0];
        NSMutableDictionary *attrsDictionary = [[NSMutableDictionary alloc] init];
        [attrsDictionary setObject:font forKey:NSFontAttributeName];
        [attrsDictionary setObject:postGray forKey:NSForegroundColorAttributeName];
        NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:[PostsViewController daysSinceDate:post.createdAt] attributes:attrsDictionary];
        self.lblPostTime.attributedText = attributedMessage;
        
        id obj = [post objectForKey:@"inspiredByPost"];
        if (obj) {
            // Set up inspired by
            UIFont *font = [UIFont fontWithName:@"MuseoSlab-300" size:16.0];
            NSMutableDictionary *attrsDictionary = [[NSMutableDictionary alloc] init];
            [attrsDictionary setObject:font forKey:NSFontAttributeName];
            [attrsDictionary setObject:postGray forKey:NSForegroundColorAttributeName];
            NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@"Inspired by " attributes:attrsDictionary];

            [attrsDictionary setObject:postBlue forKey:NSForegroundColorAttributeName];
            [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"this post" attributes:attrsDictionary]];
            UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(inspirationPostTapped)];
            [self.lblInspirationPost addGestureRecognizer:gr];
            self.lblInspirationPost.userInteractionEnabled = YES;
            self.lblInspirationPost.attributedText = attributedMessage;
        }
        else {
            self.lblInspirationPost.hidden = YES;
        }
        
        int meTooStoryCount = [[post objectForKey:@"meTooStoryCount"] intValue];
        if (meTooStoryCount > 0) {
            // Set up inspired by
            UIFont *font = [UIFont fontWithName:@"MuseoSlab-300" size:16.0];
            NSMutableDictionary *attrsDictionary = [[NSMutableDictionary alloc] init];
            [attrsDictionary setObject:font forKey:NSFontAttributeName];
            [attrsDictionary setObject:postGray forKey:NSForegroundColorAttributeName];
            NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@"This post inspired " attributes:attrsDictionary];
            
            NSString *inspiredPosts = (meTooStoryCount > 1) ? @"posts" : @"post";
            [attrsDictionary setObject:postBlue forKey:NSForegroundColorAttributeName];
            [attributedMessage appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d %@",meTooStoryCount,inspiredPosts] attributes:attrsDictionary]];
            UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(inspiredPostsTapped)];
            [self.lblInspiredPosts addGestureRecognizer:gr];
            self.lblInspiredPosts.userInteractionEnabled = YES;
            self.lblInspiredPosts.attributedText = attributedMessage;
        }
        else {
            self.inspiredContainerView.hidden = YES;
            self.inspiredContainerHeight.constant = 0;
        }
        
        [self updatePostStats];
        
        // Set up the post content
        [self configurePostText];
        [txtPostContent addGestureRecognizer:gr];
        
        //[self getSocialStatusForPost:nil];
        if (self.skipLoading == YES) {
            self.skipLoading = NO;
            [self updatePostHashNotes];            
        } else {
            
            [self getHashNotesForPost:nil];
        }
        
        
        //Uncomment this if we need to hide these controls for the user's own posts
        PFUser *postUser = [post objectForKey:@"user"];
        if ([postUser.objectId isEqualToString:[[PFUser currentUser] objectId]]) {
            //statsContainerView.hidden = YES;
            btnMeToo.enabled = NO;
            btnLike.enabled = NO;
            btnHug.enabled = NO;
            
        }
        
        
        [self layoutIfNeeded];
    }
}

-(void)updatePostStats
{
    //self.lblNumLinks.text = [[post objectForKey:@"meTooStoryCount"] stringValue];
    
    [btnLike setTitle:[[post objectForKey:@"likeCount"] stringValue] forState:UIControlStateNormal];
    [btnHug setTitle:[[post objectForKey:@"hugCount"] stringValue] forState:UIControlStateNormal];
    [btnMeToo setTitle:[[post objectForKey:@"meTooCount"] stringValue] forState:UIControlStateNormal];
    
    // Set the hash tags.
    NSInteger hashTagCount = [[post objectForKey:@"noteCount"] integerValue];
    noteCount = hashTagCount;
    NSString *  hashTagText = nil;
    if (hashTagCount > 1 || hashTagCount == 0)
        hashTagText = @"%d Hashnotes";
    else hashTagText = @"%d Hashnote";
    
    // Set the social status.
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    if ([appDelegate hasPostLike:post.objectId]) {
        [btnLike setSelected:YES];
        btnLike.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
    } else {
        [btnLike setSelected:NO];
        btnLike.backgroundColor = [UIColor colorWithRed:0.86 green:0.86 blue:0.86 alpha:1];
    }
    
    if ([appDelegate hasPostHug:post.objectId]) {
        [btnHug setSelected:YES];
        btnHug.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
    } else {
        [btnHug setSelected:NO];
        btnHug.backgroundColor = [UIColor colorWithRed:0.86 green:0.86 blue:0.86 alpha:1];
    }
    
    if ([appDelegate hasPostMeToo:post.objectId]) {
        [btnMeToo setSelected:YES];
        btnMeToo.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
    } else {
        [btnMeToo setSelected:NO];
        btnMeToo.backgroundColor = [UIColor colorWithRed:0.86 green:0.86 blue:0.86 alpha:1];
    }
    
    
    self.lblNumHashTags.text = [NSString stringWithFormat:hashTagText, hashTagCount];
}

-(void)updatePostHashNotes {
    UIColor *postBlue = [UIColor colorWithRed:23.0f/255.0f green:135.0f/255.0f blue:151.0f/255.0f alpha:1.0];
    UIColor *postGray = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    int viewOffset = 0;
    for (int i = 0; i<notes.count; i++) {
        UIView *sepView = [[UIView alloc] initWithFrame:CGRectMake(0, viewOffset, tagContainerView.frame.size.width, 1)];
        sepView.backgroundColor = postGray;
        if (i > 0) [tagContainerView addSubview:sepView];
        viewOffset += 10;
        
        PFObject *postNote = [notes objectAtIndex:i];
        NSDate *createdAt = postNote.createdAt;//[postNote objectForKey:@"createdAt"];
        UIFont *font = [UIFont fontWithName:@"ProximaNova-SemiboldIt" size:16.0];
        NSMutableDictionary *attrsDictionary = [[NSMutableDictionary alloc] init];
        [attrsDictionary setObject:font forKey:NSFontAttributeName];
        [attrsDictionary setObject:postGray forKey:NSForegroundColorAttributeName];
        NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n",[PostsViewController daysSinceDate:createdAt]] attributes:attrsDictionary];
        
        UIFont *font2 = [UIFont fontWithName:@"ProximaNova-Regular" size:20.0];
        [attrsDictionary setObject:font2 forKey:NSFontAttributeName];
        [attrsDictionary setObject:postBlue forKey:NSForegroundColorAttributeName];
        NSArray *noteObjList = [postNote objectForKey:@"notes"];
        for (int j=0; j<noteObjList.count; j++) {
            //PFObject *tmp = [noteObjList objectAtIndex:j];
            NSString *name = [noteObjList objectAtIndex:j];
            name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

            NSMutableAttributedString *hashStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",name] attributes:attrsDictionary];
            [hashStr addAttributes:@{wordType: @"HashNote", @"HashNote":name} range:NSMakeRange(0,hashStr.length)];
            [attributedMessage appendAttributedString:hashStr];
            NSMutableAttributedString *hashStrSpace = [[NSMutableAttributedString alloc] initWithString:@" " attributes:attrsDictionary];
            [attributedMessage appendAttributedString:hashStrSpace];
            
        }
        NSMutableAttributedString *hashStr = [[NSMutableAttributedString alloc] initWithString:@"  " attributes:attrsDictionary];
        [attributedMessage appendAttributedString:hashStr];
        UITextView *tv = [[UITextView alloc] initWithFrame:CGRectMake(0, viewOffset, tagContainerView.frame.size.width, 512)];
        tv.editable = NO;
        tv.selectable = NO;
        tv.scrollEnabled = NO;
        tv.attributedText = attributedMessage;
        CGSize maximumLabelSize = CGSizeMake(tagContainerView.frame.size.width,CGFLOAT_MAX);
        CGSize requiredSize = [tv sizeThatFits:maximumLabelSize];
        tv.frame = CGRectMake(0,viewOffset, tv.frame.size.width, requiredSize.height);
        UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTapped:)];
        [tv addGestureRecognizer:gr];
        [tagContainerView addSubview:tv];
        viewOffset += tv.frame.size.height + 10;
        
        //NSLog(@"%@",noteObjList);
    }
    tagsContainerHeight.constant = viewOffset;
    UIScrollView *sv = (UIScrollView *)self.superview;
    if (sv) {
        [self layoutIfNeeded];
        if (viewOffset > 0) {
            CGSize sz = self.frame.size;
            sz.height = tagContainerView.frame.origin.y + tagsContainerHeight.constant + 152;
            sv.contentSize = sz;
        }
        else {
            CGSize sz = self.frame.size;
            sz.height = inspiredContainerView.frame.origin.y + inspiredContainerView.frame.size.height + 152;
            sv.contentSize = sz;
        }
    }
}

- (void)getHashNotesForPost:(id)sender
{
    PFUser *user = [PFUser currentUser];
    //Create query for all Post object by the current user
    PFQuery *postQuery = [PFQuery queryWithClassName:@"PostNote"];
    [postQuery whereKey:@"post" equalTo:post];

    [postQuery includeKey:@"notes"];
    
    [postQuery orderByDescending:@"updatedAt"];
    
    // Run the query
    [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //Save results and update the table
            notes = (NSMutableArray *)objects;
            [self updatePostHashNotes];
        }
    }];
}

/*
- (void)getSocialStatusForPost:(id)sender
{

    PFUser *user = [PFUser currentUser];
    NSDictionary *params = @{@"postId":post.objectId};
    
    [PFCloud callFunctionInBackground:@"fetchUserPostSocialStatus" withParameters:params block:^(id object, NSError *error) {
        if (!error) {
            NSDictionary *results = (NSDictionary *)object;
            socialPostLike = [results objectForKey:@"PostLike"];
            socialPostHug = [results objectForKey:@"PostHug"];
            socialPostMeToo = [results objectForKey:@"PostMeToo"];
            [self updatePostStats];
        }
        
    }];

}
*/



- (void)configurePostText
{
    NSString *rawText = [post objectForKey:@"text"];
    
    //format string
    txtPostContent.attributedText = [self attributedMessageFromMessage:rawText];
    
    CGSize maximumLabelSize = CGSizeMake(txtPostContent.frame.size.width,CGFLOAT_MAX);
    CGSize requiredSize = [txtPostContent sizeThatFits:maximumLabelSize];
    
    postContentHeight.constant = requiredSize.height;
    
    txtPostContent.textContainerInset = UIEdgeInsetsZero;
    txtPostContent.textContainer.lineFragmentPadding = 0;
}


#pragma mark - configure attributedMessage

- (NSAttributedString *)attributedMessageFromMessage:(NSString *)message {
    

    UIColor *postBlue = [UIColor colorWithRed:23.0f/255.0f green:135.0f/255.0f blue:151.0f/255.0f alpha:1.0];
    UIColor *postGreen = [UIColor colorWithRed:103.0f/255.0f green:138.0f/255.0f blue:2.0f/255.0f alpha:1.0];
    UIColor *postGray = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    
    message = [message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //NSLog(@"message: %@", message);
    NSArray* messageWords = [message componentsSeparatedByString: @" "];
    
    UIFont *firstFont = [UIFont fontWithName:@"MuseoSlab-300" size:22.0];
    UIFont *font = [UIFont fontWithName:@"ProximaNova-Regular" size:18.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font
                                                                forKey:NSFontAttributeName];
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@"" attributes:attrsDictionary];
    NSInteger sentenceFlag = 0;
    for (NSString *partialWord in messageWords) {
        
        unsigned long previousIndex = 0;
        unsigned long newlineIndex = 0;
        NSMutableArray *partialWords = [[NSMutableArray alloc] init];
        while (newlineIndex < partialWord.length && [partialWord rangeOfString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(newlineIndex, partialWord.length - newlineIndex) ].location != NSNotFound) {
            newlineIndex = [partialWord rangeOfString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(newlineIndex, partialWord.length - newlineIndex) ].location;
            if (newlineIndex - previousIndex > 0) {
                [partialWords addObject:[partialWord substringWithRange:NSMakeRange(previousIndex, newlineIndex - previousIndex)]];
            }
            [partialWords addObject:@"\n"];
            newlineIndex++;
            previousIndex = newlineIndex;
        }
        
        if (partialWords.count == 0)
            [partialWords addObject:partialWord];
        else {
            
            if (newlineIndex < partialWord.length) {
                [partialWords addObject:[partialWord substringWithRange:NSMakeRange(newlineIndex, partialWord.length - newlineIndex)]];
            }
        }
        
        for (NSString *word in partialWords) {
            
            NSDictionary *attributes;
            
            //NSLog(@"word: ::%@::%@", word, [word substringToIndex:1]);
            
            if ([word length] == 0) {
                //NSLog(@"WHITESPACE ERROR");
            }
            else if ([[word substringToIndex:1] isEqualToString:@"#"]) {
                //if([word characterAtIndex:0] == '#'){
                attributes = @{NSForegroundColorAttributeName:postBlue, wordType: hashTagKey, hashTagKey:[word substringFromIndex:1], NSFontAttributeName:font};
                
            }
            else {
                if (sentenceFlag) {
                    attributes = @{NSForegroundColorAttributeName:[UIColor blackColor], wordType: normalKey, NSFontAttributeName:font};
                } else {
                    attributes = @{NSForegroundColorAttributeName:postGreen, wordType: normalKey, NSFontAttributeName:firstFont};
                }
                
                if ([[word substringFromIndex: [word length] - 1] isEqualToString:@"."]) {
                    sentenceFlag = 1;
                }
                
            }
            
            NSAttributedString * subString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",word] attributes:attributes];
            [attributedMessage appendAttributedString:subString];
        }
        
    }
    
    // Find the feeling -
    NSString *currentMessage = [attributedMessage string];
    NSRange feelingRange = [currentMessage rangeOfString:@"- Feeling" options:NSBackwardsSearch];
    if (feelingRange.length > 0) {
        
        NSDictionary *attributes = @{NSForegroundColorAttributeName:[UIColor blackColor], wordType: normalKey, NSFontAttributeName:font};
        NSRange range = NSMakeRange(feelingRange.location, currentMessage.length - feelingRange.location);
        [attributedMessage setAttributes:attributes range:range];
    }
    
    //
    //    //split string
    //    NSArray* textFormat = [message componentsSeparatedByString: @"."];
    //    NSString* firstSentence = [textFormat objectAtIndex: 0];
    //
    //    // set attributed text properties
    //    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:message];
    //    NSInteger _stringLength = [firstSentence length];
    //
    //
    //    //TODO: font size not working
    //    UIFont *font = [UIFont fontWithName:@"Arial" size:50.0];
    //
    //    [attString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, _stringLength)];
    //    [attString addAttribute:NSForegroundColorAttributeName
    //        value:postGreen range:NSMakeRange(0, _stringLength)];
    
    //[attributedMessage addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, attributedMessage.length)];
    
    return attributedMessage;
}

- (void)textTapped:(UITapGestureRecognizer *)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        NSDictionary *attrs = [textView.textStorage attributesAtIndex:characterIndex effectiveRange:&range];
        
        NSString *wordType  = [attrs objectForKey:@"WordType"];
        if ([wordType isEqualToString:@"HashTag"]) {
            //[self userEnteredSearchTerm:[attrs objectForKey:@"HashTag"]];
            NSString *term = [attrs objectForKey:@"HashTag"];
            [self userEnteredSearchTerm:term searchField:@"hashtags"];
        }
        else if ([wordType isEqualToString:@"HashNote"]) {
            //[self userEnteredSearchTerm:[attrs objectForKey:@"HashTag"]];
            NSString *term = [attrs objectForKey:@"HashNote"];
            [self userEnteredSearchTerm:term searchField:@"hashnotes"];
        }
    }
}

-(void)userEnteredSearchTerm:(NSString *)term searchField:(NSString *)searchField
{
    if (!term || term.length == 0) return;
    term = [term lowercaseString];
    term = [term stringByReplacingOccurrencesOfString:@"#" withString:@""];
    term = [@"#" stringByAppendingString:term];
    term = [term stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    term = [term stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
    UINavigationController *nav = parentViewController.navigationController;
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PostsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostsViewController"];
    vc.searchTerm = term;
    vc.searchField = searchField;
    [nav pushViewController:vc animated:YES];
}

- (IBAction)markPost:(id)sender {

    int tag = 1;
    PFUser *postUser = [post objectForKey:@"user"];
    UIActionSheet *popup;
    if ([[postUser objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
        tag = 2;
        popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete Post" otherButtonTitles:nil];
    }
    else {
        popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                 @"Flag Post",
                 @"Hide Post",
                 @"Block Author",
                 nil];
    }
    popup.tag = tag;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self flagPost];
                    break;
                case 1:
                    [self hidePost];
                    break;
                case 2:
                    [self blockAuthor];
                    break;
                default:
                    break;
            }
            break;
            
        }
        case 2: {
            switch (buttonIndex) {
                case 0:
                    [self deletePost];
                    break;
                default:
                    break;
            }
            break;
        }
        case 3: {
            switch (buttonIndex) {
                case 0:
                    [self confirmedFlagPost:POST_FLAG_INAPPROPRIATE];
                    break;
                case 1:
                    [self confirmedFlagPost:POST_FLAG_SELF_HARM];
                    break;
                case 2:
                    [self confirmedFlagPost:POST_FLAG_THREATENING];
                    break;
                case 3:
                    [self confirmedFlagPost:POST_FLAG_BULLYING];
                    break;
                case 4:
                    [self confirmedFlagPost:POST_FLAG_SPAM];
                    break;
                default:
                    break;
            }
            break;
            
        }
        default:
            break;
    }
}

-(void)deletePost {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Post?"
                                                    message:@"Are you sure you want to delete this post?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Delete", nil];
    alert.tag = 1;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        if (buttonIndex) {
            // delete the post
            
            
            NSDictionary *params = @{@"postId":post.objectId};
            [PFCloud callFunctionInBackground:@"deletePost" withParameters:params block:^(id object, NSError *error) {
                if (error) {
                    NSLog(@"Error:%@",error);
                    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                }
               
                activityView.hidden = NO;
                [parentViewController.navigationController popViewControllerAnimated:YES];
            }];        
        }
    }
}

-(void)flagPost {
    [[PFUser currentUser] setObject:@[] forKey:@"postsHidden"];
    [[PFUser currentUser] setObject:@[] forKey:@"authorsBlocked"];
    
    [[PFUser currentUser] saveInBackground];
    UIActionSheet *popup;
    popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                 @"Inappropriate",
                 @"Self-Harm",
                 @"Threatening",
                 @"Bullying",
                 @"Spam",
                 nil];
    popup.tag = 3;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
    
}

-(void)confirmedFlagPost:(int)flag {
    NSDictionary *params = @{@"postId":post.objectId,@"flaggedStatus":@(flag)};
    [PFCloud callFunctionInBackground:@"flagPost" withParameters:params block:^(id object, NSError *error) {
        if (error) {
            NSLog(@"Error:%@",error);
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
        }
        else {
            [AppDelegate updatePostFlagStatus:^(BOOL completed) {
                if (completed) {
                    
                    pfvc = [[PostFlaggedPopupViewController alloc] initWithNibName:@"PostFlaggedPopupViewController" bundle:nil];
                    [parentViewController.view addSubview:pfvc.view];
                    pfvc.delegate = parentViewController;
                    pfvc.view.center = parentViewController.view.center;
                }
                
            }];
        }
    }];
    
}

-(void)hidePost {
    NSMutableArray *hiddenList = [[[PFUser currentUser] objectForKey:@"postsHidden"] mutableCopy];
    if (hiddenList == nil) {
        hiddenList = [[NSMutableArray alloc] init];
    }
    [hiddenList addObject:post.objectId];
    [[PFUser currentUser] setObject:hiddenList forKey:@"postsHidden"];
    [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL success, NSError *error) {
        if (success) {
            
            
            NSDictionary *params = @{@"postId":post.objectId};
            [PFCloud callFunctionInBackground:@"hidePost" withParameters:params block:^(id object, NSError *error) {
                if (error) {
                    
                    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Post Hide Error"
                                                                    message:error.description
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    */
                }
                
                [parentViewController.delegate userHidPost:post];
            }];
        }
        else {
            
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
            
            /*
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Post Hide Error"
                                                            message:error.description
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            */
            
        }
    }];
}

-(void)blockAuthor {
    NSMutableArray *blockedist = [[[PFUser currentUser] objectForKey:@"authorsBlocked"] mutableCopy];
    if (blockedist == nil) {
        blockedist = [[NSMutableArray alloc] init];
    }
    [blockedist addObject:[post objectForKey:@"user"]];
    [[PFUser currentUser] setObject:blockedist forKey:@"authorsBlocked"];
    [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL success, NSError *error) {
        if (success) {
            
            PFUser *blockedAuthor = [post objectForKey:@"user"];
            
            NSDictionary *params = @{@"userId":blockedAuthor.objectId};
            [PFCloud callFunctionInBackground:@"blockAuthor" withParameters:params block:^(id object, NSError *error) {
                if (error) {
                    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    
                    /*
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Post Block Error"
                                                                    message:error.description
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                    */
                }
                
                [parentViewController.delegate userBlockedAuthorOfPost:post];
            }];
            
        }
        else {
            
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
            /*
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Post Block Error"
                                                            message:error.description
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            */
            
        }
    }];
}

- (IBAction)likePost:(id)sender {
    
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    btnLike.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
    
    __block PFObject *socialPostLike = [appDelegate getPostLike:post.objectId];
    
    // Disable the button.
    __block UIButton *button = (UIButton *)sender;
    button.enabled = NO;
    
    if (socialPostLike != nil) {
        [socialPostLike deleteInBackgroundWithBlock:^(BOOL success, NSError *error){
            if (success) {
                btnLike.selected = NO;
                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubType":@(NOTIFICATION_SUBTYPE_LIKE)};
                [PFCloud callFunctionInBackground:@"notificationDelete" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    } else {
                        
                        int likeCnt = [[post objectForKey:@"likeCount"] intValue];
                        likeCnt--;
                        [post setObject:@(likeCnt) forKey:@"likeCount"];
                        [appDelegate removePostLike:socialPostLike];
                        [self updatePostStats];
                        
                    }
                }];
            }
            
            // Enable the button.
            button.enabled = YES;
        }];
        
        
    }
    else {
        PFObject *postLike = [PFObject objectWithClassName:@"PostLike"];
        [postLike setObject:post forKey:@"post"];
        [postLike setObject:[PFUser currentUser] forKey:@"user"];
        [postLike saveInBackgroundWithBlock:^(BOOL success, NSError *error){
            if (success) {
                socialPostLike = postLike;
                btnLike.selected = YES;
                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubType":@(NOTIFICATION_SUBTYPE_LIKE)};
                [PFCloud callFunctionInBackground:@"notificationCreate" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    } else {
                        
                        int likeCnt = [[post objectForKey:@"likeCount"] intValue];
                        likeCnt++;
                        [post setObject:@(likeCnt) forKey:@"likeCount"];
                        [appDelegate addUserPostLikes:@[postLike]];
                        [self updatePostStats];

                    }
                }];
            }
            
            // Enable the button.
            button.enabled = YES;
        }];
        
    }

}

- (IBAction)hugPost:(id)sender {
    
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    btnHug.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
    
    __block PFObject *socialPostHug = [appDelegate getPostHug:post.objectId];
    
    // Disable the button.
    __block UIButton *button = (UIButton *)sender;
    button.enabled = NO;
    
    if (socialPostHug) {
        [socialPostHug deleteInBackgroundWithBlock:^(BOOL success, NSError *error){
            if (success) {

                btnHug.selected = NO;
                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubType":@(NOTIFICATION_SUBTYPE_HUG)};
                [PFCloud callFunctionInBackground:@"notificationDelete" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    } else {
                        int cnt = [[post objectForKey:@"hugCount"] intValue];
                        cnt--;
                        [post setObject:@(cnt) forKey:@"hugCount"];
                        [appDelegate removePostHug:socialPostHug];
                        [self updatePostStats];

                    }
                }];
            }
            
            // Enable the button.
            button.enabled = YES;
        }];
        
        
    }
    else {
        PFObject *postHug = [PFObject objectWithClassName:@"PostHug"];
        [postHug setObject:post forKey:@"post"];
        [postHug setObject:[PFUser currentUser] forKey:@"user"];
        [postHug saveInBackgroundWithBlock:^(BOOL success, NSError *error){
            if (success) {
                btnHug.selected = YES;
                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubType":@(NOTIFICATION_SUBTYPE_HUG)};
                [PFCloud callFunctionInBackground:@"notificationCreate" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    } else {
                        
                        int cnt = [[post objectForKey:@"hugCount"] intValue];
                        cnt++;
                        [post setObject:@(cnt) forKey:@"hugCount"];
                        [appDelegate addUserPostHugs:@[postHug]];
                        [self updatePostStats];

                    }
                }];
            }
            
            // Enable the button.
            button.enabled = YES;
        }];
        
    }
}

- (IBAction)meTooPost:(id)sender {
    
    
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    btnMeToo.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
    
    __block PFObject *socialPostMeToo = [appDelegate getPostMeToo:post.objectId];
    
    // Disable the button.
    __block UIButton *button = (UIButton *)sender;
    button.enabled = NO;
    
    [parentViewController showMeTooPopup];
    
    if (socialPostMeToo) {
//        [socialPostMeToo deleteInBackgroundWithBlock:^(BOOL success, NSError *error){
//            if (success) {
//                socialPostMeToo = nil;
//                btnHug.selected = NO;
//                int cnt = [[post objectForKey:@"meTooCount"] intValue];
//                cnt--;
//                [post setObject:@(cnt) forKey:@"meTooCount"];
//                [self updatePostStats];
//                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubTyoe":@(NOTIFICATION_SUBTYPE_METOO)};
//                [PFCloud callFunctionInBackground:@"notificationDelete" withParameters:params block:^(id object, NSError *error) {
//                    if (error) {
//                        NSLog(@"Error:%@",error);
//                    }
//                }];
//            }
//        }];
        
        
        
    }
    else {
        PFObject *postMeToo = [PFObject objectWithClassName:@"PostMeToo"];
        [postMeToo setObject:post forKey:@"post"];
        [postMeToo setObject:[PFUser currentUser] forKey:@"user"];
        [postMeToo saveInBackgroundWithBlock:^(BOOL success, NSError *error){
            if (success) {
                btnMeToo.selected = YES;
                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubType":@(NOTIFICATION_SUBTYPE_METOO)};
                [PFCloud callFunctionInBackground:@"notificationCreate" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                    }
                    else {
                        
                        int cnt = [[post objectForKey:@"meTooCount"] intValue];
                        cnt++;
                        [post setObject:@(cnt) forKey:@"meTooCount"];
                        [appDelegate addUserPostMeToos:@[postMeToo]];
                        [self updatePostStats];

                        /*
                        pmtvc = [[PostMeTooPopupViewController alloc] initWithNibName:@"PostMeTooPopupViewController" bundle:nil];
                        [parentViewController.view addSubview:pmtvc.view];
                        pmtvc.delegate = parentViewController;
                        pmtvc.view.center = parentViewController.view.center;
                        */
                    }
                }];
            }
            
            // Enable the button.
            button.enabled = YES;
        }];
        
    }
}

-(void)inspirationPostTapped {
    activityView.hidden = NO;
    PFObject *inspirationPost = [post objectForKey:@"inspiredByPost"];
    [inspirationPost fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            activityView.hidden = YES;
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
            vc.delegate = [parentViewController.navigationController.viewControllers objectAtIndex:0];
            
            // Check the user type.
            PFObject *post = object;
            NSInteger currentUserType = [[[PFUser currentUser] objectForKey:@"userType"] integerValue];
            NSInteger postUserType = [[post objectForKey:@"userType"] integerValue];
            if (currentUserType != postUserType) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incorrect Group" message:@"You may not view this post because it belongs to a different user group." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                return;
            }
            
            vc.postArray = [NSArray arrayWithObjects:object, nil];
            vc.chosenPostIndex = 0;
            [parentViewController.navigationController pushViewController:vc animated:YES];
        } else {
            activityView.hidden = YES;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"This post is no longer available to view." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }];
}

-(void)inspiredPostsTapped {
    
    if ([[post objectForKey:@"meTooStoryCount"] intValue] > 1) {
        
        UINavigationController *nav = parentViewController.navigationController;
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PostsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostsViewController"];
        vc.inspirationPost = post;
        [nav pushViewController:vc animated:YES];
        
    } else {
        
        activityView.hidden = NO;
        
        PFUser *user = [PFUser currentUser];
        //Create query for all Post object by the current user
        PFQuery *postQuery = [PFQuery queryWithClassName:@"Post"];
        
        [postQuery whereKey:@"inspiredByPost" equalTo:post];
        // Hide any private posts
        [postQuery whereKey:@"isPrivate" equalTo:[NSNumber numberWithBool:FALSE]];
            
        // Hide any hidden posts.
        NSArray *postsHidden = [user objectForKey:@"postsHidden"];
        if (postsHidden || postsHidden.count) {
            [postQuery whereKey:@"objectId" notContainedIn:postsHidden];
        }
        
        // Set the approved.
        [postQuery whereKey:@"approvedStatus" equalTo:[NSNumber numberWithInt:1]];
            
        // Hide any blocked authors.
        NSArray *authorsBlocked = [user objectForKey:@"authorsBlocked"];
        if (authorsBlocked || authorsBlocked.count) {
            [postQuery whereKey:@"user" notContainedIn:authorsBlocked];
        }
        [postQuery orderByDescending:@"createdAt"];
        // Run the query
        [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            activityView.hidden = YES;
            
            if (!error) {
                
                // Get the first post.
                if ([objects count] > 0 ) {
                    
                    // Check the user type.
                    PFObject *post = [objects objectAtIndex:0];
                    NSInteger currentUserType = [[[PFUser currentUser] objectForKey:@"userType"] integerValue];
                    NSInteger postUserType = [[post objectForKey:@"userType"] integerValue];
                    if (currentUserType != postUserType) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incorrect Group" message:@"You may not view this post because it belongs to a different user group." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                        return;
                    }
                    
                    
                    activityView.hidden = YES;
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
                    vc.delegate = [parentViewController.navigationController.viewControllers objectAtIndex:0];
                    vc.postArray = objects;
                    vc.chosenPostIndex = 0;
                    [parentViewController.navigationController pushViewController:vc animated:YES];
                    
                    
                } else {
                    activityView.hidden = YES;
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"This post is no longer available to view." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    return;
                }
                    
            } else {
                activityView.hidden = YES;
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"This post is no longer available to view." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                return;
            }
        }];
        
    }
    
}

- (NSString *) constructFlaggedText:(NSInteger)flag format:(NSString *)format {
    
    NSString *flagString;
    switch (flag) {
        case POST_FLAG_INAPPROPRIATE:
            flagString = @"inappropriate";
            break;
        case POST_FLAG_SELF_HARM:
            flagString = @"self harm";
            break;
        case POST_FLAG_THREATENING:
            flagString = @"threatening";
            break;
        case POST_FLAG_BULLYING:
            flagString = @"bullying";
            break;
        case POST_FLAG_SPAM:
            flagString = @"spam";
            break;
    }
    
    return [NSString stringWithFormat:format, flagString];
}

- (void)updatePostHashNoteCount {
    
    noteCount = self.notes.count;
    NSString *  hashTagText = nil;
    if (noteCount > 1 || noteCount == 0)
        hashTagText = @"%d Hashnotes";
    else hashTagText = @"%d Hashnote";
    
    
    
    self.lblNumHashTags.text = [NSString stringWithFormat:hashTagText, noteCount];
}

@end
