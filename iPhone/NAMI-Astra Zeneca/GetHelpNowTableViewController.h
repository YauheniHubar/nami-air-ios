//
//  GetHelpNowTableViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 12/31/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface GetHelpNowTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>

- (IBAction)emailNAMI:(id)sender;

@end
