//
//  PostFlaggedPopupViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/11/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostFlaggedPopupViewController.h"

@interface PostFlaggedPopupViewController ()

@end

@implementation PostFlaggedPopupViewController

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.tag = POST_POPUP_FLAGGED;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)okButtonClicked:(id)sender {
    if (delegate) {
        [delegate cancelButtonClicked:self];
    }
}
@end
