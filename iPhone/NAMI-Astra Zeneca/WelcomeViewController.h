//
//  WelcomeViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Adam Behnke on 11/18/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface WelcomeViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *createAccountButton;

@property (nonatomic, weak) IBOutlet UIView *layoutAnchor;
@property (nonatomic, weak) IBOutlet UIView *welcomeContainer;
@property (nonatomic, weak) IBOutlet UIImageView *bubbleImage;
@property (nonatomic, weak) IBOutlet UIImageView *airImage;
@property (nonatomic, weak) IBOutlet UIImageView *namiImage;
@property (nonatomic, weak) IBOutlet UIImageView *taglineImage;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *bubbleYConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *airYConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *welcomeYConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *eulaSpacer;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *titleSpacer;

@end
