//
//  Post.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <Parse/Parse.h>

@interface Post : PFObject<PFSubclassing>

@property (strong) NSString *displayName;

@property PFUser *user;
@property NSInteger userType;
@property NSInteger approvedStatus;
@property NSInteger flaggedStatus;
@property NSString *text;
@property BOOL isPrivate;
@property Post *inspiredByPost;
@property NSInteger likeCount;
@property NSInteger hugCount;
@property NSInteger meTooCount;
@property NSInteger meTooStoryCount;
@property NSInteger noteCount;
@property NSString *searchableText;
@property NSArray *hashtags;
@property NSArray *hashnotes;
@property PFUser *author;
@property NSString *textContent;
@property NSString *userId;

+(NSString *)parseClassName;

@end
