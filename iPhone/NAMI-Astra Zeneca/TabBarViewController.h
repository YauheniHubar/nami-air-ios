//
//  TabBarViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Adam Behnke on 11/24/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarViewController : UITabBarController

@end
