//
//  konstant.h
//  NAMI-Astra Zeneca
//
//  Created by Adam Behnke on 11/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#ifndef NAMI_Astra_Zeneca_konstant_h
#define NAMI_Astra_Zeneca_konstant_h

#define FINDING @"I live with a mental health condition"
#define SUPPORTING @"I am a family member or caregiver"
#define TERMSOFUSE @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis, enim iaculis tincidunt placerat, diam urna aliquet justo, at molestie lorem nisi in sem. Donec sed placerat lectus. Aenean faucibus leo vel lorem porta, mattis suscipit justo ornare. Cras dapibus erat et dolor pharetra sagittis vitae at lectus. Donec sed magna malesuada, malesuada diam vel, pulvinar nunc. Ut scelerisque at neque vitae consequat. Curabitur a erat et arcu condimentum hendrerit.\n\nNulla interdum ex at odio elementum, at condimentum arcu ornare. Nunc fermentum scelerisque volutpat. Mauris id sapien molestie, placerat nunc at, semper lacus. Duis nec magna turpis. Duis non urna id justo feugiat feugiat eu a nunc. Integer eu imperdiet libero, mollis consectetur mauris. Etiam interdum purus vitae leo commodo, ultricies blandit magna consectetur. Aenean commodo elit eu lacus luctus, id cursus quam condimentum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin dignissim quam at purus pellentesque gravida. Fusce et nunc cursus, scelerisque enim vel, sodales dolor. Curabitur ac nibh eget orci pretium congue cursus a leo. Sed lacinia ante sed luctus posuere. Fusce luctus, ex imperdiet consequat accumsan, lorem mauris ullamcorper erat, sit amet interdum libero augue eu tellus. Vivamus augue nisi, mollis sit amet turpis vel, posuere tristique eros. Aenean venenatis consequat metus et ornare.\n\nNunc sagittis posuere varius. Nulla est mauris, faucibus at arcu in, aliquet luctus nunc. Donec non eleifend tellus, id porta ligula. Aenean lacinia luctus convallis. Praesent vitae laoreet sem. Maecenas sit amet pellentesque tortor, sed accumsan ex. Nam eget leo velit. Mauris finibus pulvinar feugiat. Maecenas tristique rutrum volutpat. Pellentesque viverra, dui et tincidunt porta, urna sem rutrum metus, tempor tincidunt ipsum elit ornare ante. Aliquam erat volutpat. In aliquet id lectus vitae facilisis. Cras et nulla nisl. Aliquam quis volutpat erat. Suspendisse placerat arcu sed ligula molestie, et ullamcorper diam consectetur. Suspendisse tortor diam, facilisis id tincidunt at, sollicitudin et lorem."


// Values
#define NOTIFICATION_TYPE_SOCIAL       1
#define NOTIFICATION_TYPE_NOTES        2
#define NOTIFICATION_TYPE_INSPIRED     3
#define NOTIFICATION_TYPE_PENDING      4
#define NOTIFICATION_TYPE_APPROVED     5
#define NOTIFICATION_TYPE_DISAPPROVED  6
#define NOTIFICATION_TYPE_DIRECT       7
#define NOTIFICATION_TYPE_CHANGE_GROUP 8

#define NOTIFICATION_SUBTYPE_LIKE  1
#define NOTIFICATION_SUBTYPE_HUG   2
#define NOTIFICATION_SUBTYPE_METOO 3

#define POST_FLAG_NOT_FLAGGED   0
#define POST_FLAG_INAPPROPRIATE 1
#define POST_FLAG_SELF_HARM     2
#define POST_FLAG_THREATENING   3
#define POST_FLAG_BULLYING      4
#define POST_FLAG_SPAM          5

#define POST_APPROVAL_NOTAPPROVED 0
#define POST_APPROVAL_APPROVED    1  
#define POST_APPROVAL_DISAPPROVED 2

#define USER_TYPE_USER      1
#define USER_TYPE_CAREGIVER 2


#endif
