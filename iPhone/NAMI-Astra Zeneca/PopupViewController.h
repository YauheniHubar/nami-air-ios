//
//  PopupViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 10/5/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PopupDelegate <NSObject>

@optional
-(void)popupCloseButtonClicked:(id)sender;
@end


@interface PopupViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIView *outerView;
@property (strong, nonatomic) IBOutlet UIView *viewPopup;
@property (strong, nonatomic) IBOutlet UILabel *lblPopupTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnLink;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong,nonatomic) id<PopupDelegate> delegate;

@property (nonatomic, strong) NSString *popupTitle;
@property (nonatomic, strong) NSString *linkTitle;
@property (nonatomic, strong) NSString *linkURL;

- (IBAction)closeButtonClick:(id)sender;

@end
