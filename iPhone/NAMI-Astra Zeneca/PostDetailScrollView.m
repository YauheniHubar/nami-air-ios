//
//  PostDetailScrollView.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/4/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostDetailScrollView.h"

@implementation PostDetailScrollView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)init
{
    return [super init];
}

-(instancetype)initWithFrame:(CGRect)frame
{
    return [super initWithFrame:frame];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    return [super initWithCoder:aDecoder];
}

-(BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view
{
    return [super touchesShouldBegin:touches withEvent:event inContentView:view];//YES;
}

-(BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    return YES;
}

@end
