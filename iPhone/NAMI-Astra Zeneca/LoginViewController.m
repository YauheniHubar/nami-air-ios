//
//  LoginViewController.m
//  ParseExample
//
//  Created by Nick Barrowclough on 8/9/13.
//  Copyright (c) 2013 Nicholas Barrowclough. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _loginButton.layer.borderWidth=2.0f;
    _loginButton.layer.cornerRadius=5.0;
    _loginButton.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.navigationController.navigationBarHidden = YES;
    _emailField.delegate = self;
    _passwordField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    //PFUser *user = [PFUser currentUser];
    //TODO: Uncomment this so the user only has to log in once
    /*
    if (user.username != nil) {
        [self performSegueWithIdentifier:@"login" sender:self];
    }
     */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //if (textField == self.inputText) {
        [textField resignFirstResponder];
        return NO;
    //}
    //return YES;
}

- (IBAction)registerAction:(id)sender {
    [_usernameField resignFirstResponder];
    [_emailField resignFirstResponder];
    [_passwordField resignFirstResponder];
    //[_reEnterPasswordField resignFirstResponder];
    [self checkFieldsComplete];
}

- (void) checkFieldsComplete {
    if (/*[_usernameField.text isEqualToString:@""] ||*/ [_emailField.text isEqualToString:@""] || [_passwordField.text isEqualToString:@""] /*|| [_reEnterPasswordField.text isEqualToString:@""]*/) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You need to complete all fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else {
        /*[self checkPasswordsMatch];*/
    }
}

- (void) checkPasswordsMatch {
    /*
    if (![_passwordField.text isEqualToString:_reEnterPasswordField.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oooopss!" message:@"Passwords don't match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else {
        [self registerNewUser];
    }
     */
}

- (void) registerNewUser {
    NSLog(@"registering....");
    PFUser *newUser = [PFUser user];
    //newUser.username = _usernameField.text;
    newUser.username = _emailField.text;
    newUser.email = _emailField.text;
    newUser.password = _passwordField.text;
    
    [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSLog(@"Registration success!");
            _loginPasswordField.text = nil;
            _loginUsernameField.text = nil;
            //_usernameField.text = nil;
            _passwordField.text = nil;
            //_reEnterPasswordField.text = nil;
            _emailField.text = nil;
            [self performSegueWithIdentifier:@"login" sender:self];
        }
        else {
            NSLog(@"There was an error in registration");
        }
    }];
}

- (IBAction)registeredButton:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        _loginOverlayView.frame = self.view.frame;
    }];
}

- (IBAction)loginButton:(id)sender {
    _loginUsernameField = _emailField;
    _loginPasswordField = _passwordField;
    [self.view endEditing:YES];
    NSLog(@"Login user::email::%@", _emailField.text);
    NSLog(@"Login user::username::%@", _loginUsernameField.text);
    NSLog(@"Login user::password::%@", _loginPasswordField.text);
    
    __block UIActivityIndicatorView *progress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    progress.color = [UIColor darkGrayColor];
    [self.view addSubview:progress];
    progress.center = self.view.center;
    [progress startAnimating];

    
    [PFUser logInWithUsernameInBackground:_loginUsernameField.text password:_loginPasswordField.text block:^(PFUser *user, NSError *error) {
        
        [progress stopAnimating];
        [progress removeFromSuperview];
        if (!error) {
            if ([[user objectForKey:@"disabled"] boolValue] == YES) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Sorry, your account has been disabled" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            } else {
                NSLog(@"Login user!");
                _loginPasswordField.text = nil;
                _loginUsernameField.text = nil;
                _passwordField.text = nil;
                _emailField.text = nil;
                
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.scrollToTopAllPosts = YES;
                [appDelegate loadModeratorFlagPosts];
                [appDelegate registerForNotifications];
                [appDelegate clearUserSocial];
                [appDelegate fetchUnreadNotifications];
                
                [AppDelegate updatePostFlagStatus:^(BOOL completed) {
                    if (completed) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"NAMIUserLoggedIn" object:nil];
                    }
                }];
                
            }
        }
        if (error) {
            
            NSString *errorMessage = [[error userInfo] objectForKey:@"error"];
            if ([errorMessage rangeOfString:@"invalid login credentials"].length > 0) {
                _passwordErrorLabel.hidden = NO;
                [_passwordErrorLabel setText:@"Sorry, your credentials don't match our records"];
                [_passwordErrorLabel setAlpha:0.0];
                [UIView animateWithDuration:0.5 delay:0                  options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                                 animations:^(void) {[_passwordErrorLabel setAlpha:1.0];}
                                 completion:^(BOOL finished) {
                                     if(finished) { [UIView animateWithDuration:0.5 delay:1
                                                                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                                                                     animations:^(void) {[_passwordErrorLabel setAlpha:0.0];}
                                                                     completion:^(BOOL finished){
                                                                     }];
                                     }
                                 }];
                
            } else {
                
                [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
            }
            
            //[((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Sorry we had a problem logging you in" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
            
        }
    }];
}

- (IBAction)forgotPassword:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset Password" message:@"Please enter your email address for further instructions on resetting your password." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    UITextField *alertEmailField = (UITextField*)[alertView textFieldAtIndex:0];
    if (alertEmailField.text.length > 0) {
        [PFUser requestPasswordResetForEmailInBackground:alertEmailField.text];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset Password" message:@"Password reset instructions sent." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
}

@end
