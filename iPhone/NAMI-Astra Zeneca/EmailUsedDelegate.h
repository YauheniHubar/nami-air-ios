//
//  EmailUsedDelegate.h
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/10/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#ifndef NAMI_Astra_Zeneca_EmailUsedDelegate_h
#define NAMI_Astra_Zeneca_EmailUsedDelegate_h

@protocol EmailUsedDelegate <NSObject>

@optional

-(void) emailUsedCloseButtonClicked:(id)sender;

@end

#endif
