//
//  Feeling.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "Feeling.h"
#import <Parse/PFObject+Subclass.h>

@implementation Feeling

@dynamic displayName,name;

+(NSString *)parseClassName {
    return @"Feeling";
}


@end
