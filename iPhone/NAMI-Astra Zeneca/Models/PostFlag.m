//
//  PostFlag.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostFlag.h"
#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>

@implementation PostFlag

@dynamic displayName,post,user,flaggedStatus;

+(NSString *)parseClassName {
    return @"PostFlag";
    
}

+ (PFQuery *)userPostFlagQuery {
    
    PFQuery *query = [PFQuery queryWithClassName:@"PostFlag"];
    [query whereKey:@"user" equalTo:[PFUser currentUser]];
    [query orderByDescending:@"createdAt"];
    [query setLimit:1000];
    
    return query;
}

+ (PFQuery *)moderatorPostFlagQuery {
    
    PFQuery *userQuery = [PFUser query];
    [userQuery whereKey:@"userType" equalTo:[NSNumber numberWithInt:3]];
    
    PFQuery *postQuery = [PFQuery queryWithClassName:@"Post"];
    [postQuery whereKey:@"user" equalTo:[PFUser currentUser]];
    
    PFQuery *flagQuery = [PFQuery queryWithClassName:@"PostFlag"];
    [flagQuery includeKey:@"post"];
    
    [flagQuery whereKey:@"user" matchesQuery:userQuery];
    [flagQuery whereKey:@"post" matchesQuery:postQuery];
    [flagQuery orderByDescending:@"createdAt"];
    [flagQuery setLimit:1000];
                                            
    return flagQuery;
}

@end
