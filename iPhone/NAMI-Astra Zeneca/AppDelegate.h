//
//  AppDelegate.h
//  NAMI-Astra Zeneca
//
//  Created by Adam Behnke on 10/30/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <Flurry.h>
#import "ModeratorNotificationDelegate.h"
#import "PopupViewController.h"

typedef void(^backgroundCompletion)(BOOL);

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate, ModeratorNotificationDelegate, PopupDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign) NSInteger selectedFeed;
@property (assign) BOOL scrollToTopAllPosts;
@property (assign) BOOL stayOnMyPosts;
@property (nonatomic, strong) NSString *unreadNotificationId;
@property (nonatomic, strong) UIAlertView *connectionAlertView;
@property (nonatomic, strong) PFObject *inspiredPost;
@property (nonatomic, strong) NSDictionary *popupPayload;

+(NSArray *)getHashNoteModels;
+(NSArray *)getFeelingModels;
+(NSArray *)getRedFlagWordModels;
+(NSInteger)getFlaggedPostStatus:(PFObject *)post;
+(NSInteger)getModeratorFlaggedPostStats:(PFObject *)post;

-(void)clearUserSocial;
-(void)addUserPostLikes:(NSArray *)postLikes;
-(void)addUserPostHugs:(NSArray *)postHugs;
-(void)addUserPostMeToos:(NSArray *)postMeToos;
-(BOOL)hasPostLike:(NSString *)postId;
-(BOOL)hasPostHug:(NSString *)postId;
-(BOOL)hasPostMeToo:(NSString *)postId;
-(PFObject *)getPostLike:(NSString *)postId;
-(PFObject *)getPostHug:(NSString *)postId;
-(PFObject *)getPostMeToo:(NSString *)postId;
-(void)removePostLike:(PFObject *)postLike;
-(void)removePostHug:(PFObject *)postHug;
-(void)removePostMeToo:(PFObject *)postMeToo;

-(void)loadModeratorFlagPosts;

+(void)updatePostFlagStatus:(backgroundCompletion)completion;
-(void) registerForNotifications;

-(void)moderatorMessageIgnoreButtonClicked:(id)sender;
-(void)moderatorMessageOpenButtonClicked:(id)sender;
-(void)popupCloseButtonClicked:(id)sender;

- (void) fetchUnreadNotifications;

-(void)showConnectionError;
-(void)showPopup:(NSDictionary *)payload;

@end

