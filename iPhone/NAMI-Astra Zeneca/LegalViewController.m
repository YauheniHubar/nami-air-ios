//
//  LegalViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 1/7/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import "LegalViewController.h"

@interface LegalViewController ()

@end

@implementation LegalViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self initializeClickableLinks];
}



- (void) initializeClickableLinks {
    
    
    if ([[self.navigationItem title] rangeOfString:@"Privacy"].length > 0) {
        
        // Get the text.
        NSString *value = self.textView.text;
        NSString *searchValue = @"EULA";
        NSMutableAttributedString *textViewValue = [[NSMutableAttributedString alloc] initWithAttributedString:self.textView.attributedText];
        
        
        // Find the occurrences.
        NSRange searchRange = NSMakeRange(0,value.length);
        NSRange foundRange;
        while (searchRange.location < value.length) {
            searchRange.length = value.length-searchRange.location;
            foundRange = [value rangeOfString:searchValue options:nil range:searchRange];
            if (foundRange.location != NSNotFound) {
                
                // Make the url.
                NSURL *ppURL = [NSURL URLWithString:@"airapp://eula"];
                [textViewValue addAttribute:NSLinkAttributeName value:ppURL range:foundRange];
                
                
                // found an occurrence of the substring! do stuff here
                searchRange.location = foundRange.location+foundRange.length;
            } else {
                // no more substring to find
                break;
            }
        }
        
        // Set the text.
        self.textView.attributedText = textViewValue;
    }
    else {
        
        // Get the text.
        NSString *value = self.textView.text;
        NSString *searchValue = @"Privacy Policy";
        NSMutableAttributedString *textViewValue = [[NSMutableAttributedString alloc] initWithAttributedString:self.textView.attributedText];
        
        
        // Find the occurrences.
        NSRange searchRange = NSMakeRange(0,value.length);
        NSRange foundRange;
        while (searchRange.location < value.length) {
            searchRange.length = value.length-searchRange.location;
            foundRange = [value rangeOfString:searchValue options:nil range:searchRange];
            if (foundRange.location != NSNotFound) {
                
                // Make the url.
                NSURL *ppURL = [NSURL URLWithString:@"airapp://pp"];
                [textViewValue addAttribute:NSLinkAttributeName value:ppURL range:foundRange];
                
                
                // found an occurrence of the substring! do stuff here
                searchRange.location = foundRange.location+foundRange.length;
            } else {
                // no more substring to find
                break;
            }
        }
        
        // Set the text.
        self.textView.attributedText = textViewValue;
    }
    
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    
    
    // Test for the PP.
    if ([[URL absoluteString] isEqualToString:@"airapp://eula"]) {
        
        [self performSegueWithIdentifier:@"ppToEULA" sender:self];
        
        return NO;
    } else if ([[URL absoluteString] isEqualToString:@"airapp://pp"]) {
        
        [self performSegueWithIdentifier:@"eulaToPP" sender:self];
        
        return NO;
    }
    
    return YES;
}
@end
