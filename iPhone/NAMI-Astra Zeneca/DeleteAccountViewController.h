//
//  DeleteAccountViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/19/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeleteAccountViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
- (IBAction)deleteClicked:(id)sender;
@end
