//
//  PostPendingPopupViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/18/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostPopupDelegate.h"

@interface PostPendingPopupViewController : UIViewController

@property (strong,nonatomic) id<PostPopupDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnPost;
@property (strong, nonatomic) IBOutlet UITextView *lblMessage;

@property (strong, nonatomic) NSArray *keywords;


- (IBAction)editButtonClick:(id)sender;
- (IBAction)postButtonClick:(id)sender;



@end
