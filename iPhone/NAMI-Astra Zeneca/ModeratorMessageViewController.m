//
//  ModeratorMessageViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/9/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import "ModeratorMessageViewController.h"
#import "PostDetailsViewController.h"

@interface ModeratorMessageViewController ()
{

    PFObject* post;
}
@end

@implementation ModeratorMessageViewController

@synthesize notification;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Set the navigation items.
    self.navigationItem.title = @"Moderator Message";    
    
    // Get the post.
    post = [notification objectForKey:@"post"];
    
    // Create the moderator message header.
    [self createModeratorMessageHeader];
    
    // Create the moderator message.
    [self createModeratorMessage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) createModeratorMessageHeader
{
    
    // Create the initial attributes.
    UIFont *font = [UIFont fontWithName:@"ProximaNova-Regular" size:14.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font
                                                                forKey:NSFontAttributeName];
    
    // Create the go to detail font.
    NSString *gotoDetail = @" GO TO POST DETAIL>";
    UIFont *gotoFont = [UIFont fontWithName:@"MuseoSlab-500" size:12.0];
    
    // Create the message.
    NSString *text = [NSString stringWithFormat:@"In reference to \"%@\"%@", [post objectForKey:@"text"], gotoDetail];
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:text attributes:attrsDictionary];
    
    // Update the message.
    NSRange gotoRange = [text rangeOfString:gotoDetail options:NSBackwardsSearch];
    [attributedMessage addAttribute:NSFontAttributeName value:gotoFont range:gotoRange];
    
    // Set the text.
    self.headerLabel.attributedText = attributedMessage;
    
    
    // Set the number of lines.
    NSInteger lines = 2;
    
    // Delcare the ellipsis.
    NSMutableAttributedString *ellipsis = [[NSMutableAttributedString alloc] initWithString:@"..." attributes:attrsDictionary];
    
    NSMutableAttributedString *truncatedString = [attributedMessage mutableCopy];
    if ([self numberOfLinesNeeded:truncatedString font:font] > lines) {
        [truncatedString insertAttributedString:ellipsis atIndex:(gotoRange.location - 1)];
        NSRange range = NSMakeRange(truncatedString.length - gotoDetail.length - 1 - (ellipsis.length + 1), 1);
        while ([self numberOfLinesNeeded:truncatedString font:font] > lines) {
            [truncatedString deleteCharactersInRange:range];
            range.location--;
        }
        [truncatedString deleteCharactersInRange:range];  //need to delete one more to make it fit
        CGRect labelFrame = self.headerLabel.frame;
        labelFrame.size.height = [@"A" sizeWithFont:font].height * lines;
        self.headerLabel.frame = labelFrame;
        self.headerLabel.attributedText = truncatedString;

    }else{
        CGRect labelFrame = self.headerLabel.frame;
        labelFrame.size.height = [@"A" sizeWithFont:font].height * lines;
        self.headerLabel.frame = labelFrame;
        self.headerLabel.attributedText = attributedMessage;
    }
    
    
    
    UITapGestureRecognizer *click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerClicked:)];
    [self.header addGestureRecognizer:click];
}

-(void) createModeratorMessage {
    
    // Set the message.
    self.message.text = [notification valueForKey:@"message"];
}

-(int)numberOfLinesNeeded:(NSMutableAttributedString *)s font:(UIFont *)font {
    float oneLineHeight = [@"A" sizeWithFont:font].height;
    CGRect bounds = self.headerLabel.bounds;
    CGRect boundingRect = [s boundingRectWithSize:CGSizeMake(bounds.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:NULL];
    float totalHeight = boundingRect.size.height;
    return nearbyint(totalHeight/oneLineHeight);
}


-(IBAction)headerClicked:(UITapGestureRecognizer *)recognizer {
    
    [self.header setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(98.0f/255.0f) blue:(48.0/255.0f) alpha:1.0f]];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
    vc.postArray = @[post];
    vc.chosenPostIndex = 0; //(NSUInteger)indexPath.row;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch= [touches anyObject];
    if ([touch view] == self.header)
    {
        [self.header setBackgroundColor:[UIColor colorWithRed:(224.0f/255.0f) green:(86.0f/255.0f) blue:(42.0/255.0f) alpha:1.0f]];
    }
    
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch= [touches anyObject];
    if ([touch view] == self.header)
    {
        [self.header setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(98.0f/255.0f) blue:(48.0/255.0f) alpha:1.0f]];
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
