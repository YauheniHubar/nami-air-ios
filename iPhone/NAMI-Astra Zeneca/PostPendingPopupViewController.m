//
//  PostPendingPopupViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/18/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostPendingPopupViewController.h"

@interface PostPendingPopupViewController ()

@end

@implementation PostPendingPopupViewController

@synthesize delegate, keywords;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Create the message.
    if (keywords.count == 1) {
        NSString *message = self.lblMessage.text;
        NSString *keywordsFormatted = [self.keywords componentsJoinedByString:@", "];
        self.lblMessage.text = [NSString stringWithFormat:message, keywordsFormatted];
    } else if (keywords.count == 2) {
        NSString *message = self.lblMessage.text;
        NSString *keywordsFormatted = [NSString stringWithFormat:@"%@ and %@", [keywords objectAtIndex:0], [keywords objectAtIndex:1]];
        self.lblMessage.text = [NSString stringWithFormat:message, keywordsFormatted];
        
    } else if (keywords.count > 2) {
        
        NSString *message = self.lblMessage.text;
        NSMutableArray *updatedKeywords = [[NSMutableArray alloc] initWithArray:keywords];
        NSString *lastKeyword = [updatedKeywords objectAtIndex:updatedKeywords.count - 1];
        [updatedKeywords removeObjectAtIndex:updatedKeywords.count - 1];
        [updatedKeywords addObject:[NSString stringWithFormat:@" and %@", lastKeyword]];
        NSString *keywordsFormatted = [updatedKeywords componentsJoinedByString:@", "];
        self.lblMessage.text = [NSString stringWithFormat:message, keywordsFormatted];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)editButtonClick:(id)sender {
    if (delegate) {
        [delegate cancelButtonClicked:self];
    }
}

- (IBAction)postButtonClick:(id)sender {
    if (delegate) {
        [delegate okButtonClicked:self];
    }
}
@end
