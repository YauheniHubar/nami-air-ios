//
//  PostHug.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <Parse/Parse.h>
#import "Post.h"

@interface PostHug : PFObject<PFSubclassing>

@property (strong) NSString *displayName;

@property Post *post;
@property PFUser *user;

+(NSString *)parseClassName;

@end
