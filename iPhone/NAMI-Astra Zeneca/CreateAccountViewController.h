//
//  CreateAccountViewController.h
//  ParseExample
//
//  Created by Nick Barrowclough on 8/9/13.
//  Copyright (c) 2013 Nicholas Barrowclough. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "EmailUsedDelegate.h"
#import "ChooseGroupDelegate.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface CreateAccountViewController : UIViewController<EmailUsedDelegate, ChooseGroupDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *reEnterPasswordField;
@property (weak, nonatomic) IBOutlet UIView *loginOverlayView;
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

- (IBAction)registerAction:(id)sender;
- (IBAction)registeredButton:(id)sender;
- (IBAction)loginButton:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *emailErrorLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordErrorLabel;

@property (weak, nonatomic) IBOutlet UITextField *loginUsernameField;
@property (weak, nonatomic) IBOutlet UITextField *loginPasswordField;

- (BOOL) validateEmail: (NSString *) candidate;

-(void) emailUsedCloseButtonClicked:(id)sender;
-(void) chooseGroupCloseButtonClicked:(id)sender;

- (IBAction) userSelected:(id)sender;
- (IBAction) caregiverSelected:(id)sender;

@end
