		//
//  GetHelpAboutTableViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 12/31/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "GetHelpAboutTableViewController.h"

@interface GetHelpAboutTableViewController ()

@end

@implementation GetHelpAboutTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nami.org/getinvolved-app?utm_source=AIR&utm_medium=About_NAMI&utm_content=Get_Involved&utm_campaign=AIRapp"]];
    } else if (indexPath.row == 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nami.org/about-app?utm_source=AIR&utm_medium=About_NAMI&utm_content=More&utm_campaign=AIRapp"]];
    }
}

@end
