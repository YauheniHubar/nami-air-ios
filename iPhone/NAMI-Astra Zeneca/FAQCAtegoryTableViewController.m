//
//  FAQCAtegoryTableViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Laura Haskins on 12/30/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "FAQCAtegoryTableViewController.h"
#import "FAQTableViewController.h"

@interface FAQCAtegoryTableViewController ()
@property (nonatomic, strong) NSArray *faqHowAnonymous;
@property (nonatomic, strong) NSArray *faqGettingStarted;
@property (nonatomic, strong) NSArray *faqTips;
@end

@implementation FAQCAtegoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    FAQTableViewController *vc = (FAQTableViewController*)[segue destinationViewController];
    NSString *fileName;
    
    if ([segue.identifier isEqualToString:@"FAQHowAnonymous"]) {
        fileName = @"faqHowAnonymous";
        vc.title = @"How Anonymous is AIR?";
    } else if ([segue.identifier isEqualToString:@"FAQGettingStarted"]) {
        fileName = @"faqGettingStarted";
        vc.title = @"Getting Started";
    } else {
        fileName = @"faqTips";
        vc.title = @"Tips and Troubleshooting";
    }
    
    
    vc.data = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"]];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 3) {        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nami.org/about"]];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

@end
