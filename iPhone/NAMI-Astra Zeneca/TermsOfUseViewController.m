//
//  TermsOfUseViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Adam Behnke on 11/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "TermsOfUseViewController.h"
#import "konstant.h"

@interface TermsOfUseViewController ()

@end

@implementation TermsOfUseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _continueButton.layer.cornerRadius=5.0;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
    // Initialize the clickable links.
    [self initializeClickableLinks];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.textView.selectedRange = NSMakeRange(0, 0);
    [self.textView scrollRangeToVisible:NSMakeRange(0,0)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)agreeButtonClicked:(id)sender {
    _agreeButton.selected = !_agreeButton.selected;
}

- (IBAction)buttonClicked:(id)sender {

    if (_agreeButton.state == 4) {
        [self performSegueWithIdentifier:@"termsaccepted" sender:self];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You must accept the End User License Agreement before continuing." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) initializeClickableLinks {
    
    // Get the text.
    NSString *value = self.textView.text;
    NSString *searchValue = @"Privacy Policy";
    NSMutableAttributedString *textViewValue = [[NSMutableAttributedString alloc] initWithAttributedString:self.textView.attributedText];
    
    
    // Find the occurrences.
    NSRange searchRange = NSMakeRange(0,value.length);
    NSRange foundRange;
    while (searchRange.location < value.length) {
        searchRange.length = value.length-searchRange.location;
        foundRange = [value rangeOfString:searchValue options:nil range:searchRange];
        if (foundRange.location != NSNotFound) {
            
            // Make the url.
            NSURL *ppURL = [NSURL URLWithString:@"airapp://pp"];
            [textViewValue addAttribute:NSLinkAttributeName value:ppURL range:foundRange];
            
            
            // found an occurrence of the substring! do stuff here
            searchRange.location = foundRange.location+foundRange.length;
        } else {
            // no more substring to find
            break;
        }
    }
    
    // Set the text.
    self.textView.attributedText = textViewValue;
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    
    
    // Test for the PP.
    if ([[URL absoluteString] isEqualToString:@"airapp://pp"]) {
        
        [self performSegueWithIdentifier:@"eulaToPP" sender:self];
        
        return NO;
    }
    
    return YES;
}

@end
