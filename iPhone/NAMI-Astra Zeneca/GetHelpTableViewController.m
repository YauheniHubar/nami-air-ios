//
//  GetHelpTableViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 12/31/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "GetHelpTableViewController.h"

@interface GetHelpTableViewController ()

@end

@implementation GetHelpTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nami.org/learn-app?utm_source=AIR&utm_medium=Get_Help&utm_content=Learn_More&utm_campaign=AIRapp"]];
    } else if (indexPath.row == 3) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nami.org/support-app?utm_source=AIR&utm_medium=Get_Help&utm_content=Help_Someone&utm_campaign=AIRapp"]];
    }
}


@end
