//
//  SearchTermViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/9/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchTermVewControllerDelegate <NSObject>

-(void)userEnteredSearchTerm:(NSString *)term;

@end

@interface SearchTermViewController : UIViewController <UISearchBarDelegate>
@property (assign) id<SearchTermVewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end
