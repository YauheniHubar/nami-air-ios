//
//  UYLInspiredHeaderCollectionViewCell.h
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 2/10/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UYLInspiredHeaderCollectionViewCell : UICollectionViewCell


@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UILabel *label;

@end
