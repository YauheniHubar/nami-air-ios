//
//  PostNotification.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <Parse/Parse.h>
#import "Post.h"

@interface PostNotification : PFObject<PFSubclassing>

@property (strong) NSString *displayName;

@property BOOL read;
@property NSUInteger likeCount;
@property NSUInteger hugCount;
@property NSUInteger meTooCount;
@property NSUInteger noteCount;
@property NSUInteger inspiredByCount;
@property NSUInteger notificationType;
@property NSDate *modificationDate;
@property NSString *message;
@property Post *post;
@property PFUser *user;

+(NSString *)parseClassName;
+(PFQuery *)queryForUsersNotifications;
+(PFQuery *)queryForUnreadModeratorotifications;


@end
