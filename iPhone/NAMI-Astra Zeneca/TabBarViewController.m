//
//  TabBarViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Adam Behnke on 11/24/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "TabBarViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [UITabBar appearance].barTintColor = [UIColor colorWithRed:33.0f/255.0f green:33.0f/255.0f blue:33.0f/255.0f alpha:1.0];
    [UITabBar appearance].selectedImageTintColor = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0];

    //[UITabBar appearance].backgroundImage =  [UIImage imageNamed:@"tabbkg.png"];
    //[[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateSelected];
    //[UITabBar appearance].backgroundColor = [UIColor blackColor];
    
    UITabBarItem *tabBarItem0 = [self.tabBar.items objectAtIndex:0];
    UIImage* selectedImage = [[UIImage imageNamed:@"icon-allposts-on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem0.selectedImage = selectedImage;
    
    UITabBarItem *tabBarItem1 = [self.tabBar.items objectAtIndex:1];
    UIImage* selectedImage1 = [[UIImage imageNamed:@"icon-notifications-on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem1.selectedImage = selectedImage1;
    
    UITabBarItem *tabBarItem2 = [self.tabBar.items objectAtIndex:2];
    UIImage* selectedImage2 = [[UIImage imageNamed:@"icon-compose.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem2.selectedImage = selectedImage2;
    tabBarItem2.image = selectedImage2;
    [tabBarItem2 setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateNormal];
    [tabBarItem2 setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateSelected];
    [tabBarItem2 setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateHighlighted];
    
    
    UITabBarItem *tabBarItem3 = [self.tabBar.items objectAtIndex:3];
    UIImage* selectedImage3 = [[UIImage imageNamed:@"icon-help-on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem3.selectedImage = selectedImage3;
    
    UITabBarItem *tabBarItem4 = [self.tabBar.items objectAtIndex:4];
    UIImage* selectedImage4 = [[UIImage imageNamed:@"icon-settings-on.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem4.selectedImage = selectedImage4;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
