//
//  PostsViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Adam Behnke on 11/18/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>
#import "UYLTextCell.h"
#import "SearchTermViewController.h"
#import "PostDetailsViewController.h"
#import "PostPopupDelegate.h"

@interface PostsViewController : UIViewController <SearchTermVewControllerDelegate,PostDetailDelegate,PostPopupDelegate>
@property (nonatomic, strong) NSArray *postArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *posts;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) NSString *searchTerm;
@property (strong, nonatomic) NSString *searchField;
@property (strong, nonatomic) PFObject *inspirationPost;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

- (IBAction)likePost:(id)sender;
- (IBAction)hugPost:(id)sender;
- (IBAction)meTooPost:(id)sender;

+(NSString *)daysSinceDate:(NSDate *)date;

-(void)userEnteredSearchTerm:(NSString *)term;

@end
