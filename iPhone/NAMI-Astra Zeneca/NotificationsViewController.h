//
//  NotificationsViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/15/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Models.h"

@interface NotificationsViewController : UITableViewController


-(IBAction)refresh:(id)sender;

@property (strong, nonatomic) IBOutlet UIRefreshControl *notificationRefreshControl;

@end
