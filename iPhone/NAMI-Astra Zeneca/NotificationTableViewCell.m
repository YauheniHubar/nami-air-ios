//
//  NotificationTableViewCell.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/16/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "NotificationTableViewCell.h"

@implementation NotificationTableViewCell

@synthesize lblText,lblTextHeight, lblImageView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
