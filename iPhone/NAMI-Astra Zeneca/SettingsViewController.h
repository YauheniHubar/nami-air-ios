//
//  SettingsViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/14/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface SettingsViewController : UITableViewController

- (IBAction)exportUserData:(id)sender;
- (IBAction)feedbackAndSupport:(id)sender;

@end
