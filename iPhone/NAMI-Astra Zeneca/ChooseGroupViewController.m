//
//  ChooseGroupViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/10/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import "ChooseGroupViewController.h"

@interface ChooseGroupViewController ()

@end

@implementation ChooseGroupViewController

@synthesize delegate, outerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.outerView.layer.cornerRadius = 3.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButtonClick:(id)sender {
    if (delegate) {
        [delegate chooseGroupCloseButtonClicked:self];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
