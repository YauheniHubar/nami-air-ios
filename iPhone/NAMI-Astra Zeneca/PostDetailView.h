//
//  PostDetailView.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/3/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostsViewController.h"
#import "PostDetailScrollView.h"
#import "PostDetailsViewController.h"
#import "PostFlaggedPopupViewController.h"
#import <Parse/Parse.h>

@interface PostDetailView : UIView <UIActionSheetDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet PostDetailScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *lblFlagBanner;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIButton *btnFlagPost;
@property (strong, nonatomic) IBOutlet UILabel *lblPostTime;
@property (strong, nonatomic) IBOutlet UITextView *txtPostContent;
@property (strong, nonatomic) IBOutlet UIButton *btnLike;
@property (strong, nonatomic) IBOutlet UIButton *btnHug;
@property (strong, nonatomic) IBOutlet UIButton *btnMeToo;
@property (strong, nonatomic) IBOutlet UILabel *lblInspirationPost;
@property (strong, nonatomic) IBOutlet UILabel *lblInspiredPosts;
@property (strong, nonatomic) IBOutlet UIView *tagContainerView;
@property (strong, nonatomic) IBOutlet UIView *inspiredContainerView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@property (strong, nonatomic) IBOutlet UILabel *lblNumHashTags;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *flagBannerHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *moderatorFlagBannerHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *postContentHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagsContainerHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *inspiredContainerHeight;
@property (strong, nonatomic) IBOutlet UILabel *moderatorLabel;

@property (assign) PFObject *post;
@property (nonatomic, strong) NSMutableArray *notes;
@property (nonatomic, assign) BOOL skipLoading;



- (IBAction)markPost:(id)sender;
- (IBAction)likePost:(id)sender;
- (IBAction)hugPost:(id)sender;
- (IBAction)meTooPost:(id)sender;

- (void)setupView:(PFObject *)thePost :(PostDetailsViewController *)viewController;
- (void)updatePostStats;
- (void)updatePostHashNotes;
- (void)updatePostHashNoteCount;

@end
