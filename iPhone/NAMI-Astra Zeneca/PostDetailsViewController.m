//
//  PostDetailViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/3/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostDetailsViewController.h"
#import "PostMeTooPopupViewController.h"
#import "PostDetailView.h"
#import "NewPostViewController.h"
#import "AppDelegate.h"

@interface PostDetailsViewController ()
{
    UIView *theView;
    NSUInteger currentIndex;
    NSUInteger maxPageCount;
    NSMutableArray *pages;
    CGFloat lastOffset;
    PostMeTooPopupViewController *pmtvc;
    UIView *popupOverlay;
}

@end

@implementation PostDetailsViewController

@synthesize scrollView,btnLeaveNote,postArray,chosenPostIndex,delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    maxPageCount = MIN(3,postArray.count);
    pages = [[NSMutableArray alloc] initWithCapacity:maxPageCount];
    
    scrollView.delegate = self;
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width * maxPageCount, scrollView.contentSize.height);
    
    currentIndex = chosenPostIndex;
    NSInteger startIdx = 0;
    if (chosenPostIndex >= 1) {
        if (chosenPostIndex == (postArray.count - 1)) {
            if (postArray.count > 2)
                startIdx = chosenPostIndex - 2;
            else
                startIdx = chosenPostIndex - 1;
        }
        else {
            startIdx = chosenPostIndex - 1;
        }
    }

    scrollView.contentOffset = CGPointMake(self.view.frame.size.width * (chosenPostIndex - startIdx), scrollView.contentOffset.y);
    lastOffset = scrollView.contentOffset.x;
    
    CGFloat contentOffset = 0;
    for (int i=0; i<maxPageCount; i++) {
        PostDetailView *pdv = [[[NSBundle mainBundle] loadNibNamed:@"PostDetailView" owner:self options:nil] firstObject];
        //[pdv setupView:[postArray objectAtIndex:chosenPostIndex] :self];
        CGRect frame = pdv.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width;
        pdv.frame = frame;
        //[pdv layoutIfNeeded];
        frame = self.view.frame;
        frame.origin.x = contentOffset;
        UIScrollView *sv = [[UIScrollView alloc] initWithFrame:frame];
        [sv addSubview:pdv];
        //[pdv setupView:[postArray objectAtIndex:(i + startIdx)] :self];
        pdv.tag = 100 + (i + startIdx);
        [self loadPostDetailView:pdv];
        [pages insertObject:sv atIndex:i];
        [scrollView addSubview:sv];
        contentOffset += frame.size.width;
    }
    /*
    pdv = [[[NSBundle mainBundle] loadNibNamed:@"PostDetailView" owner:self options:nil] firstObject];
    //[pdv setupView:[postArray objectAtIndex:chosenPostIndex] :self];
    CGRect frame = pdv.frame;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    pdv.frame = frame;
    //[pdv layoutIfNeeded];
    frame = self.view.frame;
    UIScrollView *sv = [[UIScrollView alloc] initWithFrame:self.view.frame];
    [sv addSubview:pdv];
    [pdv setupView:[postArray objectAtIndex:chosenPostIndex] :self];

    //sv.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    //scrollView.canCancelContentTouches = YES;
    //scrollView.delaysContentTouches = NO;
    //[self layoutIfNeeded];
    /*
    if (pdv.tagsContainerHeight.constant > 0) {
        CGSize sz = pdv.frame.size;
        sz.height = pdv.tagContainerView.frame.origin.y + pdv.tagsContainerHeight.constant + 64;// + pdv.tagsContainerHeight.constant;// + pdv.tagContainerView.frame.size.height;
        sv.contentSize = sz;//scrollView.frame.size;
    }
*/
    
    //[scrollView addSubview:sv];
    [self.view bringSubviewToFront:btnLeaveNote];
    NSLog(@"CurrentIndex = %d",currentIndex);

}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    /*if (((AppDelegate *)[[UIApplication sharedApplication] delegate]).inspiredPost != nil) {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    */
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"";
}

-(void)viewWillDisappear:(BOOL)animated {
    self.title = @"Cancel";
    
    PFObject *post = [postArray objectAtIndex:0];
    
    //Uncomment this if we need to hide these controls for the user's own posts
    PFUser *postUser = [post objectForKey:@"user"];
    if ([postUser.objectId isEqualToString:[[PFUser currentUser] objectId]]) {
        
        // Set to stay on my posts.
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.stayOnMyPosts = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)loadPostDetailView:(PostDetailView *)pdv
{
    NSUInteger index = pdv.tag - 100;
    [pdv setupView:[postArray objectAtIndex:index] :self];
    pdv.tag = 1;
    
    // Hide leave note button for self posts
    PFUser *postUser = [[postArray objectAtIndex:index] objectForKey:@"user"];
    btnLeaveNote.hidden = ([postUser.objectId isEqualToString:[[PFUser currentUser] objectId]]);
}

- (IBAction)leaveNote:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NoteSelectionViewController *vc = [sb instantiateViewControllerWithIdentifier:@"NoteSelectionViewController"];
    vc.delegate = self;
    vc.listMode = LIST_MODE_NOTES;
    [self.navigationController pushViewController:vc animated:YES];

}

-(void)userSelectedNotes:(NSArray *)selectionStates {
    [self.navigationController popViewControllerAnimated:YES];
    NSMutableArray *selectedNotes = [[NSMutableArray alloc] init];
    NSMutableArray *selectedNames = [[NSMutableArray alloc] init];
    NSArray *hashNoteModels = [AppDelegate getHashNoteModels];
    for (int i=0; i<hashNoteModels.count; i++) {
        if ([[selectionStates objectAtIndex:i] boolValue]) {
            [selectedNotes addObject:[hashNoteModels objectAtIndex:i]];
            [selectedNames addObject:[[hashNoteModels objectAtIndex:i] objectForKey:@"name"]];
        }
    }
    
    
    // Return if no notes selected.
    if (selectedNotes.count == 0)
        return;
    
    // Get the view.
    int pageIndex = (int)(scrollView.contentOffset.x) % 320;
    UIScrollView *sv = [pages objectAtIndex:pageIndex];
    PostDetailView *pdv = (PostDetailView *)[sv viewWithTag:1];
    pdv.skipLoading = YES;
    

    // Create the post note.
    PFObject *currentPost = [postArray objectAtIndex:currentIndex];
    PFObject *postNotes = [PFObject objectWithClassName:@"PostNote"];
    [postNotes setObject:currentPost forKey:@"post"];
    [postNotes setObject:[PFUser currentUser] forKey:@"user"];
    [postNotes setObject:selectedNames forKey:@"notes"];
    
    // Update the view.
    [pdv.notes insertObject:postNotes atIndex:0];
    [pdv setupView:[postArray objectAtIndex:currentIndex] :self];
    [pdv updatePostHashNoteCount];
    
    // Save the post note.
    [postNotes saveInBackgroundWithBlock:^(BOOL success, NSError *error) {
        if (success) {
            
            // Update the note count.
            NSInteger noteCount = [[pdv.post objectForKey:@"noteCount"] integerValue];
            [pdv.post setObject:[NSNumber numberWithInt:++noteCount] forKey:@"noteCount"];
            
            
            
            NSDictionary *params = @{@"postId":currentPost.objectId,@"notes":selectedNames};
            [PFCloud callFunctionInBackground:@"postUpdateNotes" withParameters:params block:^(id object, NSError *error) {
                if (error) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Leave Note Error"
                                                                    message:error.description
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }];
        } else {
            NSLog(@"Error : %@", error);
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
        }
    }];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint scrolledOffset = scrollView.contentOffset;
    if (pages.count == 3) {
        if (scrolledOffset.x == 0) {
            if (currentIndex > 1) {
                UIScrollView *svToRecycle = [pages objectAtIndex:2];
                UIScrollView *sv1 = [pages objectAtIndex:1];
                UIScrollView *sv0 = [pages objectAtIndex:0];
                CGRect frame = sv1.frame;
                frame.origin.x = 640;
                sv1.frame = frame;
                frame = sv0.frame;
                frame.origin.x = 320;
                sv0.frame = frame;
                frame = svToRecycle.frame;
                frame.origin.x = 0;
                svToRecycle.frame = frame;
                scrollView.contentOffset = CGPointMake(320,scrollView.contentOffset.y);
                [pages removeObjectAtIndex:2];
                [pages removeObjectAtIndex:1];
                [pages removeObjectAtIndex:0];
                [pages insertObject:svToRecycle atIndex:0];
                [pages insertObject:sv0 atIndex:1];
                [pages insertObject:sv1 atIndex:2];
                PostDetailView *pdv = (PostDetailView *)[svToRecycle viewWithTag:1];
                pdv.tag = 100 + (currentIndex - 2);
                [self performSelector:@selector(loadPostDetailView:) withObject:pdv afterDelay:0.1];
            }
            if (currentIndex > 0) currentIndex--;
        }
        if (scrolledOffset.x == 640) {
            if (currentIndex < (postArray.count - 2)) {
                UIScrollView *svToRecycle = [pages objectAtIndex:0];
                UIScrollView *sv1 = [pages objectAtIndex:1];
                UIScrollView *sv2 = [pages objectAtIndex:2];
                CGRect frame = sv1.frame;
                frame.origin.x = 0;
                sv1.frame = frame;
                frame = sv2.frame;
                frame.origin.x = 320;
                sv2.frame = frame;
                frame = svToRecycle.frame;
                frame.origin.x = 640;
                svToRecycle.frame = frame;
                scrollView.contentOffset = CGPointMake(320,scrollView.contentOffset.y);
                [pages removeObjectAtIndex:2];
                [pages removeObjectAtIndex:1];
                [pages removeObjectAtIndex:0];
                [pages insertObject:sv1 atIndex:0];
                [pages insertObject:sv2 atIndex:1];
                [pages insertObject:svToRecycle atIndex:2];
                PostDetailView *pdv = (PostDetailView *)[svToRecycle viewWithTag:1];
                pdv.tag = 100 + (currentIndex + 2);
                [self performSelector:@selector(loadPostDetailView:) withObject:pdv afterDelay:0.1];
            }
            currentIndex++;
            if (currentIndex == postArray.count) currentIndex--;
        }
        if (scrolledOffset.x == 320) {
            if (lastOffset != 320) {
                if (currentIndex == 0) currentIndex++;
                else currentIndex--;
            }
        }
        lastOffset = scrollView.contentOffset.x;
        
        
    }
    else if (pages.count == 2) {
        if (scrolledOffset.x == 320) {
            currentIndex = 1;
        }
        else {
            currentIndex = 0;
        }
    }
    NSLog(@"CurrentIndex = %d",currentIndex);
}


#pragma mark - PostPopupDelegate
-(void)okButtonClicked:(id)sender
{
    UIViewController *popupvc = (UIViewController *)sender;
    UIView *v = popupvc.view;
    if (v) {
        [v removeFromSuperview];
        if (popupOverlay) [popupOverlay removeFromSuperview];
        if (v.tag == POST_POPUP_METOO) {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            NewPostViewController *vc = [sb instantiateViewControllerWithIdentifier:@"NewPostViewController"];
            vc.inspiringPost = [postArray objectAtIndex:currentIndex];
            //vc.delegate = self;
            vc.postUpdateDelegate = self;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
}

-(void)cancelButtonClicked:(id)sender
{
    UIViewController *popupvc = (UIViewController *)sender;
    UIView *v = popupvc.view;
    if (v) [v removeFromSuperview];
    if (popupOverlay) [popupOverlay removeFromSuperview];
}


#pragma mark - PostDetailUpdateDelegate
-(void)updatePostDetailView:(PFObject *)post {
    
    
    // Get the view.
    int pageIndex = (int)(scrollView.contentOffset.x) % 320;
    UIScrollView *sv = [pages objectAtIndex:pageIndex];
    PostDetailView *pdv = (PostDetailView *)[sv viewWithTag:1];

    [pdv setupView:post :self];
    [pdv updatePostStats];

    UINavigationController *nav = self.navigationController;
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PostsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostsViewController"];
    vc.inspirationPost = post;
    [nav pushViewController:vc animated:YES];

}

-(void)showMeTooPopup {
    
    popupOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height + 40.0f)];
    popupOverlay.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.8f];
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:popupOverlay];
    
    pmtvc = [[PostMeTooPopupViewController alloc] initWithNibName:@"PostMeTooPopupViewController" bundle:nil];
    [currentWindow addSubview:pmtvc.view];
    pmtvc.delegate = self;
    pmtvc.view.center = self.view.center;
}


@end
