//
//  PostsViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Adam Behnke on 11/18/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "konstant.h"
#import "PostsViewController.h"
#import "PostDetailsViewController.h"
#import "PostMeTooPopupViewController.h"
#import "NewPostViewController.h"
#import "UYLTextCell.h"
#import "UYLCollectionViewTextCell.h"
#import "UYLInspiredHeaderCollectionViewCell.h"
#import "THSpringyFlowLayout.h"
#import "AppDelegate.h"

static NSString * const UYLBasicCellIdentifier = @"UYLTextCell";
static NSString * const UYLEmptyCellIdentifier = @"UYLEmptyTextCell";
static NSString * const UYLSearchEmptyCellIdentifier = @"UYLSearchEmptyTextCell";
static NSString * const UYLInspiredHeader = @"UYLInspiredHeader";

@interface PostsViewController ()
{
    UIViewController *tourController;
    PostMeTooPopupViewController *pmtvc;
    NSInteger currentIndex;
    NSInteger reloadIndex;
    UYLCollectionViewTextCell *sizingCell;
    NSMutableDictionary *cellHeights;
    UIView *overlay;
    UIView *popupOverlay;
    NSArray *inspiredPostControllers;
}

@end

@implementation PostsViewController

@synthesize postArray,searchTerm,searchField,inspirationPost, refreshControl;


+(NSString *)daysSinceDate:(NSDate *)date
{
    NSString *rc;
    NSString *units  = @"day";
    int secs = (int)(date.timeIntervalSinceNow * -1);
    int days = secs / 86400;
    if (days == 0) {
        units  = @"hour";
        days = secs / 3600;
        if (days == 0) {
            units  = @"minute";
            days = secs / 60;
        }
    }
    BOOL plural = NO;
    if (days) {
        if (days > 1) plural = YES;
        rc = [NSString stringWithFormat:@"%d %@%@", (int)days, units,(plural)?@"s":@""];
    }
    else {
        rc = @"less than a minute";
    }
    rc = [rc stringByAppendingString:@" ago"];
    return rc;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    PFUser *user = [PFUser currentUser];
//    if (user.username == nil) {
//        [self performSegueWithIdentifier:@"login" sender:self];
//    }
    
    UINib *cellNib = [UINib nibWithNibName:@"PostFeedView" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:UYLBasicCellIdentifier];
    sizingCell = [[cellNib instantiateWithOwner:nil options:nil] objectAtIndex:0];
    
    UINib *emptyCellNib = [UINib nibWithNibName:@"PostFeedEmptyView" bundle:nil];
    [self.collectionView registerNib:emptyCellNib forCellWithReuseIdentifier:UYLEmptyCellIdentifier];
    
    UINib *searchEmptyCellNib = [UINib nibWithNibName:@"PostFeedSearchEmptyView" bundle:nil];
    [self.collectionView registerNib:searchEmptyCellNib forCellWithReuseIdentifier:UYLSearchEmptyCellIdentifier];
    
    UINib *inspiredHeaderNib = [UINib nibWithNibName:@"InspiredPostFeedHeader" bundle:nil];
    [self.collectionView registerNib:inspiredHeaderNib forCellWithReuseIdentifier:UYLInspiredHeader];

    refreshControl = [[UIRefreshControl alloc]init];
    refreshControl.tintColor = [UIColor whiteColor];
    //[self.tableView addSubview:refreshControl];
    [self.collectionView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];


    if (!searchTerm && !inspirationPost) {
        //TODO: segmented control diplay changes not working, specifically font and removing the borders
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"All Posts", @"My Posts", nil]];
        //[statFilter setSegmentedControlStyle:UISegmentedControlStyleBar];
        [_segmentedControl sizeToFit];
        ((AppDelegate *)[UIApplication sharedApplication].delegate).selectedFeed = _segmentedControl.selectedSegmentIndex = 0;
        self.navigationItem.titleView = _segmentedControl;
        
        [_segmentedControl addTarget:self action:@selector(feedSelectionChanged:) forControlEvents:UIControlEventValueChanged];
        [_segmentedControl setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial" size:14.0],
                NSForegroundColorAttributeName:[UIColor colorWithRed:103.0f/255.0f green:138.0f/255.0f blue:2.0f/255.0f alpha:1.0]}
                forState:UIControlStateHighlighted];
        [_segmentedControl setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial" size:14.0],
                                                    NSForegroundColorAttributeName:[UIColor colorWithRed:103.0f/255.0f green:138.0f/255.0f blue:2.0f/255.0f alpha:1.0]}
                                         forState:UIControlStateSelected];
        [_segmentedControl setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial" size:14.0],
                NSForegroundColorAttributeName:[UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0]}
                forState:UIControlStateNormal];
        [_segmentedControl.layer setBorderWidth:0];
        _segmentedControl.tintColor = [UIColor clearColor];
    }
    else if (searchTerm) {
        searchTerm = [searchTerm lowercaseString];
        searchTerm = [searchTerm stringByReplacingOccurrencesOfString:@"#" withString:@""];
        searchTerm = [@"#" stringByAppendingString:searchTerm];
        self.title = searchTerm;
    }
    
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;


    reloadIndex = -1;
    cellHeights = [[NSMutableDictionary alloc] init];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedOut) name:@"NAMIUserLoggedOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showInspiredPost:) name:@"NAMIShowInspiredPost" object:nil];
    
}



- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    
    if (inspiredPostControllers != nil) {
        [self.navigationController setViewControllers:inspiredPostControllers animated:YES];
        inspiredPostControllers = nil;
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        [self showGuidedTour];
    }
    
    BOOL tourShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"tourshown"];
    if (tourShown && [PFUser currentUser] != nil) {
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (delegate.popupPayload != nil) {
            
            [delegate showPopup:delegate.popupPayload];
            delegate.popupPayload = nil;
        }
    }
}

- (void)showInspiredPost:(NSNotification *) notification {
    
    PFObject *inspiredPost = [notification.userInfo objectForKey:@"inspiredPost"];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
    vc.postArray = @[inspiredPost];
    vc.chosenPostIndex = 0; //(NSUInteger)indexPath.row;

    
    PostsViewController *vc2 = [sb instantiateViewControllerWithIdentifier:@"PostsViewController"];
    vc2.inspirationPost = inspiredPost;
    
    inspiredPostControllers = [NSArray arrayWithObjects:[self.navigationController.viewControllers objectAtIndex:0], vc, vc2, nil];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)tourSwipe:(UISwipeGestureRecognizer *)gesture {
    
    // Get the scroll view.
    UIView *view = tourController.view;
    UIScrollView *scrollView = (UIScrollView *)[view viewWithTag:1];
    
    // Get the page.
    int page = scrollView.contentOffset.x / 320.0f;
    
    if (gesture.direction == UISwipeGestureRecognizerDirectionLeft) {
        
        page++;
        if (page == 4) {
            [self skipTutorial:nil];
            return;
        }
        
    } else if (gesture.direction == UISwipeGestureRecognizerDirectionRight) {
        
        page--;
        if (page < 0)
            return;
    }
    
    [scrollView setContentOffset:CGPointMake(page * 320.0f, 0.0f) animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    
    if ([PFUser currentUser] == nil) {
        overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height + 40.0f)];
        overlay.tag = 9999;
        overlay.backgroundColor = [UIColor colorWithRed:(92.0f/255.0f) green:(124.0f/255.0f) blue:(42.0f/255.0f) alpha:1.0f];
        UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
        [currentWindow addSubview:overlay];
    } else {
        
        UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
        UIView *overlayView = [currentWindow viewWithTag:9999];
        if (overlayView != nil) [overlayView removeFromSuperview];
        overlay = nil;
    }

    
    PFUser *user = [PFUser currentUser];
    //TODO: Uncomment this so the user only has to log in once
    //NSLog(@"MainFC::user::%@", user.username);
    if (user.username == nil) {
        [self performSegueWithIdentifier:@"login" sender:self];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn) name:@"NAMIUserLoggedIn" object:nil];
    }
    
    if (searchTerm) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [self showGuidedTour];
    }
    
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Check for stay on my.
    if (delegate.stayOnMyPosts == NO) {
        
        // Check for scroll to top.
        if (((AppDelegate *)[[UIApplication sharedApplication] delegate]).scrollToTopAllPosts == YES && (postArray != nil && postArray.count > 0)) {
            //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
            ((AppDelegate *)[[UIApplication sharedApplication] delegate]).scrollToTopAllPosts = NO;
        }
        
        
        
        // Clear the my posts selection.
        if (((AppDelegate *)[[UIApplication sharedApplication] delegate]).selectedFeed == 1) {
            
            postArray = nil;
            ((AppDelegate *)[[UIApplication sharedApplication] delegate]).selectedFeed = 0;
            //[self.tableView reloadData];
            // cellHeights = [[NSMutableDictionary alloc] init];
            [((THSpringyFlowLayout *)self.collectionView.collectionViewLayout) resetLayout];
            [self.collectionView reloadData];
            self.segmentedControl.selectedSegmentIndex = 0;
        }
    }
    
    // Clear the stay on my posts.
    delegate.stayOnMyPosts = NO;
    
    // Reload any index.
    if (reloadIndex >= 0) {
        //[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:reloadIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:reloadIndex inSection:0]]];
        reloadIndex = -1;
    }
    
    //self.navigationController.navigationBarHidden = YES;
    if ([PFUser currentUser]) {
        [self refreshButtonHandler:nil];
    }
    
    
    if (self.inspirationPost != nil) {
        self.title = @"Inspired Posts";
        refreshControl.enabled = NO;
    }
    
}

-(void) showGuidedTour
{
    
    BOOL tourShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"tourshown"];
    if (!tourShown) {
        
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc1 = [sb instantiateViewControllerWithIdentifier:@"TourScreen1"];
        UIViewController *vc2 = [sb instantiateViewControllerWithIdentifier:@"TourScreen2"];
        UIViewController *vc3 = [sb instantiateViewControllerWithIdentifier:@"TourScreen3"];
        UIViewController *vc4 = [sb instantiateViewControllerWithIdentifier:@"TourScreen4"];
        
        UIScrollView *sv = (UIScrollView *)[vc1.view viewWithTag:1];
        UIButton *skipButton = (UIButton *)[vc1.view viewWithTag:2];
        [skipButton addTarget:self action:@selector(skipTutorial:) forControlEvents:UIControlEventTouchUpInside];
        
        
        CGRect frame = vc2.view.frame;
        frame.origin.x = 320;
        vc2.view.frame = frame;
        [vc1 addChildViewController:vc2];
        [sv addSubview:vc2.view];
        [vc2 didMoveToParentViewController:vc1];
        
        
        frame = vc3.view.frame;
        frame.origin.x = 640;
        vc3.view.frame = frame;
        [vc1 addChildViewController:vc3];
        [sv addSubview:vc3.view];
        [vc3 didMoveToParentViewController:vc1];
        
        frame = vc4.view.frame;
        frame.origin.x = 960;
        vc4.view.frame = frame;
        [vc1 addChildViewController:vc4];
        [sv addSubview:vc4.view];
        [vc4 didMoveToParentViewController:vc1];
        
        
        
        UIImageView *tour1 = (UIImageView *)[vc1.view viewWithTag:1000];
        UIImageView *tour2 = (UIImageView *)[vc2.view viewWithTag:1000];
        UIImageView *tour3 = (UIImageView *)[vc3.view viewWithTag:1000];
        UIImageView *tour4 = (UIImageView *)[vc4.view viewWithTag:1000];
        
        if (vc1.view.frame.size.height < 500) {
            
            [tour1 removeFromSuperview];
            [tour2 removeFromSuperview];
            [tour3 removeFromSuperview];
            [tour4 removeFromSuperview];
            tour1 = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 480.0f)];
            tour2 = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 480.0f)];
            tour3 = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 480.0f)];
            tour4 = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 480.0f)];
            
            tour1.image = [UIImage imageNamed:@"Screen1_4"];
            tour2.image = [UIImage imageNamed:@"Screen2_4"];
            tour3.image = [UIImage imageNamed:@"Screen3_4"];
            tour4.image = [UIImage imageNamed:@"Screen4_4"];
            
            [[vc1.view viewWithTag:1001] addSubview:tour1];
            [[vc2.view viewWithTag:1001] addSubview:tour2];
            [[vc3.view viewWithTag:1001] addSubview:tour3];
            [[vc4.view viewWithTag:1001] addSubview:tour4];
            
            
            
            CGRect skipRect = skipButton.frame;
            skipRect.origin.y -= 87;
            [skipButton removeFromSuperview];
            UIButton *newSkipButton = [[UIButton alloc] initWithFrame:skipRect];
            newSkipButton.backgroundColor = [UIColor clearColor];
            [[vc1.view viewWithTag:1001] addSubview:newSkipButton];
            [newSkipButton addTarget:self action:@selector(skipTutorial:) forControlEvents:UIControlEventTouchUpInside];
            
            /*
             tour1.contentMode = UIViewContentModeScaleAspectFit;
             tour2.contentMode = UIViewContentModeScaleAspectFit;
             tour3.contentMode = UIViewContentModeScaleAspectFit;
             tour4.contentMode = UIViewContentModeScaleAspectFit;
             */
        } else {
            
            tour1.image = [UIImage imageNamed:@"Screen1"];
            tour2.image = [UIImage imageNamed:@"Screen2"];
            tour3.image = [UIImage imageNamed:@"Screen3"];
            tour4.image = [UIImage imageNamed:@"Screen4"];
        }
        
        
        sv.scrollEnabled = NO;
        
        UISwipeGestureRecognizer *leftSwipe =[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tourSwipe:)];
        UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tourSwipe:)];
        [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
        [rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
        [sv addGestureRecognizer:leftSwipe];
        [sv addGestureRecognizer:rightSwipe];
        
        /*
         UIButton *finishTourButton = [[UIButton alloc] initWithFrame:vc4.view.frame];
         [finishTourButton addTarget:self action:@selector(skipTutorial:) forControlEvents:UIControlEventTouchUpInside];
         [sv addSubview:finishTourButton];
         */
        
        sv.contentSize = CGSizeMake(1280,self.view.frame.size.height);
        tourController = vc1;
        [self presentViewController:tourController animated:NO completion:nil];
    }
}


-(void)userLoggedIn
{
    ((AppDelegate *)[UIApplication sharedApplication].delegate).selectedFeed = _segmentedControl.selectedSegmentIndex = 0;
    [self refreshButtonHandler:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NAMIUserLoggedIn" object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [[PFInstallation currentInstallation] setObject:[PFUser currentUser] forKey:@"user"];
    [[PFInstallation currentInstallation] saveInBackground];
}

-(void)userLoggedOut
{
    
    [PFUser logOut];
    [[PFInstallation currentInstallation] setObject:[NSNull null] forKey:@"user"];
    [[PFInstallation currentInstallation] saveInBackground];
    
    postArray = @[];
    reloadIndex = -1;
    //[self.tableView reloadData];
    cellHeights = [[NSMutableDictionary alloc] init];
    [((THSpringyFlowLayout *)self.collectionView.collectionViewLayout) resetLayout];
    [self.collectionView reloadData];
    
    [self.tabBarController.viewControllers[0] popToRootViewControllerAnimated:NO];
    
    UITabBarController *tbc = self.tabBarController;
    tbc.selectedIndex = 0;
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)refreshTable {
    
    if (self.inspirationPost != nil) {
        [refreshControl endRefreshing];
        return;
    }
   
    
    [self refreshButtonHandler:nil];
}

-(void)feedSelectionChanged:(id)sender
{

    
    ((AppDelegate *)[UIApplication sharedApplication].delegate).selectedFeed = _segmentedControl.selectedSegmentIndex;
    if ([PFUser currentUser]) {
        if (postArray != nil && postArray.count > 0) {
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
        } else {
            [((THSpringyFlowLayout *)self.collectionView.collectionViewLayout) resetLayout];
        }
        
        [self refreshButtonHandler:nil];
    }
}

-(void)skipTutorial:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tourshown"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (tourController) {
        NSArray *childVcs = [[tourController childViewControllers] copy];
        for (UIViewController *vc in childVcs) {
            [vc.view removeFromSuperview];
            [vc removeFromParentViewController];
        }
    }
    [self dismissViewControllerAnimated:YES completion:^{
        tourController = nil;
    }];
}

- (void)refreshButtonHandler:(id)sender
{
    //[refreshControl beginRefreshing];
    


    PFUser *user = [PFUser currentUser];
    //Create query for all Post object by the current user
    PFQuery *postQuery = [PFQuery queryWithClassName:@"Post"];
    
    if (inspirationPost) {
        [postQuery whereKey:@"inspiredByPost" equalTo:inspirationPost];
        // Hide any private posts
        [postQuery whereKey:@"isPrivate" equalTo:[NSNumber numberWithBool:FALSE]];
        
        // Hide any hidden posts.
        NSArray *postsHidden = [user objectForKey:@"postsHidden"];
        if (postsHidden || postsHidden.count) {
            [postQuery whereKey:@"objectId" notContainedIn:postsHidden];
        }
        
        // Only show approved posts
        [postQuery whereKey:@"approvedStatus" equalTo:[NSNumber numberWithInt:POST_APPROVAL_APPROVED]];
        
        // Hide any blocked authors.
        NSArray *authorsBlocked = [user objectForKey:@"authorsBlocked"];
        if (authorsBlocked || authorsBlocked.count) {
            [postQuery whereKey:@"user" notContainedIn:authorsBlocked];
        }
        [postQuery orderByDescending:@"createdAt"];
        // Run the query
        [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                postArray = objects;
                //[self.tableView reloadData];
                // cellHeights = [[NSMutableDictionary alloc] init];
                [((THSpringyFlowLayout *)self.collectionView.collectionViewLayout) resetLayout];
                [self.collectionView reloadData];

            } else {
                
                NSLog(@"Error:%@", error);
                [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
            }
        }];
        return;
    }
    
    if (((AppDelegate *)[UIApplication sharedApplication].delegate).selectedFeed == 1) {
        
        // Only show posts from current user
        [postQuery whereKey:@"user" equalTo:user];
        
    }
    else {
        // Only show approved posts
        [postQuery whereKey:@"approvedStatus" equalTo:[NSNumber numberWithInt:POST_APPROVAL_APPROVED]];
            
        // Show just subscribed types
        [postQuery whereKey:@"userType" equalTo:user[@"userType"]];
        
        // Hide any private posts
        [postQuery whereKey:@"isPrivate" equalTo:[NSNumber numberWithBool:FALSE]];
        
        // Hide any hidden posts.
        NSArray *postsHidden = [user objectForKey:@"postsHidden"];
        if (postsHidden || postsHidden.count) {
            [postQuery whereKey:@"objectId" notContainedIn:postsHidden];
        }
        
        // Hide any blocked authors.
        NSArray *authorsBlocked = [user objectForKey:@"authorsBlocked"];
        if (authorsBlocked || authorsBlocked.count) {
            [postQuery whereKey:@"user" notContainedIn:authorsBlocked];
        }
    }
    [postQuery orderByDescending:@"createdAt"];
    
    __block UIActivityIndicatorView *progress = nil;
    
    if ([refreshControl isRefreshing] == NO) {
        
        progress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        progress.color = [UIColor darkGrayColor];
        [self.view addSubview:progress];
        progress.center = self.view.center;
        [progress startAnimating];
    }
    
    
    if (searchTerm) {
        if ([searchField isEqualToString:@"both"]) {
            [postQuery whereKey:@"hashtags" equalTo:searchTerm];
            PFQuery *q2 = [postQuery copy];
            [q2 whereKey:@"hashnotes" equalTo:searchTerm];
            postQuery = [PFQuery orQueryWithSubqueries:@[postQuery, q2]];
        }
        else [postQuery whereKey:searchField equalTo:searchTerm];
    }     
    // Run the query
    [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //Save results and update the table
            postArray = @[];
            //[((THSpringyFlowLayout *)self.collectionView.collectionViewLayout) resetLayout];
            //[self.collectionView reloadData];
            postArray = objects;
            
            // Create the post pointers.
            NSMutableArray *postPointers = [[NSMutableArray alloc] init];
            for (PFObject *postItem in postArray) {
                [postPointers addObject:postItem.objectId];
            }
            
            // Create the params and call the function.
            NSDictionary *params = @{@"posts": postPointers};
            [PFCloud callFunctionInBackground:@"queryPostsSocialNotifications" withParameters:params block:^(id object, NSError *cloudError) {
                
                if (!cloudError) {
                    
                    // Get the social statuses.
                    NSArray *userPostLikes = (NSArray *)[object valueForKey:@"postLikes"];
                    NSArray *userPostHugs = (NSArray *)[object valueForKey:@"postHugs"];
                    NSArray *userPostMeToos = (NSArray *)[object valueForKey:@"postMeToos"];
                    
                    // Add the social statuses.
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    [appDelegate addUserPostLikes:userPostLikes];
                    [appDelegate addUserPostHugs:userPostHugs];
                    [appDelegate addUserPostMeToos:userPostMeToos];
                    
                } else {
                    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
                }
                
                [refreshControl endRefreshing];
                //[self.tableView reloadData];
                // cellHeights = [[NSMutableDictionary alloc] init];
                
                if (progress != nil) {
                    [progress stopAnimating];
                    [progress removeFromSuperview];
                }
                
                [self performSelector:@selector(reloadData) withObject:nil afterDelay:0.1];
            }];
            
            
            
            // Load the moderator flagged posts.
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if (appDelegate.selectedFeed == 1) {
                [appDelegate loadModeratorFlagPosts];
            }
        } else {
            
            [refreshControl endRefreshing];
            
            NSLog(@"Error :%@",error);
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
        }
    }];
 
}

- (void)reloadData {
    [((THSpringyFlowLayout *)self.collectionView.collectionViewLayout) resetLayout];
    [self.collectionView reloadData];
    self.collectionView.alwaysBounceVertical = YES;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.selectedFeed == 1) {
        if (postArray != nil && postArray.count > 0) {
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
        }
    }
}

- (IBAction)indexChanged:(UISegmentedControl *)sender {
    if ([PFUser currentUser]) {
        [self refreshButtonHandler:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"GetSearchTerm"]) {
        SearchTermViewController *vc = segue.destinationViewController;
        vc.delegate = self;
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark - Collection View


- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
  
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (searchTerm != nil && postArray.count == 0)
        return 1;
    else if (appDelegate.selectedFeed == 1 && postArray.count == 0)
        return 1;
    
    if (self.inspirationPost != nil)
        return (postArray.count + 1);
    
    // Return the number of rows in the section.
    return postArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (searchTerm != nil && postArray.count == 0) {
        
        UICollectionViewCell *noresults = [self.collectionView dequeueReusableCellWithReuseIdentifier:UYLSearchEmptyCellIdentifier forIndexPath:indexPath];
        return noresults;
    }
    else if (appDelegate.selectedFeed == 1 && postArray.count == 0) {
        

        UICollectionViewCell *noresults = [self.collectionView dequeueReusableCellWithReuseIdentifier:UYLEmptyCellIdentifier forIndexPath:indexPath];
        return noresults;
    }
    if (self.inspirationPost != nil && indexPath.row == 0) {
        UYLInspiredHeaderCollectionViewCell *inspiredHeader = [cv dequeueReusableCellWithReuseIdentifier:UYLInspiredHeader forIndexPath:indexPath];
        [self configureInspiredHeadingCell:inspiredHeader];
        return inspiredHeader;
    }
    
    UYLCollectionViewTextCell *cell = [cv dequeueReusableCellWithReuseIdentifier:UYLBasicCellIdentifier forIndexPath:indexPath];
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.inspirationPost != nil && indexPath.row == 0) {
        return CGSizeMake(self.view.frame.size.width, 60.0f);
    }
    
    if (postArray.count == 0) {
        
        CGFloat tableHeight = self.collectionView.bounds.size.height;
        return CGSizeMake(self.view.frame.size.width, tableHeight - 65.0f);
    }
    
    return CGSizeMake(self.view.frame.size.width, [self heightForBasicCellAtIndexPath:indexPath]);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (postArray.count == 0) {
        
        return;
    }
    
    if (self.inspirationPost != nil) {
        
        if (indexPath.row == 0) {
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
            vc.postArray = @[self.inspirationPost];
            vc.chosenPostIndex = 0; //(NSUInteger)indexPath.row;
            reloadIndex = indexPath.row;
            [self.navigationController pushViewController:vc animated:YES];
            
        } else {
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
            vc.postArray = @[[postArray objectAtIndex:(indexPath.row - 1)]];
            vc.chosenPostIndex = 0; //(NSUInteger)indexPath.row;
            reloadIndex = indexPath.row;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    } else {
        
    
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
        vc.postArray = @[[postArray objectAtIndex:indexPath.row]];
        vc.chosenPostIndex = 0; //(NSUInteger)indexPath.row;
        reloadIndex = indexPath.row;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (searchTerm != nil && postArray.count == 0)
        return 1;
    else if (appDelegate.selectedFeed == 1 && postArray.count == 0)
        return 1;
    
    
    // Return the number of rows in the section.
    return postArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (searchTerm != nil && postArray.count == 0) {
        
        UITableViewCell *noresults = [tableView dequeueReusableCellWithIdentifier:@"noresultssearch" forIndexPath:indexPath];
        return noresults;
    }
    else if (appDelegate.selectedFeed == 1 && postArray.count == 0) {
        
        UITableViewCell *noresults = [tableView dequeueReusableCellWithIdentifier:@"noresults" forIndexPath:indexPath];
        return noresults;
    }
    
    
    
    UYLTextCell *cell = [tableView dequeueReusableCellWithIdentifier:UYLBasicCellIdentifier forIndexPath:indexPath];
    [self configureBasicCell:cell atIndexPath:indexPath];
    //[cell setNeedsLayout];
    //[cell layoutIfNeeded];

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.selectedFeed == 1 && postArray.count == 0) {
        
        return;
    }
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
    vc.postArray = @[[postArray objectAtIndex:indexPath.row]];
    vc.chosenPostIndex = 0; //(NSUInteger)indexPath.row;
    reloadIndex = indexPath.row;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)configureBasicCell:(/*UYLTextCell*/ UYLCollectionViewTextCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    //UYLTextCell *textCell = (UYLTextCell *)cell;
    UYLCollectionViewTextCell *textCell = (UYLCollectionViewTextCell *)cell;
    
    
    textCell.meTooButton.layer.cornerRadius = 3.0f;
    textCell.likeButton.layer.cornerRadius = 3.0f;
    textCell.hugButton.layer.cornerRadius = 3.0f;
    
    //alternate display of white border labels
    if(indexPath.row % 2 == 0) {
        [textCell.leftBorder setHidden:TRUE];
        [textCell.rightBorder setHidden:FALSE];
    } else {
        [textCell.leftBorder setHidden:FALSE];
        [textCell.rightBorder setHidden:TRUE];
    }
    
    textCell.pendingContainerHeight.constant = 0;
    textCell.flagContainerHeight.constant = 0;
    [textCell.pendingView setHidden:YES];
    [textCell.flagView setHidden:YES];
    
    
    textCell.backgroundMask.layer.cornerRadius = 7.0f;
    textCell.backgroundMask.layer.masksToBounds = YES;
    textCell.backgroundMask.backgroundColor = [UIColor whiteColor];
    
    PFObject *post = nil;
    if (self.inspirationPost != nil)
        post = [postArray objectAtIndex:(indexPath.row - 1)];
    else post = [postArray objectAtIndex:indexPath.row];
    

    //NSLog(@"post: %@",post);
    NSString *rawText = [post objectForKey:@"text"];
    
    NSInteger approval = [[post objectForKey:@"approvedStatus"] integerValue];
    if (approval != POST_APPROVAL_APPROVED) {
        if (approval == 0) {
            textCell.pendingContainerHeight.constant = 20;
            textCell.pendingLabel.text = @"Your post is pending.";
            textCell.pendingImage.image = [UIImage imageNamed:@"ribbon-orange"];
            textCell.pendingLabel.textColor = [UIColor colorWithRed:(252.0f/255.0f) green:(78.0f/255.0f) blue:(23.0f/255.0f) alpha:1.0];
            [textCell.pendingView setHidden:NO];
        } else if (approval == 2) {
            textCell.pendingContainerHeight.constant = 20;
            textCell.pendingLabel.text = [NSString stringWithFormat:@"Your post has been disapproved."];
            textCell.pendingImage.image = [UIImage imageNamed:@"ribbon-red"];
            textCell.pendingLabel.textColor = [UIColor colorWithRed:(255.0f/255.0f) green:0.0f blue:0.0f alpha:1.0f];
            [textCell.pendingView setHidden:NO];
        }
        else textCell.pendingContainerHeight.constant = 0;
    }
    NSInteger flagged = [AppDelegate getFlaggedPostStatus:post];
    NSInteger moderatorFlagged = [AppDelegate getModeratorFlaggedPostStats:post];
    if (moderatorFlagged != 0 && ((AppDelegate *)[UIApplication sharedApplication].delegate).selectedFeed == 1) {
        textCell.flagContainerHeight.constant = 20;
        textCell.lblFlagBanner.text = [self constructFlaggedText:moderatorFlagged format:@"Your post was flagged as %@."];
        [textCell.flagView setHidden:NO];
    } else if (flagged != 0) {
        textCell.flagContainerHeight.constant = 20;        
        textCell.lblFlagBanner.text = [self constructFlaggedText:flagged format:@"You flagged this post as %@."];
        [textCell.flagView setHidden:NO];
    }
    
    //textCell.lineLabel.attributedText = [self attributedMessageFromMessage:rawText];
    //textCell.lineLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];

    
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTapped:)];
    [textCell.txtContent addGestureRecognizer:gr];
    
    
    textCell.txtContent.attributedText = [self attributedMessageFromMessage:rawText];
    //textCell.txtContent.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    //NSLog(@"att: %@", textCell.lineLabel.attributedText);
    
    CGSize maximumLabelSize = CGSizeMake(textCell.txtContent.frame.size.width,CGFLOAT_MAX);
    CGSize requiredSize = [textCell.txtContent sizeThatFits:maximumLabelSize];

    textCell.contentHeight.constant = requiredSize.height;
    
    [self updatePostStats:textCell post:post];
    
    
    NSDate *startDate = post.createdAt;
    textCell.daysAgo.text = [PostsViewController daysSinceDate:startDate];//[textCell.daysAgo.text stringByAppendingString:@" ago"];

    textCell.txtContent.tag = 1500 + indexPath.row;

}

- (void)configureInspiredHeadingCell:(UYLInspiredHeaderCollectionViewCell *)cell {
    
    // Set the border radius.
    cell.headerView.layer.cornerRadius = 5.0f;
    
    
    
    // Create the initial attributes.
    UIFont *font = [UIFont fontWithName:@"ProximaNova-Semibold" size:16.0];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.minimumLineHeight = 20.0f;
    paragraphStyle.maximumLineHeight = 20.0f;
    
    NSDictionary *attrsDictionary = @{NSFontAttributeName:font,
                                      NSParagraphStyleAttributeName:paragraphStyle
                                      };
    
    // Create the go to detail font.
    NSString *gotoDetail = @" GO TO POST DETAIL>";
    UIFont *gotoFont = [UIFont fontWithName:@"MuseoSlab-500" size:14.0];
    
    // Create the message.
    NSString *text = [NSString stringWithFormat:@"Posts inspired by \"%@\"%@", [self.inspirationPost objectForKey:@"text"], gotoDetail];
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:text attributes:attrsDictionary];
    
    // Update the message.
    NSRange gotoRange = [text rangeOfString:gotoDetail options:NSBackwardsSearch];
    [attributedMessage addAttribute:NSFontAttributeName value:gotoFont range:gotoRange];
    
    // Set the text.
    //cell.label.attributedText = attributedMessage;
    
    
    // Set the number of lines.
    NSInteger lines = 2;
    
    // Delcare the ellipsis.
    NSMutableAttributedString *ellipsis = [[NSMutableAttributedString alloc] initWithString:@"..." attributes:attrsDictionary];
    
    NSMutableAttributedString *truncatedString = [attributedMessage mutableCopy];
    if ([self numberOfLinesNeeded:truncatedString font:font cell:cell] > lines) {
        [truncatedString insertAttributedString:ellipsis atIndex:(gotoRange.location - 1)];
        NSRange range = NSMakeRange(truncatedString.length - gotoDetail.length - 1 - (ellipsis.length + 1), 1);
        while ([self numberOfLinesNeeded:truncatedString font:font cell:cell] > lines) {
            [truncatedString deleteCharactersInRange:range];
            range.location--;
        }
        [truncatedString deleteCharactersInRange:range];  //need to delete one more to make it fit
        CGRect labelFrame = cell.label.frame;
        labelFrame.size.height = [@"A" sizeWithFont:font].height * lines;
        cell.label.frame = labelFrame;
        cell.label.attributedText = truncatedString;
        
    }else{
        /*
        CGRect labelFrame = cell.label.frame;
        labelFrame.size.height = [@"A" sizeWithFont:font].height * lines;
        cell.label.frame = labelFrame;
        */
        cell.label.attributedText = attributedMessage;
        
    }
    
    [cell.label sizeToFit];
}

-(int)numberOfLinesNeeded:(NSMutableAttributedString *)s font:(UIFont *)font cell:(UYLInspiredHeaderCollectionViewCell *)cell {
    float oneLineHeight = [@"A" sizeWithFont:font].height;
    CGRect bounds = cell.label.bounds;
    CGRect boundingRect = [s boundingRectWithSize:CGSizeMake(bounds.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:NULL];
    float totalHeight = boundingRect.size.height;
    return nearbyint(totalHeight/oneLineHeight);
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (postArray.count == 0) {
        
        CGFloat tableHeight = self.tableView.bounds.size.height;
        return tableHeight - 65.0f;
    }
    
    return [self heightForBasicCellAtIndexPath:indexPath];
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    //static UYLTextCell *sizingCell = nil;
    //static UYLCollectionViewTextCell *sizingCell = nil;
    //static dispatch_once_t onceToken;
    
    /*
    dispatch_once(&onceToken, ^{
        //sizingCell = [self.tableView dequeueReusableCellWithIdentifier:UYLBasicCellIdentifier];
        sizingCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:UYLBasicCellIdentifier forIndexPath:indexPath];
    });
    */
    
    if (self.inspirationPost != nil) {
        
        if (indexPath.row >= (postArray.count + 1))
            return 0.1;
    } else if (indexPath.row >= postArray.count)
        return 0.1;
    
    
    
    PFObject *post = nil;
    if (self.inspirationPost != nil)
        post = [postArray objectAtIndex:(indexPath.row - 1)];
    else post = [postArray objectAtIndex:indexPath.row];

    CGFloat height = 0.0f;
    if ([cellHeights objectForKey:post.objectId] != nil) {
        
        height = [[cellHeights objectForKey:post.objectId] floatValue];
        return height;
    }

    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    height = [self calculateHeightForConfiguredSizingCell:sizingCell];
    
    [cellHeights setObject:[NSNumber numberWithFloat:height] forKey:post.objectId];
    
    return height;
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(/*UYLTextCell */ UYLCollectionViewTextCell *)cell {
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    //CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    CGSize size = [cell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 2.0f;
}

#pragma mark - configure attributedMessage

- (NSAttributedString *)attributedMessageFromMessage:(NSString *)message {
    
    NSString * const hashTagKey = @"HashTag";
    NSString * const normalKey = @"NormalKey";
    NSString * const wordType = @"WordType";
    UIColor *postBlue = [UIColor colorWithRed:23.0f/255.0f green:135.0f/255.0f blue:151.0f/255.0f alpha:1.0];
    UIColor *postGreen = [UIColor colorWithRed:103.0f/255.0f green:138.0f/255.0f blue:2.0f/255.0f alpha:1.0];
    UIColor *postGray = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    
    if (message.length > 250) {
        message = [NSString stringWithFormat:@"%@...", [message substringToIndex:250]];
    }

    NSString *originalMessage = [NSString stringWithString:message];
    message = [message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSArray* messageWords = [message componentsSeparatedByString: @" "];

    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:@""];
    NSInteger sentenceFlag = 0;
    
    for (NSString *partialWord in messageWords) {
        unsigned long previousIndex = 0;
        unsigned long newlineIndex = 0;
        NSMutableArray *partialWords = [[NSMutableArray alloc] init];
        while (newlineIndex < partialWord.length && [partialWord rangeOfString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(newlineIndex, partialWord.length - newlineIndex) ].location != NSNotFound) {
            newlineIndex = [partialWord rangeOfString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(newlineIndex, partialWord.length - newlineIndex) ].location;
            if (newlineIndex - previousIndex > 0) {
                [partialWords addObject:[partialWord substringWithRange:NSMakeRange(previousIndex, newlineIndex - previousIndex)]];
            }
            [partialWords addObject:@"\n"];
            newlineIndex++;
            previousIndex = newlineIndex;
        }
        
        if (partialWords.count == 0)
            [partialWords addObject:partialWord];
        else {
            
            if (newlineIndex < partialWord.length) {
                [partialWords addObject:[partialWord substringWithRange:NSMakeRange(newlineIndex, partialWord.length - newlineIndex)]];
            }
        }
        
        for (NSString *word in partialWords) {
            
            NSDictionary * attributes;
            //NSLog(@"word: ::%@::%@", word, [word substringToIndex:1]);
            
            // TODO: get hashtags to link to search screen when tapped
            if ([word length] == 0) {
                //NSLog(@"WHITESPACE ERROR");
            }
            /*else if ([word rangeOfString:@"#"].location != NSNotFound) {
             NSRange startRange = [word rangeOfString:@"#"];
             NSRange endRange = [word range]
             }*/
            else if ([[word substringToIndex:1] isEqualToString:@"#"]) {
                attributes = @{NSForegroundColorAttributeName:postBlue, wordType: hashTagKey, hashTagKey:[word substringFromIndex:1]};
                
            }
            else {
                if (sentenceFlag) {
                    attributes = @{NSForegroundColorAttributeName:[UIColor blackColor], wordType: normalKey};
                } else {
                    attributes = @{NSForegroundColorAttributeName:postGreen, wordType: normalKey};
                }
                
                if ([[word substringFromIndex: [word length] - 1] isEqualToString:@"."]) {
                    sentenceFlag = 1;
                }
            }
            
            NSAttributedString * subString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",word] attributes:attributes];
            [attributedMessage appendAttributedString:subString];
        
        }
    }
    
    // Find the feeling -
    NSString *currentMessage = [attributedMessage string];
    NSRange feelingRange = [currentMessage rangeOfString:@"- Feeling" options:NSBackwardsSearch];
    if (feelingRange.length > 0) {
        
        NSDictionary *attributes = @{NSForegroundColorAttributeName:[UIColor blackColor], wordType: normalKey};
        NSRange range = NSMakeRange(feelingRange.location, currentMessage.length - feelingRange.location);
        [attributedMessage setAttributes:attributes range:range];
    }
    
//    
//    //split string
//    NSArray* textFormat = [message componentsSeparatedByString: @"."];
//    NSString* firstSentence = [textFormat objectAtIndex: 0];
//    
//    // set attributed text properties
//    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:message];
//    NSInteger _stringLength = [firstSentence length];
//    
//    
//    //TODO: font size not working
//    UIFont *font = [UIFont fontWithName:@"Arial" size:50.0];
//    
//    [attString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, _stringLength)];
//    [attString addAttribute:NSForegroundColorAttributeName
//        value:postGreen range:NSMakeRange(0, _stringLength)];
    
    UIFont *font = [UIFont fontWithName:@"Arial" size:16];
    [attributedMessage addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [attributedMessage length])];
 
    
    if (originalMessage.length > 250) {
        
        NSMutableAttributedString *continueReading = [[NSMutableAttributedString alloc] initWithString:@"CONTINUE READING"];
        //TODO: font size not working on continue reading
        [continueReading addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Arial" size:12.0] range:NSMakeRange(0, [continueReading length])];
        [continueReading addAttribute:NSForegroundColorAttributeName
            value:postGray range:NSMakeRange(0, [continueReading length])];
        [attributedMessage appendAttributedString:continueReading];
    }
    
    return attributedMessage;
}

- (void)textTapped:(UITapGestureRecognizer *)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        NSDictionary *attrs = [textView.textStorage attributesAtIndex:characterIndex effectiveRange:&range];
        
        // Handle as required...
        NSString *wordType  = [attrs objectForKey:@"WordType"];
        if ([wordType isEqualToString:@"HashTag"]) {
            [self userEnteredSearchTerm:[attrs objectForKey:@"HashTag"]];
        }
        else {
            
            if (self.inspirationPost != nil) {
                
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
                vc.delegate = self;
                vc.postArray = @[[postArray objectAtIndex:(textView.tag - 1501)]];
                vc.chosenPostIndex = 0;
                [self.navigationController pushViewController:vc animated:YES];
                
            } else {
                
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                PostDetailsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostDetailsViewController"];
                vc.delegate = self;
                vc.postArray = @[[postArray objectAtIndex:(textView.tag - 1500)]];
                vc.chosenPostIndex = 0;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
        
       // NSLog(@"%@, %d, %d", attrs, range.location, range.length);
        
    }
}

-(void)userEnteredSearchTerm:(NSString *)term
{
    [self userEnteredSearchTerm:term searchAll:NO];
}

-(void)userEnteredSearchTerm:(NSString *)term searchAll:(BOOL)searchAll
{
    if (!term || term.length == 0) return;
    term = [term lowercaseString];
    term = [term stringByReplacingOccurrencesOfString:@"#" withString:@""];
    term = [@"#" stringByAppendingString:term];
    term = [term stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    term = [term stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
    UINavigationController *nav = self.navigationController;
    [self dismissViewControllerAnimated:NO completion:nil];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PostsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PostsViewController"];
    vc.searchTerm = term;
    if (searchAll)
        vc.searchField = @"both";
    else
        vc.searchField = @"hashtags";
    [nav pushViewController:vc animated:YES];
}

-(void)userHidPost:(PFObject *)post {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hide Post"
                                                    message:@"The post was hidden."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)userBlockedAuthorOfPost:(PFObject *)post {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Blocked"
                                                    message:@"The post's author was blocked."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}



//- (IBAction)messageTapped:(UITapGestureRecognizer *)recognizer {
//    NSLog(@"tapped.");
//    NSString * const hashTagKey = @"HashTag";
//    NSString * const normalKey = @"NormalKey";
//    NSString * const wordType = @"WordType";
//    
//    UITextView *textView = (UITextView *)recognizer.view;
//    
//    NSLayoutManager *layoutManager = textView.layoutManager;
//    CGPoint location = [recognizer locationInView:textView];
//    
//    NSUInteger characterIndex;
//    characterIndex = [layoutManager characterIndexForPoint:location inTextContainer:textView.textContainer                 fractionOfDistanceBetweenInsertionPoints:NULL];
//    
//    if (characterIndex < textView.textStorage.length) {
//        
//        NSRange range;
//        id wordType = [textView.attributedText attribute:wordType atIndex:characterIndex effectiveRange:&range];
//        
//        if([wordType isEqualToString:hashTagKey]){
//            // TODO: Segue to hashtag controller once it is in place.
//            NSLog(@"SEGUE to HASHTAG");
//        }
//    }
//}

- (IBAction)likePost:(id)sender {
    
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.selectedFeed == 1) return;
    
    // Disable the button.
    __block UIButton *button = (UIButton *)sender;
    button.enabled = NO;
    
    // Get the post
    //CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.collectionView];
    //NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:buttonPosition];
    //UYLTextCell *cell = (UYLTextCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    UYLCollectionViewTextCell *cell = (UYLCollectionViewTextCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    PFObject *post = [postArray objectAtIndex:indexPath.row];
    __block PFObject *social = [appDelegate getPostLike:post.objectId];
    
    if (social != nil) {
        
        [social deleteInBackgroundWithBlock:^(BOOL success, NSError *error){
            if (success) {
                cell.likeButton.selected = NO;
                
                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubType":@(NOTIFICATION_SUBTYPE_LIKE)};
                [PFCloud callFunctionInBackground:@"notificationDelete" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                    } else  {
                        
                        int likeCnt = [[post objectForKey:@"likeCount"] intValue];
                        likeCnt--;
                        [post setObject:@(likeCnt) forKey:@"likeCount"];
                        [appDelegate removePostLike:social];
                        [self updatePostStats:cell post:post];
                    }
                }];
            } else {
                
                [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
            }
            
            // Enable the button.
            button.enabled = YES;
        }];
        
        
    }
    else {
        
        PFObject *postLike = [PFObject objectWithClassName:@"PostLike"];
        [postLike setObject:post forKey:@"post"];
        [postLike setObject:[PFUser currentUser] forKey:@"user"];
        [postLike saveInBackgroundWithBlock:^(BOOL success, NSError *error){
            if (success) {
                social= postLike;
                cell.likeButton.selected = YES;
                cell.likeButton.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
                
                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubType":@(NOTIFICATION_SUBTYPE_LIKE)};
                [PFCloud callFunctionInBackground:@"notificationCreate" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                    } else {
                        NSLog(@"current like count: %@",[post objectForKey:@"likeCount"]);
                        int likeCnt = [[post objectForKey:@"likeCount"] intValue];
                        likeCnt++;
                        [post setObject:@(likeCnt) forKey:@"likeCount"];
                        NSLog(@"new like count: %@",[post objectForKey:@"likeCount"]);
                        [appDelegate addUserPostLikes:@[postLike]];
                        [self updatePostStats:cell post:post];
                    }
                }];
            } else {
                
                [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
            }
            
            // Enable the button.
            button.enabled = YES;
        }];
        
    }
    
    
}

- (IBAction)hugPost:(id)sender {
    
    // Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.selectedFeed == 1) return;
    
    // Disable the button.
    __block UIButton *button = (UIButton *)sender;
    button.enabled = NO;
    
    // Get the post
    //CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.collectionView];
    //NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:buttonPosition];
    //UYLTextCell *cell = (UYLTextCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    UYLCollectionViewTextCell *cell = (UYLCollectionViewTextCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    PFObject *post = [postArray objectAtIndex:indexPath.row];
    __block PFObject *social = [appDelegate getPostHug:post.objectId];
    
    if (social != nil) {
        
        [social deleteInBackgroundWithBlock:^(BOOL success, NSError *error){
            if (success) {
                cell.hugButton.selected = NO;
                
                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubType":@(NOTIFICATION_SUBTYPE_HUG)};
                [PFCloud callFunctionInBackground:@"notificationDelete" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                    } else {
                        
                        int hugCnt = [[post objectForKey:@"hugCount"] intValue];
                        hugCnt--;
                        [post setObject:@(hugCnt) forKey:@"hugCount"];
                        [appDelegate removePostHug:social];
                        [self updatePostStats:cell post:post];
                    }
                }];
            } else {
                
                [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
            }
            
            // Enable the button.
            button.enabled = YES;
        }];
        
        
    }
    else {
        
        PFObject *postHug = [PFObject objectWithClassName:@"PostHug"];
        [postHug setObject:post forKey:@"post"];
        [postHug setObject:[PFUser currentUser] forKey:@"user"];
        [postHug saveInBackgroundWithBlock:^(BOOL success, NSError *error){
            if (success) {
                social= postHug;
                cell.hugButton.selected = YES;
                cell.hugButton.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
                
                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubType":@(NOTIFICATION_SUBTYPE_HUG)};
                [PFCloud callFunctionInBackground:@"notificationCreate" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                    } else {
                        
                        int hugCnt = [[post objectForKey:@"hugCount"] intValue];
                        hugCnt++;
                        [post setObject:@(hugCnt) forKey:@"hugCount"];
                        [appDelegate addUserPostHugs:@[postHug]];
                        [self updatePostStats:cell post:post];
                    }
                }];
            } else {
                
                [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
            }
            
            // Enable the button.
            button.enabled = YES;
        }];
        
    }
}

- (IBAction)meTooPost:(id)sender {
    
	// Get the deletgate.
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.selectedFeed == 1) return;
    
    // Disable the button.
    __block UIButton *button = (UIButton *)sender;
    button.enabled = NO;
    
    // Get the post
    //CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.collectionView];
    //NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:buttonPosition];
    //UYLTextCell *cell = (UYLTextCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    UYLCollectionViewTextCell *cell = (UYLCollectionViewTextCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    PFObject *post = [postArray objectAtIndex:indexPath.row];
    __block PFObject *social = [appDelegate getPostMeToo:post.objectId];

    
    // Set the current index.
    currentIndex = indexPath.row;
    
    popupOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height + 40.0f)];
    popupOverlay.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.8f];
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:popupOverlay];
    
    pmtvc = [[PostMeTooPopupViewController alloc] initWithNibName:@"PostMeTooPopupViewController" bundle:nil];
    [currentWindow addSubview:pmtvc.view];
    pmtvc.delegate = self;
    CGPoint center = self.view.center;
    pmtvc.view.center = self.view.center;
    
    if (social != nil) {
        
        
    }
    else {
        
        PFObject *postMeToo = [PFObject objectWithClassName:@"PostMeToo"];
        [postMeToo setObject:post forKey:@"post"];
        [postMeToo setObject:[PFUser currentUser] forKey:@"user"];
        [postMeToo saveInBackgroundWithBlock:^(BOOL success, NSError *error){
            if (success) {
                social= postMeToo;
                cell.meTooButton.selected = YES;
                cell.meTooButton.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
                
               
                NSDictionary *params = @{@"postId":post.objectId,@"notificationType":@(NOTIFICATION_TYPE_SOCIAL),@"notificationSubType":@(NOTIFICATION_SUBTYPE_METOO)};
                [PFCloud callFunctionInBackground:@"notificationCreate" withParameters:params block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"Error:%@",error);
                    } else {
                        
                        int meTooCnt = [[post objectForKey:@"meTooCount"] intValue];
                        meTooCnt++;
                        [post setObject:@(meTooCnt) forKey:@"meTooCount"];
                        
                        [appDelegate addUserPostMeToos:@[postMeToo]];
                        [self updatePostStats:cell post:post];
                    }
                }];
            } else {
                
                [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
            }
            
            // Enable the button.
            button.enabled = YES;
        }];
        
    }   
}

-(void)updatePostStats:(/*UYLTextCell*/ UYLCollectionViewTextCell *)textCell post:(PFObject *)post
{
    
    // Add the social counts.
    [textCell.likeButton setTitle:[NSString stringWithFormat:@"%@", [post objectForKey:@"likeCount"]] forState:UIControlStateNormal];
    [textCell.likeButton setTitle:[NSString stringWithFormat:@"%@", [post objectForKey:@"likeCount"]] forState:UIControlStateSelected];
    [textCell.hugButton setTitle:[NSString stringWithFormat:@"%@", [post objectForKey:@"hugCount"]] forState:UIControlStateNormal];
    [textCell.hugButton setTitle:[NSString stringWithFormat:@"%@", [post objectForKey:@"hugCount"]] forState:UIControlStateSelected];
    [textCell.meTooButton setTitle:[NSString stringWithFormat:@"%@", [post objectForKey:@"meTooCount"]] forState:UIControlStateNormal];
    [textCell.meTooButton setTitle:[NSString stringWithFormat:@"%@", [post objectForKey:@"meTooCount"]] forState:UIControlStateSelected];
    
    // Set the social status.
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([appDelegate hasPostLike:post.objectId]) {
        [textCell.likeButton setSelected:YES];
        textCell.likeButton.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
    } else {
        [textCell.likeButton setSelected:NO];
        textCell.likeButton.backgroundColor = [UIColor colorWithRed:0.86 green:0.86 blue:0.86 alpha:1];
    }
    
    if ([appDelegate hasPostHug:post.objectId]) {
        [textCell.hugButton setSelected:YES];
        textCell.hugButton.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
    } else {
        [textCell.hugButton setSelected:NO];
        textCell.hugButton.backgroundColor = [UIColor colorWithRed:0.86 green:0.86 blue:0.86 alpha:1];
    }
    
    if ([appDelegate hasPostMeToo:post.objectId]) {
        [textCell.meTooButton setSelected:YES];
        textCell.meTooButton.backgroundColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1];
    } else {
        [textCell.meTooButton setSelected:NO];
        textCell.meTooButton.backgroundColor = [UIColor colorWithRed:0.86 green:0.86 blue:0.86 alpha:1];
    }
    
    // Check the user.
    if ([((PFObject *)[post objectForKey:@"user"]).objectId isEqualToString:[PFUser currentUser].objectId]) {
        
        [textCell.likeButton setEnabled:NO];
        [textCell.hugButton setEnabled:NO];
        [textCell.meTooButton setEnabled:NO];
    } else {
        [textCell.likeButton setEnabled:YES];
        [textCell.hugButton setEnabled:YES];
        [textCell.meTooButton setEnabled:YES];
    }

    
}

#pragma mark - PostPopupDelegate
-(void)okButtonClicked:(id)sender
{
    UIViewController *popupvc = (UIViewController *)sender;
    UIView *v = popupvc.view;
    if (v) {
        if (v.tag == POST_POPUP_METOO) {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            NewPostViewController *vc = [sb instantiateViewControllerWithIdentifier:@"NewPostViewController"];
            vc.inspiringPost = [postArray objectAtIndex:currentIndex];
            //vc.delegate = self;
            vc.fromFeed = YES;
            [self.navigationController pushViewController:vc animated:YES];
          
        }
        [v removeFromSuperview];
    }
    
    if (popupOverlay) [popupOverlay removeFromSuperview];
}

-(void)cancelButtonClicked:(id)sender
{
    UIViewController *popupvc = (UIViewController *)sender;
    UIView *v = popupvc.view;
    if (v) [v removeFromSuperview];
    if (popupOverlay) [popupOverlay removeFromSuperview];	
}

- (NSString *) constructFlaggedText:(NSInteger)flag format:(NSString *)format {
    
    NSString *flagString;
    switch (flag) {
        case POST_FLAG_INAPPROPRIATE:
            flagString = @"inappropriate";
            break;
        case POST_FLAG_SELF_HARM:
            flagString = @"self harm";
            break;
        case POST_FLAG_THREATENING:
            flagString = @"threatening";
            break;
        case POST_FLAG_BULLYING:
            flagString = @"bullying";
            break;
        case POST_FLAG_SPAM:
            flagString = @"spam";
            break;
    }
    
    return [NSString stringWithFormat:format, flagString];
}

@end
