//
//  Note.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <Parse/Parse.h>

@interface Note : PFObject<PFSubclassing>

@property (strong) NSString *displayName;

@property NSString *name;
@property NSString *nameSearchable;

+(NSString *)parseClassName;

@end
