//
//  PostDetailScrollView.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/4/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostDetailScrollView : UIScrollView

@end
