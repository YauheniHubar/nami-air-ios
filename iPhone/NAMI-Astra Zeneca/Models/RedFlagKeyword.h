//
//  RedFlagKeyword.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/18/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <Parse/Parse.h>

@interface RedFlagKeyword : PFObject<PFSubclassing>

@property (strong) NSString *displayName;

@property NSString *name;

+(NSString *)parseClassName;

@end
