//
//  AppDelegate.m
//  NAMI-Astra Zeneca
//
//  Created by Adam Behnke on 10/30/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "AppDelegate.h"
#import "Models.h"
#import "ModeratorNotificationViewController.h"
#import "PopupViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

static NSArray *hashNoteModels = nil;
static NSArray *feelingModels = nil;
static NSArray *redFlagModels = nil;
static NSMutableDictionary *postFlagMap = nil;
static NSMutableDictionary *userPostLikes = nil;
static NSMutableDictionary *userPostHugs = nil;
static NSMutableDictionary *userPostMeToos = nil;
static NSMutableDictionary *modFlagMap = nil;

@interface AppDelegate ()
{
    ModeratorNotificationViewController *mnvc;
    PopupViewController *pvc;
    UIView *overlay;
}

@end

@implementation AppDelegate

@synthesize selectedFeed, scrollToTopAllPosts, stayOnMyPosts, unreadNotificationId, connectionAlertView, inspiredPost, popupPayload;

+(NSString *)getHashNoteName:(PFObject *)noteObj
{
    NSString *name = @"";
    for (PFObject *hashTagName in hashNoteModels) {
        if ([hashTagName.objectId isEqualToString:noteObj.objectId]) {
            name = [hashTagName objectForKey:@"name"];
            break;
        }
    }
    return name;
}

+(NSArray *)getHashNoteModels
{
    return hashNoteModels;
}

+(NSArray *)getFeelingModels
{
    return feelingModels;
}

+(NSArray *)getRedFlagWordModels
{
    return redFlagModels;
}

+(NSInteger)getFlaggedPostStatus:(PFObject *)post
{
    return [[postFlagMap objectForKey:post.objectId] integerValue];
}
+(NSInteger)getModeratorFlaggedPostStats:(PFObject *)post
{
    PFObject *flag = [modFlagMap objectForKey:post.objectId];
    if (flag != nil) {
        return [[flag valueForKey:@"flaggedStatus"] integerValue];
    }
    
    return 0;
}


-(void)initHashNoteModels
{
    if (hashNoteModels) return;
    //Create query for all Post object by the current user
    PFQuery *query = [Note query];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //Save results and update the table
            hashNoteModels = objects;
        } else {
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
        }
    }];
    
}

-(void)initFeelingModels
{
    if (feelingModels) return;
    //Create query for all Post object by the current user
    PFQuery *query = [Feeling query];
    [query orderByAscending:@"name"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //Save results and update the table
            feelingModels = objects;
        } else {
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
        }
    }];
    
}

-(void)initRedFlagModels
{
    if (redFlagModels) return;
    //Create query for all Post object by the current user
    PFQuery *query = [RedFlagKeyword query];
    [query orderByAscending:@"name"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //Save results and update the table
            redFlagModels = objects;
        } else {
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
        }
    }];
}

-(void)loadModeratorFlagPosts{
    
    modFlagMap = [[NSMutableDictionary alloc] init];

    /*
    if ([PFUser currentUser] == nil)
        return;
    
    PFQuery *query = [PostFlag moderatorPostFlagQuery];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            modFlagMap = [[NSMutableDictionary alloc] init];
            for (PFObject *flag in objects) {
                PFObject *post = [flag objectForKey:@"post"];
                [modFlagMap setObject:flag forKey:post.objectId];
            }
            
        }
        
    }];
    */
}

-(void)loadUnreadModeratorNotifications {
    
    if ([PFUser currentUser] == nil)
        return;
    
    PFQuery *query = [PostNotification queryForUnreadModeratorotifications];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            for (PFObject *notification in objects) {
                NSString *notificationId = notification.objectId;
                BOOL read = [[notification objectForKey:@"read"] boolValue];
                
                if (read == false) {
                    [self showModeratorMessage:notificationId];
                    break;
                }
            }
        } else {
            
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
        }
    }];
}

-(void)showModeratorMessage:(NSString *)notificationId {
    
    // Return if already showing message.
    if (self.unreadNotificationId != nil)
        return;
    
    // Set the notification id.
    self.unreadNotificationId = notificationId;
    
    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height + 40.0f)];
    overlay.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.8f];
    [self.window addSubview:overlay];

    mnvc = [[ModeratorNotificationViewController alloc] initWithNibName:@"ModeratorNotificationViewController" bundle:nil];
    [self.window addSubview:mnvc.view];
    mnvc.delegate = self;
    mnvc.view.center = CGPointMake(self.window.center.x, self.window.center.y - 40.0f);
}

-(void)showPopup:(NSDictionary *)payload {
    
    
    
    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height + 40.0f)];
    overlay.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.8f];
    [self.window addSubview:overlay];
    
    
    pvc = [[PopupViewController alloc] initWithNibName:@"Popup" bundle:nil];
    pvc.popupTitle = [payload objectForKey:@"pt"];
    pvc.linkTitle = [payload objectForKey:@"lt"];
    pvc.linkURL = [payload objectForKey:@"lu"];
    
    
    [self.window addSubview:pvc.view];
    pvc.delegate = self;
    

    
    
    pvc.view.center = CGPointMake(self.window.center.x, self.window.center.y - 40.0f);
}

-(void)clearUserSocial {
    
    if (userPostLikes != nil) [userPostLikes removeAllObjects];
    if (userPostHugs != nil) [userPostHugs removeAllObjects];
    if (userPostMeToos != nil) [userPostMeToos removeAllObjects];
}

-(void)addUserPostLikes:(NSArray *)postLikes
{
    if (userPostLikes == nil)
        userPostLikes = [[NSMutableDictionary alloc] init];
    
    if (postLikes != nil)
    {
        for (PFObject *postLike in postLikes) {
            [userPostLikes setObject:postLike forKey:((PFObject *)[postLike objectForKey:@"post"]).objectId];
        }
    }
}

-(void)addUserPostHugs:(NSArray *)postHugs
{
    if (userPostHugs == nil)
        userPostHugs = [[NSMutableDictionary alloc] init];
    
    if (postHugs != nil)
    {
        for (PFObject *postHug in postHugs) {
            [userPostHugs setObject:postHug forKey:((PFObject *)[postHug objectForKey:@"post"]).objectId];
        }
    }
}

-(void)addUserPostMeToos:(NSArray *)postMeToos
{
    if (userPostMeToos == nil)
        userPostMeToos = [[NSMutableDictionary alloc] init];
    
    if (postMeToos != nil) {
        for (PFObject *postMeToo in postMeToos) {
            [userPostMeToos setObject:postMeToo forKey:((PFObject *)[postMeToo objectForKey:@"post"]).objectId];
        }
    }
}

-(BOOL)hasPostLike:(NSString *)postId {
    return [userPostLikes objectForKey:postId] != nil;
}
-(BOOL)hasPostHug:(NSString *)postId {
    return [userPostHugs objectForKey:postId] != nil;
}
-(BOOL)hasPostMeToo:(NSString *)postId {
    return [userPostMeToos objectForKey:postId] != nil;
}
-(PFObject *)getPostLike:(NSString *)postId {
    return [userPostLikes objectForKey:postId];
}
-(PFObject *)getPostHug:(NSString *)postId {
    return [userPostHugs objectForKey:postId];
}
-(PFObject *)getPostMeToo:(NSString *)postId {
    return [userPostMeToos objectForKey:postId];
}
-(void)removePostLike:(PFObject *)postLike {
    [userPostLikes removeObjectForKey:((PFObject *)[postLike objectForKey:@"post"]).objectId];
}
-(void)removePostHug:(PFObject *)postHug {
    [userPostHugs removeObjectForKey:((PFObject *)[postHug objectForKey:@"post"]).objectId];
}
-(void)removePostMeToo:(PFObject *)postMeToo {
    [userPostMeToos removeObjectForKey:((PFObject *)[postMeToo objectForKey:@"post"]).objectId];
}


+(void)updatePostFlagStatus:(backgroundCompletion)completion
{
    if ([PFUser currentUser] == nil)
        return;
    
    if (postFlagMap)
        [postFlagMap removeAllObjects];
    else
        postFlagMap = [[NSMutableDictionary alloc] init];
    PFQuery *query = [PostFlag userPostFlagQuery];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //Save results and update the table
            for (PostFlag *pf in objects) {
                [postFlagMap setObject:@(pf.flaggedStatus) forKey:pf.post.objectId];
            }
        } else {
            
            [((AppDelegate *)[[UIApplication sharedApplication] delegate]) showConnectionError];
        }
        
        completion(YES);
    }];
    
}

- (void) fetchUnreadNotifications {
    
    // Test for a user.
    if ([PFUser currentUser] == nil)
        return;
    
    PFQuery *postQuery = [PostNotification queryForUsersNotifications];
    
    // Run the query
    [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
        
            int unreadCount = 0;
            
            for (int index = 0; index < objects.count; index++) {
                PostNotification *notification = [objects objectAtIndex:index];
                if (!notification.read) unreadCount++;
            }
            
            // Get the notification controller.
            UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
            UIViewController *viewController = [tabBarController.viewControllers objectAtIndex:1];
            
            if (unreadCount > 0) {
                viewController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%ld",(long)unreadCount];
            }
            else {
                viewController.tabBarItem.badgeValue = nil;
            }
            
            
            
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:unreadCount];
        }
    }];

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [Fabric with:@[[Crashlytics class]]];
    
    [Feeling registerSubclass];
    [Note registerSubclass];
    [Post registerSubclass];
    [PostFlag registerSubclass];
    [PostLike registerSubclass];
    [PostMeToo registerSubclass];
    [PostNote registerSubclass];
    [PostNotification registerSubclass];
    [RedFlagKeyword registerSubclass];
    
    // Stage
//    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
//        configuration.applicationId = @"C7FNB0zU4q5cvtuyFuLHkPwreKUXhl90BOTSzsZQ";
//        configuration.clientKey = @"pkmqxHEmVXNHP56i9sejgvCKiy5o02lKfokx5fcZ";
//        configuration.server = @"https://parseapi.back4app.com";
//    }]];
    
    // Live
    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
        configuration.applicationId = @"uJUkecVnvp90fNExwxXQEphTqcdmVEpDVJE5IWaj";
        configuration.clientKey = @"8CVatJmTRaunYpYI5VaAzCMb8uXCf7hfeXjMJ5Jj";
        configuration.server = @"https://parseapi.back4app.com";
    }]];
    
    // Old Parse Live
//    [Parse setApplicationId:@"uJUkecVnvp90fNExwxXQEphTqcdmVEpDVJE5IWaj" clientKey:@"MPj0nmoRz1rlBLAAr7B35n3dxdfJrGNXGlzke3FQ"];
    
    // Old Parse Stage
    //[Parse setApplicationId:@"C7FNB0zU4q5cvtuyFuLHkPwreKUXhl90BOTSzsZQ" clientKey:@"V1wn99OnxyMoSrPWVKQT2aChRZ30pG80984Trq9S"];
    
    [Parse setLogLevel:PFLogLevelNone];
    
    [Flurry setCrashReportingEnabled:YES];
    [Flurry setAppVersion:[NSString stringWithFormat:@"%@(%@)", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"], [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]]];
    [Flurry startSession:@"6HXKG9PK3H7HD6QTGM6Y"];
    
    
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:18/255.0 green:127/255.0 blue:143/255.0 alpha:1]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:103.0f/255.0f green:138.0f/255.0f blue:2.0f/255.0f alpha:1.0]}];
    [[UITabBar appearance] setSelectionIndicatorImage:[AppDelegate imageFromColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1] forSize:CGSizeMake(64, 49) withCornerRadius:0]];
    
    UITabBarController *tabBar = (UITabBarController *)self.window.rootViewController;
    [tabBar setDelegate:self];

    // Uncomment this to reset the tuor for testing
    //[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"tourshown"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    

    [self registerForNotifications];
    
    // Fetch unread notifications.
    [self fetchUnreadNotifications];
    
    NSDictionary *payload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if(payload)
    {
        NSLog(@"Payload from didFinishLaunching : %@", payload);
        self.popupPayload = payload;        
    }
    
    /*
    NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
    [tmp setValue:@"This is the test title" forKey:@"pt"];
    [tmp setValue:@"This is the link title" forKey:@"lt"];
    [tmp setValue:@"http://www.google.com" forKey:@"lu"];
    self.popupPayload = tmp;
    */
     
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    // Return if there is no user.
    if ([PFUser currentUser] == nil)
        return;
    
    // Store the deviceToken in the current Installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation setObject:[PFUser currentUser] forKey:@"user"];
    [currentInstallation addUniqueObject:@"receivePopups" forKey:@"channels"];
    [currentInstallation saveInBackground];

}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"couldn't register for remote notifications : %@", [error localizedDescription]);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self initHashNoteModels];
    [self initFeelingModels];
    [self initRedFlagModels];
    [self loadModeratorFlagPosts];
    [self loadUnreadModeratorNotifications];
    [AppDelegate updatePostFlagStatus:^(BOOL completed) {
        
        if ([PFUser currentUser]) {
            [[PFUser currentUser] fetch];
            if ([[[PFUser currentUser] objectForKey:@"disabled"] boolValue] == YES) {
                [PFUser logOut];
                [[PFInstallation currentInstallation] deleteInBackground];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NAMIUserLoggedOut" object:nil];
            }
        }
    }];
    
    
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    
    // Check type.
    if ([userInfo objectForKey:@"type"] != nil && [[userInfo objectForKey:@"type"] isEqualToString:@"moderator"]) {
        if ([userInfo objectForKey:@"messageId"] != nil) {
            NSString *messageId = [userInfo objectForKey:@"messageId"];
            
            // Notify the user.
            [self showModeratorMessage:messageId];
        }
    } else if ([userInfo objectForKey:@"type"] != nil && [[userInfo objectForKey:@"type"] isEqualToString:@"popup"]) {
        
        [self showPopup:userInfo];
        
    }

}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    self.stayOnMyPosts = NO;
}

-(void) registerForNotifications {
    
    //This code will work in iOS 8.0 xcode 6.0 or later
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability| UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }

}

+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContext(size);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    // Draw your image
    [image drawInRect:rect];
    
    // Get the image, here setting the UIImageView image
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return image;
}


-(void)moderatorMessageIgnoreButtonClicked:(id)sender {
    
    UIViewController *popupvc = (UIViewController *)sender;
    UIView *v = popupvc.view;
    if (v) [v removeFromSuperview];
    if (overlay) [overlay removeFromSuperview];
    
    self.unreadNotificationId = nil;
    
}
-(void)moderatorMessageOpenButtonClicked:(id)sender {
    
    UIViewController *popupvc = (UIViewController *)sender;
    UIView *v = popupvc.view;
    if (v) [v removeFromSuperview];
    if (overlay) [overlay removeFromSuperview];
    
    
    UITabBarController *tb = (UITabBarController *)self.window.rootViewController;
    tb.selectedIndex = 1;
}


-(void)popupCloseButtonClicked:(id)sender {
    
    UIViewController *popupvc = (UIViewController *)sender;
    UIView *v = popupvc.view;
    if (v) [v removeFromSuperview];
    if (overlay) [overlay removeFromSuperview];
    
}

-(void)showConnectionError {
    
    // Check for already open view.
    if (self.connectionAlertView != nil && self.connectionAlertView.isVisible)
        return;
    
    // Create the alert view.
    self.connectionAlertView = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"We were unable to connect to the database at this time. Please check back later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self.connectionAlertView show];
    
}

@end
