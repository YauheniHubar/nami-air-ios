//
//  Post.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "Post.h"
#import <Parse/PFObject+Subclass.h>

@implementation Post

@dynamic displayName;
@dynamic user;
@dynamic userType;
@dynamic approvedStatus;
@dynamic flaggedStatus;
@dynamic text;
@dynamic isPrivate;
@dynamic inspiredByPost;
@dynamic likeCount;
@dynamic hugCount;
@dynamic meTooCount;
@dynamic meTooStoryCount;
@dynamic noteCount;
@dynamic searchableText;
@dynamic hashtags;
@dynamic hashnotes;
@dynamic author;
@dynamic textContent;
@dynamic userId;

+(NSString *)parseClassName {
    return @"Post";
}


@end
