//
//  WelcomeViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Adam Behnke on 11/18/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

@synthesize bubbleImage, airImage, namiImage, taglineImage;
@synthesize bubbleYConstraint, airYConstraint, welcomeYConstraint;
@synthesize layoutAnchor, welcomeContainer, eulaSpacer, titleSpacer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _createAccountButton.layer.borderWidth=2.0f;
    _createAccountButton.layer.cornerRadius=5.0;
    _createAccountButton.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
    
    
    [self.view layoutIfNeeded];
    [self performSelector:@selector(startWelcomeAnimation) withObject:nil afterDelay:0.5];
    
    
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    UIView *overlay = [currentWindow viewWithTag:9999];
    if (overlay != nil) [overlay removeFromSuperview];
    
    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
    if (screenRect.size.height < 548) {
        self.titleSpacer.constant = 5.0f;
        self.eulaSpacer.constant = 5.0f;
    } else {
        self.titleSpacer.constant = 20.0f;
        self.eulaSpacer.constant = 47.0f;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClicked:(id)sender {
    _createAccountButton.backgroundColor = [UIColor colorWithRed:1.0/255.0 green:103.0/255.0 blue:113.0/255.0 alpha:1.0f];
}

- (void)startWelcomeAnimation {
    
    NSLayoutConstraint *bubbleTopConstraint = [NSLayoutConstraint constraintWithItem:bubbleImage
                                                                           attribute:NSLayoutAttributeBottom
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:layoutAnchor
                                                                           attribute:NSLayoutAttributeTop
                                                                          multiplier:1.0
                                                                            constant:-10];
    
    NSLayoutConstraint *airTopConstraint = [NSLayoutConstraint constraintWithItem:airImage
                                                                           attribute:NSLayoutAttributeCenterY
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:layoutAnchor
                                                                           attribute:NSLayoutAttributeCenterY
                                                                          multiplier:1.0
                                                                         constant:0];
    
    NSLayoutConstraint *airEndingConstraint = [NSLayoutConstraint constraintWithItem:airImage
                                                                        attribute:NSLayoutAttributeTop
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:layoutAnchor
                                                                        attribute:NSLayoutAttributeTop
                                                                       multiplier:1.0
                                                                            constant:20];
    
    NSLayoutConstraint *welcomeTopConstraint = [NSLayoutConstraint constraintWithItem:welcomeContainer
                                                                           attribute:NSLayoutAttributeTop
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:namiImage
                                                                           attribute:NSLayoutAttributeTop
                                                                          multiplier:1.0
                                                                             constant:40];
    
    NSLayoutConstraint *welcomeBottomConstraint = [NSLayoutConstraint constraintWithItem:welcomeContainer
                                                                            attribute:NSLayoutAttributeBottom
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:layoutAnchor
                                                                            attribute:NSLayoutAttributeBottom
                                                                           multiplier:1.0
                                                                             constant:0];
    [self.view removeConstraint:bubbleYConstraint];
    [self.view addConstraint:bubbleTopConstraint];
    
    [self.view removeConstraint:airYConstraint];
    [self.view addConstraint:airTopConstraint];
    
    [UIView animateWithDuration:5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished){
        [UIView animateWithDuration:1.3 animations:^{
            taglineImage.alpha = 1;
            namiImage.alpha = 1;
        } completion:^(BOOL finished){
            [self.view removeConstraint:airTopConstraint];
            [self.view addConstraint:airEndingConstraint];
            [self.view removeConstraint:welcomeYConstraint];
            [self.view addConstraint:welcomeTopConstraint];
            [UIView animateWithDuration:2 animations:^{
                [self.view addConstraint:welcomeBottomConstraint];
                [self.view layoutIfNeeded];
            }];
        }];
    }];
    
}


@end
