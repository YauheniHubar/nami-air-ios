//
//  ModeratorNotificationViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/12/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModeratorNotificationDelegate.h"

@interface ModeratorNotificationViewController : UIViewController


@property (nonatomic, strong) id<ModeratorNotificationDelegate> delegate;
@property (nonatomic, strong) IBOutlet UIView *outerView;
@property (nonatomic, strong) IBOutlet UIButton *ignoreButton;
@property (nonatomic, strong) IBOutlet UIButton *openButton;

-(IBAction) ignoreButtonClicked:(id)sender;
-(IBAction) openButtonClicked:(id)sender;

@end
