//
//  CreateAccountViewController.m
//  ParseExample
//
//  Created by Nick Barrowclough on 8/9/13.
//  Copyright (c) 2013 Nicholas Barrowclough. All rights reserved.
//

#import "CreateAccountViewController.h"
#import "EmailUsedViewController.h"
#import "ChooseGroupViewController.h"
#import "DLRadioButton.h"
#import "konstant.h"
#import "AppDelegate.h"

@interface CreateAccountViewController ()
{
    
    EmailUsedViewController *euvc;
    ChooseGroupViewController *cgvc;
    
    UIView *overlay;
}

@property (strong, nonatomic) IBOutletCollection(DLRadioButton) NSArray *topRadioButtons;
@property (nonatomic) NSArray *bottomRadioButtons;

@end

@implementation CreateAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _createAccountButton.layer.borderWidth=2.0f;
    _createAccountButton.layer.cornerRadius=5.0;
    _createAccountButton.layer.borderColor=[[UIColor whiteColor] CGColor];

    for (DLRadioButton *radioButton in self.topRadioButtons) {
        radioButton.ButtonIcon = [UIImage imageNamed:@"check-white-off.png"];
        radioButton.ButtonIconSelected = [UIImage imageNamed:@"check-white-on.png"];
    }
    
    // Set the navigation items.
    self.navigationItem.title = @"Create an Account";
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButton:)];
    self.navigationItem.leftBarButtonItem = cancel;
}

- (IBAction)cancelButton:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    PFUser *user = [PFUser currentUser];
     if (user.username != nil) {
     //[self performSegueWithIdentifier:@"login" sender:self];
     }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.emailField) {
        [self.passwordField becomeFirstResponder];
    } else if(textField == self.passwordField) {
        [self.reEnterPasswordField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)emailPopup:(id)sender {
    
    [self.view endEditing:YES];
    
    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height + 40.0f)];
    overlay.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.8f];
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:overlay];

    euvc = [[EmailUsedViewController alloc] initWithNibName:@"EmailUsedViewController" bundle:nil];
    [currentWindow addSubview:euvc.view];
    euvc.delegate = self;
    euvc.view.center = CGPointMake(self.view.center.x, self.view.center.y - 40.0f);
}

- (IBAction)registerAction:(id)sender {
    [_usernameField resignFirstResponder];
    [_emailField resignFirstResponder];
    [_passwordField resignFirstResponder];
    [_reEnterPasswordField resignFirstResponder];
    [self checkFieldsComplete];
}

- (void) checkFieldsComplete {
    _usernameField.text = _emailField.text;
    
    DLRadioButton *userButton = [self.topRadioButtons objectAtIndex:0];
    DLRadioButton *caregiverButton = [self.topRadioButtons objectAtIndex:1];
    
    if (![self validateEmail:(_emailField.text)]) {
        
        // Scroll to top.
        [self.scrollView setContentOffset:CGPointMake(0, -self.scrollView.contentInset.top) animated:YES];
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You must enter a valid email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
        _emailErrorLabel.hidden = NO;
        [_emailErrorLabel setText:@"You must enter a valid email address"];
        [_emailErrorLabel setAlpha:0.0];
        [UIView animateWithDuration:0.5 delay:0                  options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                animations:^(void) {[_emailErrorLabel setAlpha:1.0];}
                completion:^(BOOL finished) {
            if(finished) { [UIView animateWithDuration:0.5 delay:1
                options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                    animations:^(void) {[_emailErrorLabel setAlpha:0.0];}
                    completion:^(BOOL finished){
                }];
            }
         }];

    }
//    else if ([_usernameField.text isEqualToString:@""] || [_emailField.text isEqualToString:@""] || [_passwordField.text isEqualToString:@""] || [_reEnterPasswordField.text isEqualToString:@""]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You need to complete all fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//    }
    else if (userButton.isSelected == NO && caregiverButton.isSelected == NO) {
        
        overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height + 40.0f)];
        overlay.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.8f];
        UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
        [currentWindow addSubview:overlay];
        
        cgvc = [[ChooseGroupViewController alloc] initWithNibName:@"ChooseGroupViewController" bundle:nil];
        [currentWindow addSubview:cgvc.view];
        cgvc.delegate = self;
        cgvc.view.center = CGPointMake(self.view.center.x, self.view.center.y - 40.0f);
        
    }
    else {
        [self checkPasswordsMatch];
    }
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

- (void) checkPasswordsMatch {
    if (_passwordField.text.length == 0) {
        
        // Scroll to top.
        [self.scrollView setContentOffset:CGPointMake(0, -self.scrollView.contentInset.top) animated:YES];
        
        _passwordErrorLabel.hidden = NO;
        [_passwordErrorLabel setText:@"Please enter a password"];
        [_passwordErrorLabel setAlpha:0.0];
        [UIView animateWithDuration:0.5 delay:0                  options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                         animations:^(void) {[_passwordErrorLabel setAlpha:1.0];}
                         completion:^(BOOL finished) {
                             if(finished) { [UIView animateWithDuration:0.5 delay:1
                                                                options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                                                             animations:^(void) {[_passwordErrorLabel setAlpha:0.0];}
                                                             completion:^(BOOL finished){
                                                             }];
                             }
                         }];
    }
    else if (![_passwordField.text isEqualToString:_reEnterPasswordField.text]) {
//     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Sorry, your passwords don't match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//     [alert show];
        
        // Scroll to top.
        [self.scrollView setContentOffset:CGPointMake(0, -self.scrollView.contentInset.top) animated:YES];
        
         _passwordErrorLabel.hidden = NO;
         [_passwordErrorLabel setText:@"Sorry, your passwords don't match"];
         [_passwordErrorLabel setAlpha:0.0];
         [UIView animateWithDuration:0.5 delay:0                  options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
              animations:^(void) {[_passwordErrorLabel setAlpha:1.0];}
              completion:^(BOOL finished) {
                  if(finished) { [UIView animateWithDuration:0.5 delay:1
                                                     options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                                                  animations:^(void) {[_passwordErrorLabel setAlpha:0.0];}
                                                  completion:^(BOOL finished){
                                                  }];
                  }
              }];
     }
     else {
     [self registerNewUser];
     }
}

- showSelectedButton:(NSArray *)radioButtons {
    NSString *buttonName = [(DLRadioButton *)radioButtons[0] selectedButton].titleLabel.text;
    //[[[UIAlertView alloc] initWithTitle: buttonName ? @"Selected Button" : @"No Button Selected" message:buttonName delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    return buttonName;
}

- (void) registerNewUser {
    
    [self.view endEditing:YES];
    
    NSLog(@"registering....");
    NSNumber *userTypeInt = 0;
    NSString *userType = [self showSelectedButton:self.topRadioButtons];
    if ([userType isEqual:FINDING]) {
        userTypeInt = [NSNumber numberWithInteger:1];
    } else {
        userTypeInt = [NSNumber numberWithInteger:2];
    }

    PFUser *newUser = [PFUser user];
    //newUser.username = _usernameField.text;
    newUser.username = _emailField.text;
    newUser.email = _emailField.text;
    newUser.password = _passwordField.text;
    [newUser setObject:userTypeInt forKey:@"userType"];
    
    
    __block UIActivityIndicatorView *progress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    progress.color = [UIColor darkGrayColor];
    [self.view addSubview:progress];
    progress.center = self.view.center;
    [progress startAnimating];
    
    
    [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        [progress stopAnimating];
        [progress removeFromSuperview];
        
        if (!error) {
            NSLog(@"Registration success!");
            _loginPasswordField.text = nil;
            _loginUsernameField.text = nil;
            _usernameField.text = nil;
            _passwordField.text = nil;
            _reEnterPasswordField.text = nil;
            _emailField.text = nil;
            //[self performSegueWithIdentifier:@"login" sender:self];
            
            if ([PFUser currentUser]) {
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                [currentInstallation setObject:[PFUser currentUser] forKey:@"user"];
                [currentInstallation saveInBackground];
                
                AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [delegate registerForNotifications];
                delegate.scrollToTopAllPosts = YES;
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else {
            
            
            
            NSLog(@"There was an error in registration");
            
            // Check the error.
            NSString *errorMessage = (NSString *)[[error userInfo] valueForKey:@"error"];
            NSRange errorRange = [errorMessage rangeOfString:@"already taken"];
            if (errorRange.length > 0) {
                
                // Scroll to top.
                [self.scrollView setContentOffset:CGPointMake(0, -self.scrollView.contentInset.top) animated:YES];
                
                _passwordErrorLabel.hidden = NO;
                [_passwordErrorLabel setText:@"An account already exists under this email"];
                [_passwordErrorLabel setAlpha:0.0];
                [UIView animateWithDuration:0.5 delay:0                  options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                                 animations:^(void) {[_passwordErrorLabel setAlpha:1.0];}
                                 completion:^(BOOL finished) {
                                     if(finished) { [UIView animateWithDuration:0.5 delay:1
                                                                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                                                                     animations:^(void) {[_passwordErrorLabel setAlpha:0.0];}
                                                                     completion:^(BOOL finished){
                                                                     }];
                                     }
                                 }];
            }
            
            
            
        }
    }];
}

- (IBAction)registeredButton:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        _loginOverlayView.frame = self.view.frame;
    }];
}

- (IBAction)loginButton:(id)sender {
    _loginUsernameField = _emailField;
    _loginPasswordField = _passwordField;
    NSLog(@"Login user::email::%@", _emailField.text);
    NSLog(@"Login user::username::%@", _loginUsernameField.text);
    NSLog(@"Login user::password::%@", _loginPasswordField.text);
    [PFUser logInWithUsernameInBackground:_loginUsernameField.text password:_loginPasswordField.text block:^(PFUser *user, NSError *error) {
        if (!error) {
            NSLog(@"Login user!");
            _loginPasswordField.text = nil;
            _loginUsernameField.text = nil;
            _usernameField.text = nil;
            _passwordField.text = nil;
            _reEnterPasswordField.text = nil;
            _emailField.text = nil;
            [self performSegueWithIdentifier:@"login" sender:self];
        }
        if (error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Sorry we had a problem logging you in" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

-(void) emailUsedCloseButtonClicked:(id)sender {
    
    UIViewController *popupvc = (UIViewController *)sender;
    UIView *v = popupvc.view;
    if (v) [v removeFromSuperview];
    if (overlay) [overlay removeFromSuperview];
}

-(void) chooseGroupCloseButtonClicked:(id)sender {
    
    UIViewController *popupvc = (UIViewController *)sender;
    UIView *v = popupvc.view;
    if (v) [v removeFromSuperview];
    if (overlay) [overlay removeFromSuperview];
}

- (IBAction) userSelected:(id)sender {
    
    DLRadioButton *userButton = [self.topRadioButtons objectAtIndex:0];
    DLRadioButton *caregiverButton = [self.topRadioButtons objectAtIndex:1];
    
    [userButton setSelected:YES];
    [caregiverButton setSelected:NO];
}
- (IBAction) caregiverSelected:(id)sender {
    
    DLRadioButton *userButton = [self.topRadioButtons objectAtIndex:0];
    DLRadioButton *caregiverButton = [self.topRadioButtons objectAtIndex:1];
    
    [userButton setSelected:NO];
    [caregiverButton setSelected:YES];
}

@end
