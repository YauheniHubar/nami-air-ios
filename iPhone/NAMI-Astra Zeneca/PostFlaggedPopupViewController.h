//
//  PostFlaggedPopupViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/11/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostPopupDelegate.h"

@interface PostFlaggedPopupViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnOk;
@property (strong, nonatomic) IBOutlet UILabel *lblMessage;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) id<PostPopupDelegate> delegate;

- (IBAction)okButtonClicked:(id)sender;

@end
