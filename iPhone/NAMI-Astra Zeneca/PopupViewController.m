//
//  PopupViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 10/5/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import "PopupViewController.h"

@interface PopupViewController ()

@end

@implementation PopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblPopupTitle.text = self.popupTitle;
    self.btnLink.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    if (self.linkTitle != nil && self.linkTitle.length > 0) {
    
        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:self.linkTitle];
    
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
        [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
        [title addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"ProximaNova-Regular" size:19.0]
                  range:NSMakeRange(0, self.linkTitle.length)];
    
        [title addAttribute:NSUnderlineStyleAttributeName
                  value:@(NSUnderlineStyleSingle)
                  range:NSMakeRange(0, self.linkTitle.length)];
    
        [title addAttribute:NSForegroundColorAttributeName
                  value:[UIColor darkGrayColor]
                  range:NSMakeRange(0, self.linkTitle.length)];
    
        [title addAttribute:NSParagraphStyleAttributeName
                  value:paragraphStyle
                  range:NSMakeRange(0, self.linkTitle.length)];
    
        [self.btnLink setAttributedTitle:title forState:UIControlStateNormal];
        self.btnLink.hidden = NO;
    } else {
        
        self.btnLink.hidden = YES;
    }

    
    self.viewPopup.layer.cornerRadius = 3.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)linkButtonClick:(id)sender {
    
    NSURL*    url  = [[NSURL alloc] initWithString:self.linkURL];
    
    if (url.scheme.length == 0)
    {
        self.linkURL = [@"http://" stringByAppendingString:self.linkURL];
        url  = [[NSURL alloc] initWithString:self.linkURL];
    }
    
    [[UIApplication sharedApplication] openURL:url];
    [self.delegate popupCloseButtonClicked:self];
}

- (IBAction)closeButtonClick:(id)sender {
    
    [self.delegate popupCloseButtonClicked:self];
}

@end
