//
//  ChooseGroupViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/10/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseGroupDelegate.h"

@interface ChooseGroupViewController : UIViewController

@property (strong,nonatomic) id<ChooseGroupDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UIView *outerView;


- (IBAction)closeButtonClick:(id)sender;

@end
