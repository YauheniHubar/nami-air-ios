//
//  PostMeToo.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostMeToo.h"
#import <Parse/PFObject+Subclass.h>

@implementation PostMeToo

@dynamic displayName,user,post;

+(NSString *)parseClassName {
    return @"PostMeToo";
}


@end
