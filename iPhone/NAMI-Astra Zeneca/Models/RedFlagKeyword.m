//
//  RedFlagKeyword.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/18/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "RedFlagKeyword.h"
#import <Parse/PFObject+Subclass.h>

@implementation RedFlagKeyword

@dynamic displayName,name;

+(NSString *)parseClassName {
    return @"RedFlagKeyword";
}


@end
