//
//  AppVersionViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/12/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import "AppVersionViewController.h"

@interface AppVersionViewController ()

@end

@implementation AppVersionViewController

@synthesize version;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    self.version.text = [NSString stringWithFormat:@"%@ (%@)", majorVersion, minorVersion];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
