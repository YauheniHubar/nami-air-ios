//
//  ModeratorNotificationDelegate.h
//  NAMI-Astra Zeneca
//
//  Created by Brian Swartz on 1/11/15.
//  Copyright (c) 2015 IMRE. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ModeratorNotificationDelegate <NSObject>


-(void)moderatorMessageIgnoreButtonClicked:(id)sender;
-(void)moderatorMessageOpenButtonClicked:(id)sender;

@end
