//
//  PostMeTooPopupViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/18/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostMeTooPopupViewController.h"

@interface PostMeTooPopupViewController ()

@end

@implementation PostMeTooPopupViewController

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.tag = POST_POPUP_METOO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)yesButtonClicked:(id)sender {
    if (delegate) {
        [delegate okButtonClicked:self];
    }
}

- (IBAction)noButtonClicked:(id)sender {
    if (delegate) {
        [delegate cancelButtonClicked:self];
    }
}

@end
