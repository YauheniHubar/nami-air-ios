//
//  GetHelpSupportTableViewController.h
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 12/31/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface GetHelpSupportTableViewController : UITableViewController

- (IBAction)askButton:(id)sender;

@end
