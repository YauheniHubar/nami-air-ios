//
//  PostNote.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/17/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "PostNote.h"
#import <Parse/PFObject+Subclass.h>

@implementation PostNote

@dynamic displayName,post,user,notes;

+(NSString *)parseClassName {
    return @"PostNote";
}


@end
