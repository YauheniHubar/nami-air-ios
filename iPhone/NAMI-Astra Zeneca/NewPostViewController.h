//

#import <UIKit/UIKit.h>
#import "Models.h"
#import "NoteSelectionViewController.h"
#import "PostPopupDelegate.h"
#import "PostDetailsViewController.h"

@interface NewPostViewController : UIViewController <NoteSelectionDelegate,PostPopupDelegate>

//@property (nonatomic, strong) UITextView *textView;
//The outlet of the bottom spacing constraint
@property (strong, nonatomic) IBOutlet UIView *accessoryView;
@property (strong, nonatomic) IBOutlet UIButton *btnFeelingsIndicator;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *postButton;
@property (assign, nonatomic) BOOL fromFeed;
@property (assign, nonatomic) id<PostDetailUpdateDelegate> postUpdateDelegate;

@property PFObject *inspiringPost;

- (IBAction)dismissKeyboard:(id)sender;
- (void)addButtonTouchHandler:(id)sender;
- (void)cancelButtonTouchHandler:(id)sender;
- (IBAction)cancelButtonHandler:(id)sender;
- (IBAction)addButtonHandler:(id)sender;

- (IBAction)selectFeelings:(id)sender;

@end
