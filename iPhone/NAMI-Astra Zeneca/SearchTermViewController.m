//
//  SearchTermViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Steven Smith on 12/9/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "SearchTermViewController.h"
#import "PostsViewController.h"

@interface SearchTermViewController ()

@end

@implementation SearchTermViewController

@synthesize searchBar,delegate;

- (void)viewDidLoad {
    [super viewDidLoad];

    searchBar.delegate = self;
    [searchBar becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSString *searchText = searchBar.text;
    PostsViewController *pvc = (PostsViewController *)self.presentingViewController;
    if (delegate) {
        [delegate userEnteredSearchTerm:searchText];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
