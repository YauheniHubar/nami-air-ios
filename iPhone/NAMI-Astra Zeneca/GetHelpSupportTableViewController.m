			//
//  GetHelpSupportTableViewController.m
//  NAMI-Astra Zeneca
//
//  Created by Jacob Haskins on 12/31/14.
//  Copyright (c) 2014 IMRE. All rights reserved.
//

#import "GetHelpSupportTableViewController.h"

@interface GetHelpSupportTableViewController ()
@property (weak, nonatomic) IBOutlet UIButton *askButton;
@end

@implementation GetHelpSupportTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.askButton.layer.cornerRadius = 5;
}

- (void)didReceiveMemoryWarning {	
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nami.org/local-app?utm_source=AIR&utm_medium=Support&utm_content=Local_NAMI&utm_campaign=AIRapp"]];
    } else if (indexPath.row == 2) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nami.org/programs-app?utm_source=AIR&utm_medium=Support&utm_content=Programs&utm_campaign=AIRapp"]];
    } else if (indexPath.row == 3) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nami.org/asknami-app?utm_source=AIR&utm_medium=Support&utm_content=Ask_NAMI&utm_campaign=AIRapp"]];
    }
}

- (IBAction)askButton:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nami.org/asknami-app?utm_source=AIR&utm_medium=Support&utm_content=Ask_NAMI&utm_campaign=AIRapp"]];
}


@end
